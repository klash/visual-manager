<?php
require_once('object.inc.php');
session_start();
reset($_SESSION);
reset($_GET);
$function = "vedi";
require('config.inc.php');
require('front.inc.php');
require('start.inc.php');
$title = "title";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title><?php echo $title; ?></title>
<link href="http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>css/reset.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
<link href="http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>css/lightview.css" rel="stylesheet" type="text/css" />
<link href="http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>css/lightview/lightview.css" rel="stylesheet" type="text/css" />
<link href="http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>css/jshowoff.css" rel="stylesheet" type="text/css" />

<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
<script src="http://code.jquery.com/jquery-migrate-1.2.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script type="text/javascript" src="http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>js/lightview/spinners.min.js"></script>
<script type="text/javascript" src="http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>js/lightview/lightview.js"></script>
<script type="text/javascript" src="http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>js/jquery.jshowoff.min.js"></script>

<script type="text/javascript">
function esegui(forma,tabella,azione,id) {
	stringa = $("#"+forma).serialize();
	path = "http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>manager/"+tabella+"/"+azione+".php?tabella="+tabella+"&id="+id;
	$.ajax({
		type: "POST",
		url: path,
		data: stringa,
		success: function(msg) {
			$("#tool_"+tabella).load("http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>manager/"+tabella+".php");
			alert(msg);
		}
	});
}

function cancella() {
  if (document.getElementById) 
  {
    var args=cancella.arguments;
	var rule='';
	var testo = args[0];
	var tabella = args[1];
	var id = args[2];
	var pagina = args[3];
  	for (i=4; i<(args.length); i++)
	{
		if(args[i] != "")
		{
			rule += "&rule["+i+"]="+args[i];
		}
	}
	if (confirm('Stai per cancellare * '+testo+' * confermi la cancellazione?'))
	{
		path = "http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>manager/delete.php?tabella="+tabella+"&id="+id+"&pagina="+pagina;
		$.ajax({
			type: "POST",
			url: path,
			data: rule,
			success: function(msg) {
//			alert(msg);
//			location.reload();
				window.location.href = pagina;
			}
		});
	}
  }
}

function sposta() {
  if (document.getElementById) 
  {
    var args=sposta.arguments;
	var rule='';
	var tabella = args[0];
	var id = args[1];
	var azione = args[2];
  	for (i=3; i<(args.length); i++)
	{
//		alert(args[i]);
		rule += "&rule["+i+"]="+args[i];
	}
	path = "http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>manager/sposta.php?tabella="+tabella+"&id="+id+"&azione="+azione;
	$.ajax({
		type: "POST",
		url: path,
		data: rule,
		success: function(msg) {
//			alert(msg);
			location.reload();
		}
	});
  }
}

function attivo(tabella,id) {
	data = 0;
	path = "http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>manager/attivo.php?tabella="+tabella+"&id="+id;
	$.ajax({
		type: "POST",
		url: path,
		data: data,
		success: function(msg) {
//			alert(msg);
			location.reload();
		}
	});
}
</script>

</head>
<?php
if(isset($_SESSION['utente']) && $_SESSION['utente']->id > 0)
{
	$function = "manager";
}
if(isset($_SESSION['pagina']) && $_SESSION['pagina'] > 0 && $function == "manager")
{
	$function = $_SESSION['utente']->autorizzazione($_SESSION['pagina']);
}
if(isset($_POST['mode']) && $_POST['mode'] == "on")
{
	$_SESSION['preview'] = TRUE;
}
if(isset($_POST['mode']) && $_POST['mode'] == "off")
{
	unset($_SESSION['preview']);
}
if(isset($_SESSION['preview']) && $_SESSION['preview'])
{
	$function = "vedi";
}
?>
<body>
<?php
if (isset($stato) && $stato == "login" && $function != "manager" && !$_SESSION['preview'])
{
	include("manager/accesso.php");
}
if ($function == "manager" || $_SESSION['preview'])
{
	include("manager/manager.php");
}
?>
