

CREATE TABLE IF NOT EXISTS `log` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `redattore` int(11) NOT NULL COMMENT 'index',
  `azione` enum('aggiunta','modifica','cancella') COLLATE utf8_bin NOT NULL,
  `tabella` int(11) NOT NULL COMMENT 'index',
  `key` int(11) NOT NULL,
  `campo` varchar(255) COLLATE utf8_bin NOT NULL,
  `data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `pagina` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `primario` int(11) NOT NULL,
  `lingua` int(11) NOT NULL,
  `menu` varchar(255) COLLATE utf8_bin NOT NULL,
  `permalink` varchar(255) COLLATE utf8_bin NOT NULL,
  `keywords` blob,
  `description` blob,
  `titolo` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `abstract` blob,
  `testo` blob,
  `foto` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT 'image',
  `php` varchar(255) COLLATE utf8_bin NOT NULL,
  `stato` enum('top','menu','footer','hidden') COLLATE utf8_bin NOT NULL DEFAULT 'top',
  `chiave` varchar(255) COLLATE utf8_bin NOT NULL,
  `dinamica` enum('no','si') COLLATE utf8_bin NOT NULL DEFAULT 'no',
  `attivo` enum('no','si') COLLATE utf8_bin NOT NULL DEFAULT 'no',
  `posizione` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `permalink` (`permalink`),
  KEY `php` (`php`),
  KEY `chiave` (`chiave`),
  KEY `dinamica` (`dinamica`),
  KEY `posizione` (`posizione`),
  KEY `primario` (`primario`),
  KEY `lingua` (`lingua`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `profilo` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `profilo` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=3 ;

--
-- Dump dei dati per la tabella `profilo`
--

INSERT INTO `profilo` (`ID`, `profilo`) VALUES(1, 'web master');
INSERT INTO `profilo` (`ID`, `profilo`) VALUES(2, 'redattore');



CREATE TABLE IF NOT EXISTS `profilo_pagina` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `profilo` int(11) NOT NULL COMMENT 'index',
  `pagina` int(11) NOT NULL COMMENT 'index',
  PRIMARY KEY (`ID`),
  KEY `profilo` (`profilo`),
  KEY `pagina` (`pagina`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=2 ;

--
-- Dump dei dati per la tabella `profilo_pagina`
--

INSERT INTO `profilo_pagina` (`ID`, `profilo`, `pagina`) VALUES(1, 1, 0);



CREATE TABLE IF NOT EXISTS `redattore` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `nominativo` varchar(255) COLLATE utf8_bin NOT NULL,
  `email` varchar(255) COLLATE utf8_bin NOT NULL,
  `user` varchar(255) COLLATE utf8_bin NOT NULL,
  `psw` varchar(255) COLLATE utf8_bin NOT NULL,
  `profilo` int(11) NOT NULL,
  `accesso` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `user` (`user`),
  KEY `password` (`psw`),
  KEY `profilo` (`profilo`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=2 ;

--
-- Dump dei dati per la tabella `redattore`
--

INSERT INTO `redattore` (`ID`, `data`, `nominativo`, `email`, `user`, `psw`, `profilo`, `accesso`) VALUES(1, '2012-06-02 14:26:00', 'alessandro verna', 'sandro@klash.it', 'klash', 'd1d2a69735187a1781632f931a228b65', 1, '2013-06-14 12:20:01');




CREATE TABLE IF NOT EXISTS `tabella` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) COLLATE utf8_bin NOT NULL,
  `titolo` varchar(50) COLLATE utf8_bin NOT NULL,
  `vif` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `lingua` enum('no','si') COLLATE utf8_bin NOT NULL DEFAULT 'no',
  `seo` enum('no','si') COLLATE utf8_bin NOT NULL DEFAULT 'no',
  `attivo` enum('no','si') COLLATE utf8_bin NOT NULL DEFAULT 'no',
  `posizione` enum('no','si') COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `lingua` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `lingua` varchar(100) COLLATE utf8_bin NOT NULL,
  `attivo` enum('no','si') COLLATE utf8_bin NOT NULL DEFAULT 'no',
  `primario` enum('no','si') COLLATE utf8_bin NOT NULL,
  `path` varchar(255) COLLATE utf8_bin NOT NULL,
  `posizione` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `posizione` (`posizione`),
  KEY `attivo` (`attivo`),
  KEY `primario` (`primario`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=3 ;

--
-- Dump dei dati per la tabella `lingua`
--

INSERT INTO `lingua` (`ID`, `lingua`, `attivo`, `primario`, `path`, `posizione`) VALUES(1, 'italiano', 'si', 'si', 'it', 1);
INSERT INTO `lingua` (`ID`, `lingua`, `attivo`, `primario`, `path`, `posizione`) VALUES(2, 'english', 'si', 'no', 'en', 2);

-- --------------------------------------------------------


CREATE TABLE IF NOT EXISTS `data_giorno` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `lingua` int(11) NOT NULL DEFAULT '0',
  `primario` int(11) NOT NULL DEFAULT '0',
  `giorno` varchar(50) COLLATE utf8_bin NOT NULL,
  `posizione` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `lingua` (`lingua`),
  KEY `italiano` (`primario`),
  KEY `posizione` (`posizione`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=15 ;

--
-- Dump dei dati per la tabella `data_giorno`
--

INSERT INTO `data_giorno` (`ID`, `lingua`, `primario`, `giorno`, `posizione`) VALUES
(1, 1, 1, 'luned&igrave;', 2),
(2, 2, 1, 'monday', 0),
(3, 1, 3, 'marted&igrave;', 3),
(4, 2, 3, 'tuesday', 0),
(5, 1, 5, 'mercoled&igrave;', 4),
(6, 2, 5, 'wednesday', 0),
(7, 1, 7, 'gioved&igrave;', 5),
(8, 2, 7, 'thursday', 0),
(9, 1, 9, 'venerd&igrave;', 6),
(10, 2, 9, 'friday', 0),
(11, 1, 11, 'sabato', 7),
(12, 2, 11, 'saturday', 0),
(13, 1, 13, 'domenica', 1),
(14, 2, 13, 'sunday', 0);

-- --------------------------------------------------------

--
-- Struttura della tabella `data_mese`
--

CREATE TABLE IF NOT EXISTS `data_mese` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `lingua` int(11) NOT NULL DEFAULT '0',
  `primario` int(11) NOT NULL DEFAULT '0',
  `mese` varchar(50) COLLATE utf8_bin NOT NULL,
  `posizione` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `lingua` (`lingua`),
  KEY `italiano` (`primario`),
  KEY `posizione` (`posizione`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=25 ;

--
-- Dump dei dati per la tabella `data_mese`
--

INSERT INTO `data_mese` (`ID`, `lingua`, `primario`, `mese`, `posizione`) VALUES
(1, 1, 1, 'gennaio', 1),
(2, 2, 1, 'january', 0),
(3, 1, 3, 'febbraio', 2),
(4, 2, 3, 'february', 0),
(5, 1, 5, 'marzo', 3),
(6, 2, 5, 'march', 0),
(7, 1, 7, 'aprile', 4),
(8, 2, 7, 'april', 0),
(9, 1, 9, 'maggio', 5),
(10, 2, 9, 'may', 0),
(11, 1, 11, 'giugno', 6),
(12, 2, 11, 'june', 0),
(13, 1, 13, 'luglio', 7),
(14, 2, 13, 'july', 0),
(15, 1, 15, 'agosto', 8),
(16, 2, 15, 'august', 0),
(17, 1, 17, 'settembre', 9),
(18, 2, 17, 'september', 0),
(19, 1, 19, 'ottobre', 10),
(20, 2, 19, 'october', 0),
(21, 1, 21, 'novembre', 11),
(22, 2, 21, 'november', 0),
(23, 1, 23, 'dicembre', 12),
(24, 2, 23, 'december', 0);

-- --------------------------------------------------------


CREATE TABLE IF NOT EXISTS `traduzione` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `primario` int(11) NOT NULL,
  `lingua` int(11) NOT NULL,
  `chiave` varchar(255) COLLATE utf8_bin NOT NULL,
  `testo` varchar(255) COLLATE utf8_bin NOT NULL,
  `permalink` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `primario` (`primario`),
  KEY `lingua` (`lingua`),
  KEY `chiave` (`chiave`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;
