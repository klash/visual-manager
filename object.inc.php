<?php
include("object/utente.php");
// INTERFACCIA
include("object/blob.php");
include("object/data.php");
include("object/permalink.php");
include("object/text.php");
include("object/email.php");
include("object/url.php");
include("object/prezzo.php");
include("object/traduzione.php");
include("object/streaming.php");
include("object/enum.php");
include("object/ora.php");
include("object/categoria.php");
include("object/relazione.php");
// MEDIA
include("object/allegato.php");
include("object/grafica.php");
include("object/video.php");
// GALLERY
include("object/photogallery.php");
include("object/videogallery.php");
?>