<?php
include("../../config.inc.php");
echo "redattore \n";
echo "-------------------------------- \n";
$errore = 0;
if(trim($_POST['nominativo_0']) == "" || trim($_POST['nominativo_0']) == "nominativo")
{
	$errore = 1;
	echo "nominativo obbligatorio \n";
}
if(!filter_var(trim($_POST['email_0']), FILTER_VALIDATE_EMAIL))
{
	$errore = 1;
	echo "email obbligatoria \n";
}
if(trim($_POST['user_0']) == "" || trim($_POST['user_0']) == "user id")
{
	$errore = 1;
	echo "user id obbligatoria \n";
}
if(trim($_POST['psw_0']) == "" || trim($_POST['psw_0']) == "password")
{
	$errore = 1;
	echo "password obbligatoria \n";
}
if(strlen(trim($_POST['psw_0'])) < 8)
{
	$errore = 1;
	echo "la password deve contenere almeno 8 caratteri \n";
}
if(strlen(trim($_POST['psw_0'])) > 20)
{
	$errore = 1;
	echo "la password deve contenere al massimo 20 caratteri \n";
}
if(!preg_match("#[0-9]+#", trim($_POST['psw_0'])))
{
	$errore = 1;
	echo "la password deve includere almeno un carattere numerico \n";
}
if(!preg_match("#[a-z]+#", trim($_POST['psw_0'])))
{
	$errore = 1;
	echo "la password deve includere almeno una lettera minuscola \n";
}
if(!preg_match("#[A-Z]+#", trim($_POST['psw_0']))) 
{
	$errore = 1;
	echo "la password deve includere almeno una lettera maiuscola \n";
}
if($errore == 0)
{
	$query_aggiungi = "INSERT INTO redattore (`data` ,`nominativo` ,`email` ,`user` ,`psw` ,`profilo`) VALUES (CURRENT_TIMESTAMP , '".$_POST['nominativo_0']."', '".$_POST['email_0']."', '".$_POST['user_0']."', '".md5($_POST['psw_0'])."', '".$_POST['profilo_0']."')";
	$risultato_aggiungi = mysql_query($query_aggiungi);
	echo "redattore ".$_POST['nominativo_0']." aggiunto correttamente \n";
}
?>
