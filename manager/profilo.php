<?php
if(!isset($db_link))
{
	session_start();
	reset($_SESSION);
	require("../config.inc.php");
	require('../object.inc.php');
	unset($_SESSION['utente']);
	$_SESSION['utente'] = new utente;
	$_SESSION['utente']->init($_SESSION['id_utente']);
}
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#FF0000">
  <tr>
    <td>
    <div style="height:10px;"></div>
    <table width="100%" border="0" cellpadding="10" cellspacing="0">
      <tr>
        <td width="20">&nbsp;</td>
        <td class="titolo_manager">profili</td>
        <td width="18"><input name="chiudi" type="button" class="bottone_manager" value="chiudi" onclick="document.getElementById('tool_profilo').style.display='none';" ></td>
        <td width="20">&nbsp;</td>
      </tr>
    </table>
    <div style="height:5px;"></div>
    <div id="linea_manager"></div>
    <div style="height:5px;"></div>
<?php
$profilo = array();
$query_profilo = "SELECT profilo.*, COUNT(*) AS livello FROM profilo, profilo_pagina WHERE profilo.ID = profilo_pagina.profilo GROUP BY ID ORDER BY livello DESC";
$risultato_profilo = mysql_query($query_profilo);
while ($riga_profilo = mysql_fetch_array($risultato_profilo))
{
	array_push ($profilo, "ID <> ".$riga_profilo['ID']);
	if($riga_profilo['livello'] <= $_SESSION['utente']->livello)
	{
?>
    <form name="form_profilo_<?php echo $riga_profilo['ID']; ?>" id="form_profilo_<?php echo $riga_profilo['ID']; ?>" method="post">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td>
          <table border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="20">&nbsp;</td>
              <td class="txt_manager">profilo</td>
              <td width="20">&nbsp;</td>
              <td><input name="profilo_profilo_<?php echo $riga_profilo['ID']; ?>" id="profilo_profilo_<?php echo $riga_profilo['ID']; ?>" type="text" class="input_manager" value="<?php echo $riga_profilo['profilo']; ?>" placeholder="profilo" /></td>
            </tr>
          </table>
<?php
		if($_SESSION['utente']->manager)
		{
			$checked = "";
			$query_relazione = "SELECT * FROM profilo_pagina WHERE profilo = '".$riga_profilo['ID']."' AND pagina = '0'";
			$risultato_relazione = mysql_query($query_relazione);
			$trovato_relazione = mysql_num_rows($risultato_relazione);
			if($trovato_relazione > 0)
			{
				$checked = "checked";
			}
?>
          <table border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td>&nbsp;</td>
            </tr>
          </table>
          <table border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="20">&nbsp;</td>
              <td class="txt_manager"><input name="profilo_pagina[<?php echo $riga_profilo['ID']; ?>][0]" id="profilo_pagina[<?php echo $riga_profilo['ID']; ?>][0]" type="checkbox" class="check_manager" value="0" <?php echo $checked; ?> /><label for="profilo_pagina[<?php echo $riga_profilo['ID']; ?>][0]">manager</label></td>
            </tr>
          </table>
<?php
		}
?>
          <table border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td>&nbsp;</td>
<?php
		$stato = "";
		$query_pagina = "SELECT * FROM pagina WHERE dinamica = 'si' ORDER BY stato,posizione";
		$risultato_pagina = mysql_query($query_pagina);
		while ($riga_pagina = mysql_fetch_array($risultato_pagina))
		{
			$checked = "";
			$query_relazione = "SELECT * FROM profilo_pagina WHERE profilo = '".$riga_profilo['ID']."' AND pagina = '".$riga_pagina['ID']."'";
			$risultato_relazione = mysql_query($query_relazione);
			$trovato_relazione = mysql_num_rows($risultato_relazione);
			if($trovato_relazione > 0)
			{
				$checked = "checked";
			}
			if($stato != $riga_pagina['stato'])
			{
				$stato = $riga_pagina['stato'];
?>
            </tr>
          </table>
          <table border="0" cellspacing="0" cellpadding="0">
            <tr>
<?php
			}
?>
              <td width="20">&nbsp;</td>
              <td class="txt_manager"><input name="profilo_pagina[<?php echo $riga_profilo['ID']; ?>][<?php echo $riga_pagina['ID']; ?>]" id="profilo_pagina[<?php echo $riga_profilo['ID']; ?>][<?php echo $riga_pagina['ID']; ?>]" type="checkbox" class="check_manager" value="<?php echo $riga_pagina['ID']; ?>" <?php echo $checked; ?> /><label for="profilo_pagina[<?php echo $riga_profilo['ID']; ?>][<?php echo $riga_pagina['ID']; ?>]"><?php echo $riga_pagina['menu']; ?></label></td>
<?php
		}
?>
            </tr>
          </table>
          </td>
          <td align="right" valign="top">
          <table border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td><input name="modifica" type="button" class="bottone_manager" value="modifica" onclick="esegui('form_profilo_<?php echo $riga_profilo['ID']; ?>','profilo','modifica',<?php echo $riga_profilo['ID']; ?>)" ></td>
              <td width="20">&nbsp;</td>
              <td><input name="cancella" type="button" class="bottone_manager" value="cancella" onclick="if( confirm('sei sicuro di voler cancellare il profilo: <?php echo $riga_profilo['profilo']; ?>?') ) esegui('form_profilo_<?php echo $riga_profilo['ID']; ?>','profilo','cancella',<?php echo $riga_profilo['ID']; ?>)" /></td>
              <td width="20">&nbsp;</td>
            </tr>
          </table>
          </td>
          </tr>
      </table>
    </form>
	<div style="height:5px;"></div>
    <div id="linea_manager"></div>
    <div style="height:5px;"></div>
<?php
	}
}
?>
<?php
$where = implode(" AND ",$profilo);
if(count($where) > 0)
{
	$query_profilo = "SELECT profilo.* FROM profilo WHERE ".$where." ORDER BY profilo";
}
else
{
	$query_profilo = "SELECT profilo.* FROM profilo ORDER BY profilo";
}
$risultato_profilo = mysql_query($query_profilo);
while ($riga_profilo = mysql_fetch_array($risultato_profilo))
{
	array_push ($profilo, $riga_profilo['ID']);
	if($riga_profilo['livello'] <= $_SESSION['utente']->livello)
	{
?>
    <form name="form_profilo_<?php echo $riga_profilo['ID']; ?>" id="form_profilo_<?php echo $riga_profilo['ID']; ?>" method="post">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td>
          <table border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="20">&nbsp;</td>
              <td class="txt_manager">profilo</td>
              <td width="20">&nbsp;</td>
              <td><input name="profilo_profilo_<?php echo $riga_profilo['ID']; ?>" id="profilo_profilo_<?php echo $riga_profilo['ID']; ?>" type="text" class="input_manager" value="<?php echo $riga_profilo['profilo']; ?>" placeholder="profilo" /></td>
            </tr>
          </table>
<?php
		if($_SESSION['utente']->manager)
		{
			$checked = "";
			$query_relazione = "SELECT * FROM profilo_pagina WHERE profilo = '".$riga_profilo['ID']."' AND pagina = '0'";
			$risultato_relazione = mysql_query($query_relazione);
			$trovato_relazione = mysql_num_rows($risultato_relazione);
			if($trovato_relazione > 0)
			{
				$checked = "checked";
			}
?>
          <table border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td>&nbsp;</td>
            </tr>
          </table>
          <table border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="20">&nbsp;</td>
              <td class="txt_manager"><input name="profilo_pagina[<?php echo $riga_profilo['ID']; ?>][0]" id="profilo_pagina[<?php echo $riga_profilo['ID']; ?>][0]" type="checkbox" class="check_manager" value="0" <?php echo $checked; ?> /><label for="profilo_pagina[<?php echo $riga_profilo['ID']; ?>][0]">manager</label></td>
            </tr>
          </table>
<?php
		}
?>
          <table border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td>&nbsp;</td>
<?php
		$stato = "";
		$query_pagina = "SELECT * FROM pagina WHERE dinamica = 'si' ORDER BY stato,posizione";
		$risultato_pagina = mysql_query($query_pagina);
		while ($riga_pagina = mysql_fetch_array($risultato_pagina))
		{
			$checked = "";
			$query_relazione = "SELECT * FROM profilo_pagina WHERE profilo = '".$riga_profilo['ID']."' AND pagina = '".$riga_pagina['ID']."'";
			$risultato_relazione = mysql_query($query_relazione);
			$trovato_relazione = mysql_num_rows($risultato_relazione);
			if($trovato_relazione > 0)
			{
				$checked = "checked";
			}
			if($stato != $riga_pagina['stato'])
			{
				$stato = $riga_pagina['stato'];
?>
            </tr>
          </table>
          <table border="0" cellspacing="0" cellpadding="0">
            <tr>
<?php
			}
?>
              <td width="20">&nbsp;</td>
              <td class="txt_manager"><input name="profilo_pagina[<?php echo $riga_profilo['ID']; ?>][<?php echo $riga_pagina['ID']; ?>]" id="profilo_pagina[<?php echo $riga_profilo['ID']; ?>][<?php echo $riga_pagina['ID']; ?>]" type="checkbox" class="check_manager" value="<?php echo $riga_pagina['ID']; ?>" <?php echo $checked; ?> /><label for="profilo_pagina[<?php echo $riga_profilo['ID']; ?>][<?php echo $riga_pagina['ID']; ?>]"><?php echo $riga_pagina['menu']; ?></label></td>
<?php
		}
?>
            </tr>
          </table>
          </td>
          <td align="right" valign="top">
          <table border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td><input name="modifica" type="button" class="bottone_manager" value="modifica" onclick="esegui('form_profilo_<?php echo $riga_profilo['ID']; ?>','profilo','modifica',<?php echo $riga_profilo['ID']; ?>)" ></td>
              <td width="20">&nbsp;</td>
              <td><input name="cancella" type="button" class="bottone_manager" value="cancella" onclick="if( confirm('sei sicuro di voler cancellare il profilo: <?php echo $riga_profilo['profilo']; ?>?') ) esegui('form_profilo_<?php echo $riga_profilo['ID']; ?>','profilo','cancella',<?php echo $riga_profilo['ID']; ?>)" /></td>
              <td width="20">&nbsp;</td>
            </tr>
          </table>
          </td>
          </tr>
      </table>
    </form>
	<div style="height:5px;"></div>
    <div id="linea_manager"></div>
    <div style="height:5px;"></div>
<?php
	}
}
?>
    <form name="form_profilo_0" id="form_profilo_0" method="post">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td>
          <table border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="20">&nbsp;</td>
              <td class="txt_manager">profilo</td>
              <td width="20">&nbsp;</td>
              <td><input name="profilo_profilo_0" id="profilo_profilo_0" type="text" class="input_manager" value="" placeholder="profilo" /></td>
            </tr>
          </table>
<?php
		if($_SESSION['utente']->manager)
		{
			$checked = "";
			$query_relazione = "SELECT * FROM profilo_pagina WHERE profilo = '".$riga_profilo['ID']."' AND pagina = '0'";
			$risultato_relazione = mysql_query($query_relazione);
			$trovato_relazione = mysql_num_rows($risultato_relazione);
			if($trovato_relazione > 0)
			{
				$checked = "checked";
			}
?>
          <table border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td>&nbsp;</td>
            </tr>
          </table>
          <table border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="20">&nbsp;</td>
              <td class="txt_manager"><input name="profilo_pagina[0][0]" id="profilo_pagina[0][0]" type="checkbox" class="check_manager" value="0" <?php echo $checked; ?> /><label for="profilo_pagina[0][0]">manager</label></td>
            </tr>
          </table>
<?php
		}
?>
          <table border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td>&nbsp;</td>
<?php
$stato = "";
$query_pagina = "SELECT * FROM pagina WHERE dinamica = 'si' ORDER BY stato,posizione";
$risultato_pagina = mysql_query($query_pagina);
while ($riga_pagina = mysql_fetch_array($risultato_pagina))
{
	$query_relazione = "SELECT * FROM profilo_pagina WHERE profilo = '".$riga_profilo['ID']."' AND pagina = '".$riga_pagina['ID']."'";
	$risultato_relazione = mysql_query($query_relazione);
	$trovato_relazione = mysql_num_rows($risultato_relazione);
	if($stato != $riga_pagina['stato'])
	{
		$stato = $riga_pagina['stato'];
?>
            </tr>
          </table>
          <table border="0" cellspacing="0" cellpadding="0">
            <tr>
<?php
	}
?>
              <td width="20">&nbsp;</td>
              <td class="txt_manager"><input name="profilo_pagina[0][<?php echo $riga_pagina['ID']; ?>]" id="profilo_pagina[0][<?php echo $riga_pagina['ID']; ?>]" type="checkbox" class="check_manager" value="<?php echo $riga_pagina['ID']; ?>" /><label for="profilo_pagina[0][<?php echo $riga_pagina['ID']; ?>]"><?php echo $riga_pagina['menu']; ?></label></td>
<?php
}
?>
            </tr>
          </table>
          </td>
          <td align="right" valign="top">
          <table border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td><input name="aggiungi" type="button" class="bottone_manager" value="aggiungi" onclick="esegui('form_profilo_0','profilo','aggiungi',0)" ></td>
              <td width="20">&nbsp;</td>
            </tr>
          </table>
          </td>
          </tr>
      </table>
    </form>
	<div style="height:10px;"></div>
    </td>
  </tr>
</table>
