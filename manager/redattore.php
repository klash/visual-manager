<?php
if(!isset($db_link))
{
	session_start();
	reset($_SESSION);
	require("../config.inc.php");
	require('../object.inc.php');
	unset($_SESSION['utente']);
	$_SESSION['utente'] = new utente;
	$_SESSION['utente']->init($_SESSION['id_utente']);
}
$livello = array();
$query_profilo = "SELECT * FROM profilo";
$risultato_profilo = mysql_query($query_profilo);
while ($riga_profilo = mysql_fetch_array($risultato_profilo))
{
	$query_pagina = "SELECT profilo_pagina.ID FROM profilo_pagina WHERE profilo_pagina.profilo = '".$riga_profilo['ID']."'";
	$risultato_pagina = mysql_query($query_pagina);
	$profilo_livello = mysql_num_rows($risultato_pagina);
	if($_SESSION['utente']->livello >= $profilo_livello)
	{
		$livello[$riga_profilo['ID']] = $profilo_livello;
	}
}
arsort($livello);
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#FF0000">
  <tr>
    <td>
    <div style="height:10px;"></div>
    <table width="100%" border="0" cellpadding="10" cellspacing="0">
      <tr>
        <td width="20">&nbsp;</td>
        <td class="titolo_manager">redattori</td>
        <td width="18"><input name="chiudi" type="button" class="bottone_manager" value="chiudi" onclick="document.getElementById('tool_redattore').style.display='none';" ></td>
        <td width="20">&nbsp;</td>
      </tr>
    </table>
    <div style="height:5px;"></div>
    <div id="linea_manager"></div>
    <div style="height:5px;"></div>
<?php
$query_redattore = "SELECT redattore.* FROM redattore";
$risultato_redattore = mysql_query($query_redattore);
while ($riga_redattore = mysql_fetch_array($risultato_redattore))
{
	if($_SESSION['utente']->livello >= $_SESSION['utente']->ctrl_livello($riga_redattore['ID']))
	{
?>
    <form name="form_redattore_<?php echo $riga_redattore['ID']; ?>" id="form_redattore_<?php echo $riga_redattore['ID']; ?>" method="post">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><table border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="20">&nbsp;</td>
              <td class="txt_manager">nominativo</td>
              <td width="20">&nbsp;</td>
              <td class="txt_manager">email</td>
              <td width="20">&nbsp;</td>
              <td class="txt_manager">profilo</td>
              <td width="20">&nbsp;</td>
              <td class="txt_manager">user id</td>
              <td width="20">&nbsp;</td>
              <td class="txt_manager">password</td>
            </tr>
            <tr>
              <td width="20">&nbsp;</td>
              <td><input name="nominativo_<?php echo $riga_redattore['ID']; ?>" id="nominativo_<?php echo $riga_redattore['ID']; ?>" type="text" class="input_manager" value="<?php echo $riga_redattore['nominativo']; ?>" placeholder="nominativo" /></td>
              <td width="20">&nbsp;</td>
              <td><input name="email_<?php echo $riga_redattore['ID']; ?>" id="email_<?php echo $riga_redattore['ID']; ?>" type="text" class="input_manager" value="<?php echo $riga_redattore['email']; ?>" placeholder="email" /></td>
              <td width="20">&nbsp;</td>
              <td><select name="profilo_<?php echo $riga_redattore['ID']; ?>" id="profilo_<?php echo $riga_redattore['ID']; ?>" class="select_manager">
<?php
		reset($livello);
		while (list($key, $val) = each($livello))
		{
			$selected = "";
			$query_profilo = "SELECT * FROM profilo WHERE ID = '".$key."'";
			$risultato_profilo = mysql_query($query_profilo);
			$riga_profilo = mysql_fetch_array($risultato_profilo);
			if($riga_profilo['ID'] == $riga_redattore['profilo'])
			{
				$selected = "selected='selected'";
			}
?>
                <option value="<?php echo $riga_profilo['ID']; ?>" <?php echo $selected; ?>><?php echo $riga_profilo['profilo']; ?></option>
<?php
		}
?>
              </select></td>
              <td width="20">&nbsp;</td>
              <td><input name="user_<?php echo $riga_redattore['ID']; ?>" id="user_<?php echo $riga_redattore['ID']; ?>" type="text" class="input_manager" value="<?php echo $riga_redattore['user']; ?>" placeholder="user" /></td>
              <td width="20">&nbsp;</td>
              <td><input name="psw_<?php echo $riga_redattore['ID']; ?>" id="psw_<?php echo $riga_redattore['ID']; ?>" type="password" class="input_manager" value="password" placeholder="password" /></td>
            </tr>
          </table>
          </td>
          <td align="right" valign="bottom">
          <table border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td><input name="modifica" type="button" class="bottone_manager" value="modifica" onclick="esegui('form_redattore_<?php echo $riga_redattore['ID']; ?>','redattore','modifica',<?php echo $riga_redattore['ID']; ?>)" ></td>
              <td width="20">&nbsp;</td>
              <td><input name="cancella" type="button" class="bottone_manager" value="cancella" onclick="if( confirm('sei sicuro di voler cancellare il redattore: <?php echo $riga_redattore['nominativo']; ?>?') ) esegui('form_redattore_<?php echo $riga_redattore['ID']; ?>','redattore','cancella',<?php echo $riga_redattore['ID']; ?>)" /></td>
              <td width="20">&nbsp;</td>
            </tr>
          </table>
          </td>
        </tr>
      </table>
    </form>
	<div style="height:5px;"></div>
    <div id="linea_manager"></div>
    <div style="height:5px;"></div>
<?php
	}
}
?>
    <form name="form_redattore_0" id="form_redattore_0" method="post">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td>
          <table border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="20">&nbsp;</td>
              <td class="txt_manager">nominativo</td>
              <td width="20">&nbsp;</td>
              <td class="txt_manager">email</td>
              <td width="20">&nbsp;</td>
              <td class="txt_manager">profilo</td>
              <td width="20">&nbsp;</td>
              <td class="txt_manager">user id</td>
              <td width="20">&nbsp;</td>
              <td class="txt_manager">password</td>
            </tr>
            <tr>
              <td width="20">&nbsp;</td>
              <td><input name="nominativo_0" id="nominativo_0" type="text" class="input_manager" value="" placeholder="nominativo" /></td>
              <td width="20">&nbsp;</td>
              <td><input name="email_0" id="email_0" type="text" class="input_manager" value="" placeholder="email" /></td>
              <td width="20">&nbsp;</td>
              <td><select name="profilo_0" id="profilo_0" class="select_manager">
<?php
		reset($livello);
		while (list($key, $val) = each($livello))
		{
			$query_profilo = "SELECT * FROM profilo WHERE ID = '".$key."'";
			$risultato_profilo = mysql_query($query_profilo);
			$riga_profilo = mysql_fetch_array($risultato_profilo);
?>
                <option value="<?php echo $riga_profilo['ID']; ?>" placeholder="" ><?php echo $riga_profilo['profilo']; ?></option>
<?php
		}
?>
              </select></td>
              <td width="20">&nbsp;</td>
              <td><input name="user_0" id="user_0" type="text" class="input_manager" value="" placeholder="user id" /></td>
              <td width="20">&nbsp;</td>
              <td><input name="psw_0" id="psw_0" type="password" class="input_manager" value="" placeholder="password" /></td>
            </tr>
          </table>
          </td>
          <td align="right" valign="bottom">
          <table border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td><input name="aggiungi" type="button" class="bottone_manager" value="aggiungi" onclick="esegui('form_redattore_0','redattore','aggiungi',0)" ></td>
              <td width="20">&nbsp;</td>
            </tr>
          </table>
          </td>
        </tr>
      </table>
	<div style="height:10px;"></div>
    </form>
    </td>
  </tr>
</table>
