<script type="text/javascript">
function mode(valore) {
	$("#mode").val(valore)
	$("#frm_manager").submit();
}
</script>
<link href="<?php echo $_SESSION['path']; ?>css/manager.css" rel="stylesheet" type="text/css" />
<form id="frm_manager" name="frm_manager" method="post" action="http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SERVER['REQUEST_URI']; ?>">
<input id="mode" name="mode" type="hidden" value="" />
</form>
<div id="manager">
<table border="0" cellspacing="0" cellpadding="0" style="margin-left:10px;">
<tr>
  <td valign="top" class="txt_manager" nowrap="nowrap">
  <?php echo $_SESSION['utente']->profilo; ?><br />
  <strong><?php echo $_SESSION['utente']->nominativo; ?></strong><br />
  </td>
  <td width="25">&quot;</td>
  <td valign="top" class="txt_manager" nowrap="nowrap">
  ultimo accesso<br />
  <strong><?php echo visualizza_data_ora($_SESSION['utente']->accesso); ?></strong>
  </td>
</tr>
</table>
<div style="height:25px;"></div>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
    <table border="0" cellspacing="0" cellpadding="0" style="margin-left:10px;">
      <tr>
        <!--<td valign="middle" class="menuTD"><a href="#" onclick="document.getElementById('tool_description').style.display='block';" class="menuTXT">DESCRIPTION</a></td>-->
        <!--<td valign="middle" class="menuTD"><a href="#" onclick="document.getElementById('tool_keywords').style.display='block';" class="menuTXT">KEYWORDS</a></td>-->
        <!--<td valign="middle" class="menuTD"><a href="#" onclick="document.getElementById('tool_logs').style.display='block';" class="menuTXT">LOGS</a></td>-->
        <td valign="top"><input name="redattori" type="submit" class="bottone_menu" value="redattori" onclick="document.getElementById('tool_redattore').style.display='block';" /></td>
        <td width="25">&quot;</td>
        <td valign="top"><input name="profili" type="submit" class="bottone_menu" value="profili" onclick="document.getElementById('tool_profilo').style.display='block';" /></td>
        <td width="25">&quot;</td>
        <td valign="top"><input name="lingua" type="submit" class="bottone_menu" value="lingua" onclick="document.getElementById('tool_lingua').style.display='block';" /></td>
        <td width="25">&quot;</td>
<?php
if($_SESSION['utente']->manager)
{
?>
        <td valign="top"><input name="pagine" type="submit" class="bottone_menu" value="pagine" onclick="document.getElementById('tool_pagina').style.display='block';" /></td>
        <td width="25">&quot;</td>
        <td valign="top"><input name="tabelle" type="submit" class="bottone_menu" value="tabelle" onclick="document.getElementById('tool_tabella').style.display='block';" /></td>
        <td width="25">&quot;</td>
<?php
}
?>
      </tr>
    </table>
    </td>
    <td align="right">
    <table border="0" cellspacing="0" cellpadding="0" style="margin-right:10px;">
      <tr>
<?php
if($_SESSION['preview'])
{
?>
        <td valign="top"><input name="preview" type="submit" class="bottone_menu" value="management" onclick="mode('off');" /></td>
<?php
}
else
{
?>
        <td valign="top"><input name="preview" type="submit" class="bottone_menu" value="preview" onclick="mode('on');" /></td>
<?php
}
?>
        <td width="25">&quot;</td>
        <td valign="top"><input name="log out" type="submit" class="bottone_menu" value="log out" onclick="document.location='<?php echo $_SESSION['path']; ?>logout';" /></td>
      </tr>
    </table>
    </td>
  </tr>
</table>
<!--<div id="tool_description" class="tool"><?php // include("manager/description.php"); ?></div>-->
<!--<div id="tool_keywords" class="tool"><?php // include("manager/keywords.php"); ?></div>-->
<!--<div id="tool_logs" class="tool"><?php // include("manager/logs.php"); ?></div>-->
<div id="tool_redattore" class="tool"><?php include("manager/redattore.php"); ?></div>
<div id="tool_profilo" class="tool"><?php include("manager/profilo.php"); ?></div>
<div id="tool_lingua" class="tool"><?php include("manager/lingua.php"); ?></div>
<?php
if($_SESSION['utente']->manager)
{
?>
<div id="tool_pagina" class="tool"><?php include("manager/pagina.php"); ?></div>
<div id="tool_tabella" class="tool"><?php include("manager/tabella.php"); ?></div>
<?php
}
?>
</div>