<?php
if(!isset($db_link))
{
	session_start();
	reset($_SESSION);
	require("../config.inc.php");
	require('../object.inc.php');
	unset($_SESSION['utente']);
	$_SESSION['utente'] = new utente;
	$_SESSION['utente']->init($_SESSION['id_utente']);
}
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#FF0000">
  <tr>
    <td>
    <div style="height:10px;"></div>
    <table width="100%" border="0" cellpadding="10" cellspacing="0">
      <tr>
        <td width="20">&nbsp;</td>
        <td class="titolo_manager">tabelle</td>
        <td width="18"><input name="chiudi" type="button" class="bottone_manager" value="chiudi" onclick="document.getElementById('tool_tabella').style.display='none';" ></td>
        <td width="20">&nbsp;</td>
      </tr>
    </table>
    <div style="height:5px;"></div>
    <div id="linea_manager"></div>
    <div style="height:5px;"></div>
<?php
$query_tabella = "SELECT * FROM `tabella` ORDER BY `titolo`";
$risultato_tabella = mysql_query($query_tabella);
while ($riga_tabella = mysql_fetch_array($risultato_tabella))
{
	$lingua = "";
	if($riga_tabella['lingua'] == "si")
	{
		$lingua = "checked";
	}
	$seo = "";
	if($riga_tabella['seo'] == "si")
	{
		$seo = "checked";
	}
	$attivo = "";
	if($riga_tabella['attivo'] == "si")
	{
		$attivo = "checked";
	}
	$posizione = "";
	if($riga_tabella['posizione'] == "si")
	{
		$posizione = "checked";
	}
?>
    <form name="form_tabella_<?php echo $riga_tabella['ID']; ?>" id="form_tabella_<?php echo $riga_tabella['ID']; ?>" method="post">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><table border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="20">&nbsp;</td>
              <td class="txt_manager">titolo</td>
              <td width="20">&nbsp;</td>
              <td class="txt_manager">nome</td>
              <td width="20">&nbsp;</td>
              <td class="txt_manager">vif</td>
              <td width="20">&nbsp;</td>
              <td class="txt_manager">&nbsp;</td>
              <td width="20">&nbsp;</td>
              <td class="txt_manager">&nbsp;</td>
              <td width="20">&nbsp;</td>
              <td class="txt_manager">&nbsp;</td>
              <td width="20">&nbsp;</td>
              <td class="txt_manager">&nbsp;</td>
            </tr>
            <tr>
              <td width="20">&nbsp;</td>
              <td><input name="tabella_titolo_<?php echo $riga_tabella['ID']; ?>" id="tabella_titolo_<?php echo $riga_tabella['ID']; ?>" type="text" class="input_manager" value="<?php echo $riga_tabella['titolo']; ?>" placeholder="titolo" /></td>
              <td width="20">&nbsp;</td>
              <td><input name="tabella_nome_<?php echo $riga_tabella['ID']; ?>" id="tabella_nome_<?php echo $riga_tabella['ID']; ?>" type="text" class="input_manager" value="<?php echo $riga_tabella['nome']; ?>" placeholder="nome" /></td>
              <td width="20">&nbsp;</td>
              <td><input name="tabella_vif_<?php echo $riga_tabella['ID']; ?>" id="tabella_vif_<?php echo $riga_tabella['ID']; ?>" type="text" class="input_manager" value="<?php echo $riga_tabella['vif']; ?>" placeholder="vif" /></td>
              <td width="20">&nbsp;</td>
              <td class="txt_manager"><input name="tabella_lingua_<?php echo $riga_tabella['ID']; ?>" id="tabella_lingua_<?php echo $riga_tabella['ID']; ?>" type="checkbox" class="check_manager" value="si" <?php echo $lingua; ?> /><label for="tabella_lingua_<?php echo $riga_tabella['ID']; ?>">lingua</label></td>
              <td width="20">&nbsp;</td>
              <td class="txt_manager"><input name="tabella_seo_<?php echo $riga_tabella['ID']; ?>" id="tabella_seo_<?php echo $riga_tabella['ID']; ?>" type="checkbox" class="check_manager" value="si" <?php echo $seo; ?> /><label for="tabella_seo_<?php echo $riga_tabella['ID']; ?>">seo</label></td>
              <td width="20">&nbsp;</td>
              <td class="txt_manager"><input name="tabella_attivo_<?php echo $riga_tabella['ID']; ?>" id="tabella_attivo_<?php echo $riga_tabella['ID']; ?>" type="checkbox" class="check_manager" value="si" <?php echo $attivo; ?> /><label for="tabella_attivo_<?php echo $riga_tabella['ID']; ?>">attivo</label></td>
              <td width="20">&nbsp;</td>
              <td class="txt_manager"><input name="tabella_posizione_<?php echo $riga_tabella['ID']; ?>" id="tabella_posizione_<?php echo $riga_tabella['ID']; ?>" type="checkbox" class="check_manager" value="si" <?php echo $posizione; ?> /><label for="tabella_posizione_<?php echo $riga_tabella['ID']; ?>">posizione</label></td>
            </tr>
          </table>
          </td>
          <td align="right" valign="bottom">
          <table border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td><input name="modifica" type="button" class="bottone_manager" value="modifica" onclick="esegui('form_tabella_<?php echo $riga_tabella['ID']; ?>','tabella','modifica',<?php echo $riga_tabella['ID']; ?>)" ></td>
              <td width="20">&nbsp;</td>
              <td><input name="cancella" type="button" class="bottone_manager" value="cancella" onclick="if( confirm('sei sicuro di voler cancellare il tabella: <?php echo $riga_tabella['nominativo']; ?>?') ) esegui('form_tabella_<?php echo $riga_tabella['ID']; ?>','tabella','cancella',<?php echo $riga_tabella['ID']; ?>)" /></td>
              <td width="20">&nbsp;</td>
            </tr>
          </table>
          </td>
        </tr>
      </table>
    </form>
    <div style="height:5px;"></div>
    <div id="linea_manager"></div>
    <div style="height:5px;"></div>
<?php
}
?>
    <form name="form_tabella_0" id="form_tabella_0" method="post">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><table border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="20">&nbsp;</td>
              <td class="txt_manager">titolo</td>
              <td width="20">&nbsp;</td>
              <td class="txt_manager">nome</td>
              <td width="20">&nbsp;</td>
              <td class="txt_manager">vif</td>
              <td width="20">&nbsp;</td>
              <td class="txt_manager">&nbsp;</td>
              <td width="20">&nbsp;</td>
              <td class="txt_manager">&nbsp;</td>
              <td width="20">&nbsp;</td>
              <td class="txt_manager">&nbsp;</td>
              <td width="20">&nbsp;</td>
              <td class="txt_manager">&nbsp;</td>
            </tr>
            <tr>
              <td width="20">&nbsp;</td>
              <td><input name="tabella_titolo_0" id="tabella_titolo_0" type="text" class="input_manager" value="<?php echo $riga_tabella['titolo']; ?>" placeholder="titolo" /></td>
              <td width="20">&nbsp;</td>
              <td><input name="tabella_nome_0" id="tabella_nome_0" type="text" class="input_manager" value="<?php echo $riga_tabella['nome']; ?>" placeholder="nome" /></td>
              <td width="20">&nbsp;</td>
              <td><input name="tabella_vif_0" id="tabella_vif_0" type="text" class="input_manager" value="<?php echo $riga_tabella['vif']; ?>" placeholder="vif" /></td>
              <td width="20">&nbsp;</td>
              <td valign="top" class="txt_manager"><input name="tabella_lingua_0" id="tabella_lingua_0" type="checkbox" value="si" class="check_manager" /><label for="tabella_lingua_0">lingua</label></td>
              <td width="20">&nbsp;</td>
              <td valign="top" class="txt_manager"><input name="tabella_seo_0" id="tabella_seo_0" type="checkbox" value="si" class="check_manager" /><label for="tabella_seo_0">seo</label></td>
              <td width="20">&nbsp;</td>
              <td valign="top" class="txt_manager"><input name="tabella_attivo_0" id="tabella_attivo_0" type="checkbox" value="si" class="check_manager" /><label for="tabella_attivo_0">attivo</label></td>
              <td width="20">&nbsp;</td>
              <td valign="top" class="txt_manager"><input name="tabella_posizione_0" id="tabella_posizione_0" type="checkbox" value="si" class="check_manager" /><label for="tabella_posizione_0">posizione</label></td>
            </tr>
          </table>
          </td>
          <td align="right" valign="bottom">
          <table border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td><input name="aggiungi" type="button" class="bottone_manager" value="aggiungi" onclick="esegui('form_tabella_0','tabella','aggiungi',0)" ></td>
              <td width="20">&nbsp;</td>
            </tr>
          </table>
          </td>
        </tr>
      </table>
    <div style="height:10px;"></div>
    </form>
    </td>
  </tr>
</table>
