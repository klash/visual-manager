<?php
if(!isset($db_link))
{
	session_start();
	reset($_SESSION);
	require("../config.inc.php");
	require('../object.inc.php');
	unset($_SESSION['utente']);
	$_SESSION['utente'] = new utente;
	$_SESSION['utente']->init($_SESSION['id_utente']);
}
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#FF0000">
  <tr>
    <td>
    <div style="height:10px;"></div>
    <table width="100%" border="0" cellpadding="10" cellspacing="0">
      <tr>
        <td width="20">&nbsp;</td>
        <td class="titolo_manager">pagine</td>
        <td width="18"><input name="chiudi" type="button" class="bottone_manager" value="chiudi" onclick="document.getElementById('tool_pagina').style.display='none';" ></td>
        <td width="20">&nbsp;</td>
      </tr>
    </table>
    <div style="height:5px;"></div>
    <div id="linea_manager"></div>
    <div style="height:5px;"></div>
<?php
$selected = array();
$stato = "";
$query_pagina = "SELECT * FROM pagina WHERE ID = primario ORDER BY stato,posizione";
$risultato_pagina = mysql_query($query_pagina);
while ($riga_pagina = mysql_fetch_array($risultato_pagina))
{
	$dinamica = "";
	if($riga_pagina['dinamica'] == "si")
	{
		$dinamica = "checked";
	}
	$attivo = "";
	if($riga_pagina['attivo'] == "si")
	{
		$attivo = "checked";
	}
	$selected[$riga_pagina['stato']] = "selected='selected'";
	if($stato != $riga_pagina['stato'])
	{
		$stato = $riga_pagina['stato'];
?>
<?php
	}
?>
    <form name="form_pagina_<?php echo $riga_pagina['ID']; ?>" id="form_pagina_<?php echo $riga_pagina['ID']; ?>" method="post">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><table border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="20">&nbsp;</td>
              <td class="txt_manager">menu</td>
              <td width="20">&nbsp;</td>
              <td class="txt_manager">php</td>
              <td width="20">&nbsp;</td>
              <td class="txt_manager">stato</td>
              <td width="20">&nbsp;</td>
              <td class="txt_manager">chiave</td>
              <td width="20">&nbsp;</td>
              <td class="txt_manager">&nbsp;</td>
              <td width="20">&nbsp;</td>
              <td class="txt_manager">&nbsp;</td>
            </tr>
            <tr>
              <td width="20">&nbsp;</td>
              <td><input name="pagina_menu_<?php echo $riga_pagina['ID']; ?>" id="pagina_menu_<?php echo $riga_pagina['ID']; ?>" type="text" class="input_manager" value="<?php echo $riga_pagina['menu']; ?>" /></td>
              <td width="20">&nbsp;</td>
              <td><input name="pagina_php_<?php echo $riga_pagina['ID']; ?>" id="pagina_php_<?php echo $riga_pagina['ID']; ?>" type="text" class="input_manager" value="<?php echo $riga_pagina['php']; ?>" /></td>
              <td width="20">&nbsp;</td>
              <td><select name="pagina_stato_<?php echo $riga_pagina['ID']; ?>" id="pagina_pagina_stato_<?php echo $riga_pagina['ID']; ?>" class="select_manager">
                <option value="top" <?php echo $selected['top']; ?>>top</option>
                <option value="menu" <?php echo $selected['menu']; ?>>menu</option>
                <option value="footer" <?php echo $selected['footer']; ?>>footer</option>
                <option value="hidden" <?php echo $selected['hidden']; ?>>hidden</option>
              </select></td>
              <td width="20">&nbsp;</td>
              <td><input name="pagina_chiave_<?php echo $riga_pagina['ID']; ?>" id="pagina_chiave_<?php echo $riga_pagina['ID']; ?>" type="text" class="input_manager" value="<?php echo $riga_pagina['chiave']; ?>" /></td>
              <td width="20">&nbsp;</td>
              <td class="txt_manager"><input name="pagina_dinamica_<?php echo $riga_pagina['ID']; ?>" id="pagina_dinamica_<?php echo $riga_pagina['ID']; ?>" type="checkbox" class="check_manager" value="si" <?php echo $dinamica; ?> /><label for="pagina_dinamica_<?php echo $riga_pagina['ID']; ?>">dinamica</label></td>
              <td width="20">&nbsp;</td>
              <td class="txt_manager"><input name="pagina_attivo_<?php echo $riga_pagina['ID']; ?>" id="pagina_attivo_<?php echo $riga_pagina['ID']; ?>" type="checkbox" class="check_manager" value="si" <?php echo $attivo; ?> /><label for="pagina_attivo_<?php echo $riga_pagina['ID']; ?>">attivo</label></td>
            </tr>
          </table>
          </td>
          <td align="right" valign="bottom">
          <table border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td><a href="#"><img src="http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>manager/su.gif" alt="modifica" border="0" onclick="esegui('form_pagina_<?php echo $riga_pagina['ID']; ?>','pagina','su',<?php echo $riga_pagina['ID']; ?>)" /></a></td>
              <td width="20">&nbsp;</td>
              <td><a href="#"><img src="http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>manager/giu.gif" alt="cancella" border="0" onclick="esegui('form_pagina_<?php echo $riga_pagina['ID']; ?>','pagina','giu',<?php echo $riga_pagina['ID']; ?>)" /></a></td>
              <td width="20">&nbsp;</td>
              <td><input name="modifica" type="button" class="bottone_manager" value="modifica" onclick="esegui('form_pagina_<?php echo $riga_pagina['ID']; ?>','pagina','modifica',<?php echo $riga_pagina['ID']; ?>)" ></td>
              <td width="20">&nbsp;</td>
              <td><input name="cancella" type="button" class="bottone_manager" value="cancella" onclick="if( confirm('sei sicuro di voler cancellare il pagina: <?php echo $riga_pagina['nominativo']; ?>?') ) esegui('form_pagina_<?php echo $riga_pagina['ID']; ?>','pagina','cancella',<?php echo $riga_pagina['ID']; ?>)" /></td>
              <td width="20">&nbsp;</td>
            </tr>
          </table>
          </td>
          </tr>
      </table>
    </form>
	<div style="height:5px;"></div>
    <div id="linea_manager"></div>
    <div style="height:5px;"></div>
<?php
}
?>
    <form name="form_pagina_0" id="form_pagina_0" method="post">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td>
          <table border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="20">&nbsp;</td>
              <td class="txt_manager">menu</td>
              <td width="20">&nbsp;</td>
              <td class="txt_manager">php</td>
              <td width="20">&nbsp;</td>
              <td class="txt_manager">stato</td>
              <td width="20">&nbsp;</td>
              <td class="txt_manager">chiave</td>
              <td width="20">&nbsp;</td>
              <td class="txt_manager">&nbsp;</td>
              <td width="20">&nbsp;</td>
              <td class="txt_manager">&nbsp;</td>
            </tr>
            <tr>
              <td width="20">&nbsp;</td>
              <td><input name="pagina_menu_0" id="pagina_menu_0" type="text" class="input_manager" value="" placeholder="menu" /></td>
              <td width="20">&nbsp;</td>
              <td><input name="pagina_php_0" id="pagina_php_0" type="text" class="input_manager" value="" placeholder="php" /></td>
              <td width="20">&nbsp;</td>
              <td><select name="pagina_stato_0" id="pagina_stato_0" class="select_manager">
                <option value="top" selected='selected'>top</option>
                <option value="menu">menu</option>
                <option value="footer">footer</option>
                <option value="hidden">hidden</option>
              </select></td>
              <td width="20">&nbsp;</td>
              <td><input name="pagina_chiave_0" id="pagina_chiave_0" type="text" class="input_manager" value="" placeholder="chiave" /></td>
              <td width="20">&nbsp;</td>
              <td class="txt_manager"><input name="pagina_dinamica_0" id="pagina_dinamica_0" type="checkbox" class="check_manager" value="si" /><label for="pagina_dinamica_0">dinamica</label></td>
              <td width="20">&nbsp;</td>
              <td class="txt_manager"><input name="pagina_attivo_0" id="pagina_attivo_0" type="checkbox" class="check_manager" value="si" /><label for="pagina_attivo_0">attivo</label></td>
            </tr>
          </table>
          </td>
          <td align="right" valign="bottom">
          <table border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td><input name="aggiungi" type="button" class="bottone_manager" value="aggiungi" onclick="esegui('form_pagina_0','pagina','aggiungi',0)" ></td>
              <td width="20">&nbsp;</td>
            </tr>
          </table>
          </td>
          </tr>
      </table>
    </form>
	<div style="height:10px;"></div>
    </td>
  </tr>
</table>
