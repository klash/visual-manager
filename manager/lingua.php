<?php
if(!isset($db_link))
{
	session_start();
	reset($_SESSION);
	require("../config.inc.php");
	require('../object.inc.php');
	unset($_SESSION['utente']);
	$_SESSION['utente'] = new utente;
	$_SESSION['utente']->init($_SESSION['id_utente']);
}
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#FF0000">
  <tr>
    <td>
    <div style="height:10px;"></div>
    <table width="100%" border="0" cellpadding="10" cellspacing="0">
      <tr>
        <td width="20">&nbsp;</td>
        <td class="titolo_manager">lingue</td>
        <td width="18"><input name="chiudi" type="button" class="bottone_manager" value="chiudi" onclick="document.getElementById('tool_lingua').style.display='none';" ></td>
        <td width="20">&nbsp;</td>
      </tr>
    </table>
<?php
$selected = array();
$stato = "";
$query_lingua = "SELECT * FROM lingua ORDER BY posizione";
$risultato_lingua = mysql_query($query_lingua);
while ($riga_lingua = mysql_fetch_array($risultato_lingua))
{
	$primario = "";
	if($riga_lingua['primario'] == "si")
	{
		$primario = "checked";
	}
	$attivo = "";
	if($riga_lingua['attivo'] == "si")
	{
		$attivo = "checked";
	}
	$selected[$riga_lingua['stato']] = "selected='selected'";
	if($stato != $riga_lingua['stato'])
	{
		$stato = $riga_lingua['stato'];
?>
    <div style="height:5px;"></div>
    <div id="linea_manager"></div>
<?php
	}
?>
    <div style="height:5px;"></div>
    <form name="form_lingua_<?php echo $riga_lingua['ID']; ?>" id="form_lingua_<?php echo $riga_lingua['ID']; ?>" method="post">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><table border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="20">&nbsp;</td>
              <td class="txt_manager">lingua</td>
              <td width="20">&nbsp;</td>
              <td class="txt_manager">path</td>
<?php
	if($_SESSION['utente']->manager)
	{
?>
              <td width="20">&nbsp;</td>
              <td class="txt_manager">&nbsp;</td>
<?php
	}
?>
              <td width="20">&nbsp;</td>
              <td class="txt_manager">&nbsp;</td>
            </tr>
            <tr>
              <td width="20">&nbsp;</td>
              <td><input name="lingua_lingua_<?php echo $riga_lingua['ID']; ?>" id="lingua_lingua_<?php echo $riga_lingua['ID']; ?>" type="text" class="input_manager" value="<?php echo $riga_lingua['lingua']; ?>" /></td>
              <td width="20">&nbsp;</td>
              <td><input name="lingua_path_<?php echo $riga_lingua['ID']; ?>" id="lingua_path_<?php echo $riga_lingua['ID']; ?>" type="text" class="input_manager" value="<?php echo $riga_lingua['path']; ?>" /></td>
<?php
	if($_SESSION['utente']->manager)
	{
?>
              <td width="20">&nbsp;</td>
              <td class="txt_manager"><input name="lingua_primario_<?php echo $riga_lingua['ID']; ?>" id="lingua_primario_<?php echo $riga_lingua['ID']; ?>" type="checkbox" class="check_manager" value="si" <?php echo $primario; ?> /><label for="lingua_primario_<?php echo $riga_lingua['ID']; ?>">primario</label></td>
<?php
	}
?>
              <td width="20">&nbsp;</td>
              <td class="txt_manager"><input name="lingua_attivo_<?php echo $riga_lingua['ID']; ?>" id="lingua_attivo_<?php echo $riga_lingua['ID']; ?>" type="checkbox" class="check_manager" value="si" <?php echo $attivo; ?> /><label for="lingua_attivo_<?php echo $riga_lingua['ID']; ?>">attivo</label></td>
            </tr>
          </table>
          </td>
          <td align="right" valign="bottom">
          <table border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td><a href="#"><img src="http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>manager/su.gif" alt="modifica" border="0" onclick="esegui('form_lingua_<?php echo $riga_lingua['ID']; ?>','lingua','su',<?php echo $riga_lingua['ID']; ?>)" /></a></td>
              <td width="20">&nbsp;</td>
              <td><a href="#"><img src="http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>manager/giu.gif" alt="cancella" border="0" onclick="esegui('form_lingua_<?php echo $riga_lingua['ID']; ?>','lingua','giu',<?php echo $riga_lingua['ID']; ?>)" /></a></td>
              <td width="20">&nbsp;</td>
              <td><input name="modifica" type="button" class="bottone_manager" value="modifica" onclick="esegui('form_lingua_<?php echo $riga_lingua['ID']; ?>','lingua','modifica',<?php echo $riga_lingua['ID']; ?>)" ></td>
              <td width="20">&nbsp;</td>
              <td><input name="cancella" type="button" class="bottone_manager" value="cancella" onclick="if( confirm('sei sicuro di voler cancellare il lingua: <?php echo $riga_lingua['nominativo']; ?>?') ) esegui('form_lingua_<?php echo $riga_lingua['ID']; ?>','lingua','cancella',<?php echo $riga_lingua['ID']; ?>)" /></td>
              <td width="20">&nbsp;</td>
            </tr>
          </table>
          </td>
          </tr>
      </table>
    </form>
	<div style="height:5px;"></div>
    <div id="linea_manager"></div>
    <div style="height:5px;"></div>
<?php
}
?>
    <form name="form_lingua_0" id="form_lingua_0" method="post">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td>
          <table border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="20">&nbsp;</td>
              <td class="txt_manager">lingua</td>
              <td width="20">&nbsp;</td>
              <td class="txt_manager">path</td>
<?php
if($_SESSION['utente']->manager)
{
?>
              <td width="20">&nbsp;</td>
              <td class="txt_manager">&nbsp;</td>
<?php
}
?>
              <td width="20">&nbsp;</td>
              <td class="txt_manager">&nbsp;</td>
            </tr>
            <tr>
              <td width="20">&nbsp;</td>
              <td><input name="lingua_lingua_0" id="lingua_lingua_0" type="text" class="input_manager" value="" placeholder="lingua" /></td>
              <td width="20">&nbsp;</td>
              <td><input name="lingua_path_0" id="lingua_path_0" type="text" class="input_manager" value="" placeholder="path" /></td>
<?php
if($_SESSION['utente']->manager)
{
?>
              <td width="20">&nbsp;</td>
              <td class="txt_manager"><input name="lingua_primario_0" id="lingua_primario_0" type="checkbox" class="check_manager" value="si" /><label for="lingua_primario_0">primario</label></td>
<?php
}
?>
              <td width="20">&nbsp;</td>
              <td class="txt_manager"><input name="lingua_attivo_0" id="lingua_attivo_0" type="checkbox" class="check_manager" value="si" /><label for="lingua_attivo_0">attivo</label></td>
            </tr>
          </table>
          </td>
          <td align="right" valign="bottom">
          <table border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td><input name="aggiungi" type="button" class="bottone_manager" value="aggiungi" onclick="esegui('form_lingua_0','lingua','aggiungi',0)" ></td>
              <td width="20">&nbsp;</td>
            </tr>
          </table>
          </td>
          </tr>
      </table>
    </form>
	<div style="height:10px;"></div>
    </td>
  </tr>
</table>
