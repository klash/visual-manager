<?php
class carrello
{
    var $sessione;
    var $id;
    var $ordine;
    var $esiste;
    var $totale;
    var $prezzo;
    var $carrello;
    var $prodotto;
    var $colore;
    var $taglia;
    var $qta_disponibile;
    var $qta_richiesta;
    var $settore;
	function init($sessione, $id)
	{
		$this->sessione = $sessione;
		$this->id = $id;
		$this->totale = 0;
		$this->prezzo = 0;
		$this->prodotto = array();
		$this->colore = array();
		$this->taglia = array();
		$this->qta = array();
		$this->settore = array();
		$this->esiste = 0;
		if($this->id > 0)
		{
			$query_carrello = "SELECT * FROM carrello WHERE session=\"".$this->sessione."\" AND cliente=".$this->id." AND acquisto = 'no' ORDER BY timestamp";
			$query_totale = "SELECT SUM(qta) As summa FROM carrello WHERE session=\"".$this->sessione."\" AND acquisto = 'no' AND cliente=".$this->id;
		}
		else
		{
			$query_carrello = "SELECT * FROM carrello WHERE session=\"".$this->sessione."\" AND acquisto = 'no' ORDER BY timestamp";
			$query_totale = "SELECT SUM(qta) As summa FROM carrello WHERE session=\"".$this->sessione."\" AND acquisto = 'no'";
		}
		$risultato_carrello = mysql_query($query_carrello);
		$trovato_carrello = mysql_num_rows($risultato_carrello);
		if ($trovato_carrello > 0)
		{
			$this->esiste = 1;
			$risultato_totale = mysql_query($query_totale);
			$riga_totale = mysql_fetch_array($risultato_totale);
			$this->totale = $riga_totale['summa'];
			while($riga_carrello = mysql_fetch_array($risultato_carrello))
			{
				$qry_prodotto = "SELECT colore.colore, prodotto.ID, prodotto.prezzo, magazzino.qta FROM magazzino, colore, prodotto WHERE magazzino.ID = ".$riga_carrello['magazzino']." AND colore.ID = magazzino.colore AND prodotto.ID = colore.prodotto";
				$res_prodotto = mysql_query($qry_prodotto);
				$trovato_prodotto = mysql_num_rows($res_prodotto);
				if($trovato_prodotto > 0)
				{
					$riga_prodotto = mysql_fetch_array($res_prodotto);
					$this->qta_disponibile[$riga_carrello['ID']] = $riga_prodotto['qta'];
					$this->qta_richiesta[$riga_carrello['ID']] = $riga_carrello['qta'];
					if($this->qta_disponibile[$riga_carrello['ID']] >= $this->qta_richiesta[$riga_carrello['ID']])
					{
						$this->prezzo += $riga_carrello['qta']*$riga_prodotto['prezzo'];
					}
					else
					{
						$query_disponibile = "UPDATE carrello SET ordine = 0 WHERE ID = ".$riga_carrello['ID'];
						$risultato_disponibile = mysql_query($query_disponibile);
					}
					$this->prodotto[$riga_carrello['ID']] = $riga_prodotto['ID'];
					$this->colore[$riga_carrello['ID']] = $riga_prodotto['colore'];
					$qry_settore = "SELECT settore FROM settore_prodotto WHERE prodotto = ".$riga_prodotto['ID'];
					$res_settore = mysql_query($qry_settore);
					$riga_settore = mysql_fetch_array($res_settore);
					$this->settore[$riga_carrello['ID']] = $riga_settore['settore'];
					$qry_taglia = "SELECT taglia.taglia FROM magazzino, taglia WHERE magazzino.ID = ".$riga_carrello['magazzino']." AND taglia.ID = magazzino.taglia";
					$res_taglia = mysql_query($qry_taglia);
					$trovato_taglia = mysql_num_rows($res_taglia);
					if($trovato_taglia > 0)
					{
						$riga_taglia = mysql_fetch_array($res_taglia);
						$this->taglia[$riga_carrello['ID']] = $riga_taglia['taglia'];
					}
				}
			}
		}
	}
}
?>