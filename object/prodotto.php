<?php
class prodotto
{
    var $id;
    var $esiste;
    var $settore;
    var $area;
    var $tipologia;
    var $marca;
    var $prodotto;
    var $descrizione;
    var $codice;
    var $prezzo;
    var $materiale;
    var $copertina;
    var $link;
	function init($id)
	{
		$this->id = $id;
		$this->esiste = 0;
		if($this->id > 0)
		{
			$query_prodotto = "SELECT * FROM prodotto WHERE ID = '".$this->id."'";
			$risultato_prodotto = mysql_query($query_prodotto);
			$trovato_prodotto = mysql_num_rows($risultato_prodotto);
			if ($trovato_prodotto > 0)
			{
				$this->esiste = 1;
				$riga_prodotto = mysql_fetch_array($risultato_prodotto);
				$query_settore_prodotto = "SELECT * FROM settore_prodotto WHERE prodotto = '".$riga_prodotto['settore']."'";
				$risultato_settore_prodotto = mysql_query($query_settore_prodotto);
				while ($riga_settore_prodotto = mysql_fetch_array($risultato_settore_prodotto))
				{
					$query_settore = "SELECT * FROM settore WHERE ID = '".$riga_settore_prodotto['settore']."'";
					$risultato_settore = mysql_query($query_settore);
					$riga_settore = mysql_fetch_array($risultato_settore);
					$this->settore[$riga_settore_prodotto['ID']]['ID'] = $riga_settore['ID'];
					$this->settore[$riga_settore_prodotto['ID']]['settore'] = $riga_settore['menu'];
				}
				$query_area = "SELECT * FROM area WHERE ID = '".$riga_prodotto['area']."'";
				$risultato_area = mysql_query($query_area);
				$riga_area = mysql_fetch_array($risultato_area);
				$this->area['ID'] = $riga_area['ID'];
				$this->area['area'] = $riga_area['menu'];
				$query_tipologia = "SELECT * FROM tipologia WHERE ID = '".$riga_prodotto['tipologia']."'";
				$risultato_tipologia = mysql_query($query_tipologia);
				$riga_tipologia = mysql_fetch_array($risultato_tipologia);
				$this->tipologia['ID'] = $riga_tipologia['ID'];
				$this->tipologia['tipologia'] = $riga_tipologia['menu'];
				$query_standard_taglia = "SELECT * FROM standard_taglia WHERE ID = '".$riga_prodotto['standard_taglia']."'";
				$risultato_standard_taglia = mysql_query($query_standard_taglia);
				$trovato_standard_taglia = mysql_num_rows($risultato_standard_taglia);
				if($trovato_standard_taglia > 0)
				{
					$riga_standard_taglia = mysql_fetch_array($risultato_standard_taglia);
					$this->standard_taglia['ID'] = $riga_standard_taglia['ID'];
					$this->standard_taglia['standard_taglia'] = $riga_standard_taglia['menu'];
				}
				else
				{
					$this->standard_taglia['ID'] = 0;
					$this->standard_taglia['standard_taglia'] = "";
				}
				$query_marchio = "SELECT * FROM marchio WHERE ID = '".$riga_prodotto['marchio']."'";
				$risultato_marchio = mysql_query($query_marchio);
				$riga_marchio = mysql_fetch_array($risultato_marchio);
				$this->marchio['ID'] = $riga_marchio['ID'];
				$this->marchio['marchio'] = $riga_marchio['marchio'];
				$this->prodotto = $riga_prodotto['prodotto'];
				$this->descrizione = $riga_prodotto['descrizione'];
				$this->codice = $riga_prodotto['codice'];
				$this->prezzo = $riga_prodotto['prezzo'];
				$this->materiale = $riga_prodotto['materiale'];
				$this->copertina = "file/prodotto_copertina_".$this->id."_".$riga_prodotto['copertina'];
				$this->link = $riga_area['permalink']."/".$riga_tipologia['permalink']."/".$riga_marchio['permalink']."/".$riga_prodotto['permalink'].".html";
			}
		}
	}
}
?>