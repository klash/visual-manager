<?php
class grafica
{
    var $directory;
    var $abs_path;
    var $tabella;
    var $id;
    var $nome;
    var $campo;
    var $title;
    var $original;
    var $path;
    var $classe;
    var $thumb;
    var $style;
	var $onclick;
    var $larghezza;
    var $altezza;
    var $dimensione;
    var $resize;
    var $javascript;
    var $esiste;
    var $linka;
    var $roll_over;
    var $roll_text;
    var $reload;
	function location($tabella, $id, $nome, $title)
	{
		$this->directory = $_SERVER['DOCUMENT_ROOT'].$_SESSION['path'];
		$this->abs_path = "http://".$_SERVER['SERVER_NAME'].$_SESSION['path'];
		$this->tabella = $tabella;
		$this->id = $id;
		$this->nome = str_replace("file_","",$nome);
		$this->campo = $nome;
		$this->title = htmlentities($title);
		$query = "SELECT * FROM ".$this->tabella." WHERE ID = '".$this->id."'";
//echo $query."<br>";
		$risultato = mysql_query($query);
		$riga = mysql_fetch_array($risultato);
		$this->original = $this->tabella."_".$this->nome."_".$this->id."_".$riga[$this->campo];
		$this->path = "file/".$this->original;
//echo $this->path."<br>";
		if (file_exists($this->directory.$this->path))
		{
			$this->esiste = 1;
			$this->dimensione = GetImageSize($this->directory.$this->path);
//		echo "dimensione[0]:".$this->dimensione[0]."<br>";
//		echo "dimensione[1]:".$this->dimensione[1]."<br>";
			$this->larghezza = $this->dimensione[0];
			$this->altezza = $this->dimensione[1];
		}
		else
		{
			$this->esiste = 0;
		}
	}
	function useit($valore, $tabella, $id, $nome, $title)
	{
		$this->directory = $_SERVER['DOCUMENT_ROOT'].$_SESSION['path'];
		$this->abs_path = "http://".$_SERVER['SERVER_NAME'].$_SESSION['path'];
		$this->tabella = $tabella;
		$this->id = $id;
		$this->nome = str_replace("file_","",$nome);
		$this->campo = $nome;
		$this->title = $title;
		$this->original = $this->tabella."_".$this->nome."_".$this->id."_".$valore;
		$this->path = "file/".$this->original;
		if (file_exists($this->directory.$this->path))
		{
			$this->esiste = 1;
			$this->dimensione = getimagesize($this->directory.$this->path);
//		echo "dimensione[0]:".$this->dimensione[0]."<br>";
//		echo "dimensione[1]:".$this->dimensione[1]."<br>";
			$this->larghezza = $this->dimensione[0];
			$this->altezza = $this->dimensione[1];
		}
		else
		{
			$this->esiste = 0;
		}
	}
	function classe($classe)
	{
		$this->classe = $classe;
	}
	function manca($image)
	{
		if ($this->esiste == 0)
		{
			$this->path = $image;
			$this->esiste = 1;
		}
	}
	function dimensioni($larghezza, $altezza)
	{
		$this->dimensione['full'] = $larghezza.":".$altezza;
		if($larghezza > 0)
		{
			$this->dimensione['larghezza'] = $larghezza;
		}
		else
		{
			$this->dimensione['larghezza'] = "auto";
		}
		if($altezza > 0)
		{
			$this->dimensione['altezza'] = $altezza;
		}
		else
		{
			$this->dimensione['altezza'] = "auto";
		}
		$this->path = "file/".$this->original;
	}
	function thumb($larghezza,$altezza)
	{
		$this->thumb['full'] = $larghezza.":".$altezza;
		if($larghezza > 0)
		{
			$this->dimensione['larghezza'] = $larghezza;
		}
		else
		{
			$this->dimensione['larghezza'] = "auto";
		}
		if($altezza > 0)
		{
			$this->dimensione['altezza'] = $altezza;
		}
		else
		{
			$this->dimensione['altezza'] = "auto";
		}
		$thumb_path = "file/thumb_".$this->original;
		if(file_exists($thumb_path))
		{
			$this->path = $thumb_path;
		}
	}
	function margin()
	{
		if ($this->esiste)
		{
			$area = $this->dimensione['larghezza']*$this->dimensione['altezza'];
			$ratio = floatval(@(($this->dimensione['larghezza']/$this->dimensione['altezza'])));
			list($real_width, $real_height) = getimagesize($this->directory.$this->path);
			if ($real_width > 0 && $real_height > 0)
			{
				$area_real = $real_width*$real_height;
				$real_ratio = floatval($real_width/$real_height);
				if($area > $area_real)
				{
					switch ($ratio)
					{
						case ($ratio === 0):
							if($this->dimensione['larghezza'] == "auto")
							{
								$real_height =$this->dimensione['altezza'];
								$real_width = intval($real_height*$real_ratio);
								$this->style['larghezza'] = "width:auto;";
								$this->style['altezza'] = "height:".$real_height."px;";
							}
							if($this->dimensione['altezza'] == "auto")
							{
								$real_width = $this->dimensione['larghezza'];
								$real_height = intval($real_width/$real_ratio);
								$this->style['larghezza'] = "width:".$real_width."px;";
								$this->style['altezza'] = "height:auto;";
							}
							break;
						case ($ratio < 1):
							switch ($real_ratio)
							{
								case ($real_ratio < 1):
									$real_width = $this->dimensione['larghezza'];
									$real_height = intval($real_width/$real_ratio);
									$this->style['larghezza'] = "width:".$real_width."px;";
									$this->style['altezza'] = "height:".$real_height."px;";
									break;
								default:
									$real_height =$this->dimensione['altezza'];
									$real_width = intval($real_height*$real_ratio);
									$this->style['larghezza'] = "width:".$real_width."px;";
									$this->style['altezza'] = "height:".$real_height."px;";
									break;
							}
							break;
						default:
							switch ($real_ratio)
							{
								case ($real_ratio <= 1):
									$real_width = $this->dimensione['larghezza'];
									$real_height = intval($real_width/$real_ratio);
									$this->style['larghezza'] = "width:".$real_width."px;";
									$this->style['altezza'] = "height:".$real_height."px;";
									break;
								default:
									$real_height =$this->dimensione['altezza'];
									$real_width = intval($real_height*$real_ratio);
									$this->style['larghezza'] = "width:".$real_width."px;";
									$this->style['altezza'] = "height:".$real_height."px;";
									break;
							}
					}
				}
				else
				{
					switch ($ratio)
					{
						case ($ratio === 0):
							if($this->dimensione['larghezza'] == "auto")
							{
								$real_height =$this->dimensione['altezza'];
								$real_width = intval($real_height*$real_ratio);
								$this->style['larghezza'] = "width:auto;";
								$this->style['altezza'] = "height:".$real_height."px;";
							}
							if($this->dimensione['altezza'] == "auto")
							{
								$real_width = $this->dimensione['larghezza'];
								$real_height = intval($real_width/$real_ratio);
								$this->style['larghezza'] = "width:".$real_width."px;";
								$this->style['altezza'] = "height:auto;";
							}
							break;
						case ($ratio < 1):
							switch ($real_ratio)
							{
								case ($real_ratio < 1):
									$real_width = $this->dimensione['larghezza'];
									$real_height = intval($real_width/$real_ratio);
									$this->style['larghezza'] = "width:".$real_width."px;";
									$this->style['altezza'] = "height:".$real_height."px;";
									break;
								default:
									$real_height = $this->dimensione['altezza'];
									$real_width = intval($real_height*$real_ratio);
									$this->style['larghezza'] = "width:".$real_width."px;";
									$this->style['altezza'] = "height:".$real_height."px;";
									break;
							}
							break;
						default:
							switch ($real_ratio)
							{
								case ($real_ratio < 1):
									$real_width = $this->dimensione['larghezza'];
									$real_height = intval($real_width/$real_ratio);
									$this->style['larghezza'] = "width:".$real_width."px;";
									$this->style['altezza'] = "height:".$real_height."px;";
									break;
								default:
									if($real_ratio > $ratio)
									{
										$real_height = $this->dimensione['altezza'];
										$real_width = intval($real_height*$real_ratio);
										$this->style['larghezza'] = "width:".$real_width."px;";
										$this->style['altezza'] = "height:".$real_height."px;";
									}
									else
									{
										$real_width = $this->dimensione['larghezza'];
										$real_height = intval($real_width/$real_ratio);
										$this->style['larghezza'] = "width:".$real_width."px;";
										$this->style['altezza'] = "height:".$real_height."px;";
									}
									break;
							}
					}
				}
				switch ($ratio)
				{
					case ($ratio === 0):
						break;
					case ($ratio < 1):
						switch ($real_ratio)
						{
							case ($real_ratio < 1):
								$delta = intval(($real_height - $this->dimensione['altezza']) / 2);
								$this->style['margin'] = "margin-top:-".$delta."px;";
								break;
							default:
								$delta = intval(($real_width - $this->dimensione['larghezza']) / 2);
								$this->style['margin'] = "margin-left:-".$delta."px;";
								break;
						}
						break;
					default:
						switch ($real_ratio)
						{
							case ($real_ratio > 1):
								if($real_ratio > $ratio)
								{
									$delta = intval(($real_width - $this->dimensione['larghezza']) / 2);
									$this->style['margin'] = "margin-left:-".abs($delta)."px;";
								}
								else
								{
									$delta = intval(($real_height - $this->dimensione['altezza']) / 2);
									$this->style['margin'] = "margin-top:-".$delta."px;";
								}
								break;
							default:
								$delta = intval(($real_height - $this->dimensione['altezza']) / 2);
								$this->style['margin'] = "margin-top:-".$delta."px;";
								break;
						}
						break;
				}
			}
		}
	}
	function linka($url,$target)
	{
		$this->linka['url'] = $url;
		$this->linka['target'] = $target;
	}
	function onclick($script)
	{
		$this->onclick = "onclick=\"".$script."\"";
	}
	function javascript($javascript)
	{
		$this->javascript = $javascript;
	}
	function roll($rollover)
	{
		if(count($rollover))
		{
			$this->roll_over = 1;
			$this->roll_text = $rollover;
		}
		else
		{
			$this->roll_over = 0;
		}
	}
	function reload($reload)
	{
		$this->reload = $reload;
	}
	function manager()
	{
		if($this->dimensione['larghezza'] != "auto")
		{
			$this->dimensione['larghezza'] .= "px";
		}
		if($this->dimensione['altezza'] != "auto")
		{
			$this->dimensione['altezza'] .= "px";
		}
		if ($this->esiste)
		{
			if (trim($this->linka['url']) != "")
			{
				if ($this->roll_over)
				{
					$query = "SELECT * FROM ".$this->tabella." WHERE ID = '".$this->id."'";
					$risultato = mysql_query($query);
					$riga = mysql_fetch_array($risultato);
					$rollovero = array();
					reset($this->roll_text);
					while (list($key, $val) = each($this->roll_text))
					{
						$path_roll = "file/".$this->tabella."_".$val."_".$this->id."_".$riga[$val];
						$rollovero[$key] = "'".$key."', '', '".$path_roll."',";
					}
?>
<div class="bg_grafica <?php echo $this->classe; ?>" style="width:<?php echo $this->dimensione['larghezza']; ?>; height:<?php echo $this->dimensione['altezza']; ?>; overflow:hidden;">
<a href="<?php echo $this->linka['url']; ?>" target="<?php echo $this->linka['target']; ?>"><img id="grafica_<?php echo $this->tabella; ?>_<?php echo $this->nome; ?>_<?php echo $this->id; ?>" original="<?php echo $this->abs_path; ?><?php echo $this->path; ?>" src="<?php echo $this->abs_path; ?><?php echo $this->path; ?>" title="<?php echo $this->title; ?>" alt="<?php echo $this->title; ?>" name="<?php echo $this->tabella; ?>_<?php echo $this->nome; ?>_<?php echo $this->id; ?>" style=" <?php echo $this->style['margin']; ?> <?php echo $this->style['larghezza']; ?> <?php echo $this->style['altezza']; ?>" border="0" oncontextmenu="Lightview.show({ url: '<?php echo $this->abs_path; ?>istant/grafica/modale.php?t=<?php echo $this->tabella; ?>&i=<?php echo $this->id; ?>&n=<?php echo $this->campo; ?>&d=<?php echo $this->dimensione['full']; ?>&m=<?php echo $this->thumb['full']; ?>&j=<?php echo $this->javascript; ?>', options: { width: 455, height: 490 }, type: 'iframe' });return false;" onMouseOver="MM_swapImage(<?php echo implode("", $rollovero); ?>0);" onMouseOut="MM_swapImgRestore()" <?php echo $this->onclick; ?> /></a>
</div>
<?php
				}
				else
				{
?>
<div class="bg_grafica <?php echo $this->classe; ?>" style="width:<?php echo $this->dimensione['larghezza']; ?>; height:<?php echo $this->dimensione['altezza']; ?>; overflow:hidden;">
<a href="<?php echo $this->linka['url']; ?>" target="<?php echo $this->linka['target']; ?>"><img id="grafica_<?php echo $this->tabella; ?>_<?php echo $this->nome; ?>_<?php echo $this->id; ?>" original="<?php echo $this->abs_path; ?><?php echo $this->path; ?>" src="<?php echo $this->abs_path; ?><?php echo $this->path; ?>" title="<?php echo $this->title; ?>" alt="<?php echo $this->title; ?>" name="<?php echo $this->tabella; ?>_<?php echo $this->nome; ?>_<?php echo $this->id; ?>" style=" <?php echo $this->style['margin']; ?> <?php echo $this->style['larghezza']; ?> <?php echo $this->style['altezza']; ?>" border="0" oncontextmenu="Lightview.show({ url: '<?php echo $this->abs_path; ?>istant/grafica/modale.php?t=<?php echo $this->tabella; ?>&i=<?php echo $this->id; ?>&n=<?php echo $this->campo; ?>&d=<?php echo $this->dimensione['full']; ?>&m=<?php echo $this->thumb['full']; ?>&j=<?php echo $this->javascript; ?>', options: { width: 455, height: 490 }, type: 'iframe' });return false;" <?php echo $this->onclick; ?> /></a>
</div>
<?php
				}
			}
			else
			{
				if ($this->roll_over)
				{
					$query = "SELECT * FROM ".$this->tabella." WHERE ID = '".$this->id."'";
					$risultato = mysql_query($query);
					$riga = mysql_fetch_array($risultato);
					$rollovero = array();
					reset($this->roll_text);
					while (list($key, $val) = each($this->roll_text))
					{
						$path_roll = "file/".$this->tabella."_".$val."_".$this->id."_".$riga[$val];
						$rollovero[$key] = "'".$key."', '', '".$path_roll."',";
					}
?>
<div class="bg_grafica <?php echo $this->classe; ?>" style="width:<?php echo $this->dimensione['larghezza']; ?>; height:<?php echo $this->dimensione['altezza']; ?>; overflow:hidden;">
<img id="grafica_<?php echo $this->tabella; ?>_<?php echo $this->nome; ?>_<?php echo $this->id; ?>" original="<?php echo $this->abs_path; ?><?php echo $this->path; ?>" src="<?php echo $this->abs_path; ?><?php echo $this->path; ?>" title="<?php echo $this->title; ?>" alt="<?php echo $this->title; ?>" name="<?php echo $this->tabella; ?>_<?php echo $this->nome; ?>_<?php echo $this->id; ?>" style=" <?php echo $this->style['margin']; ?> <?php echo $this->style['larghezza']; ?> <?php echo $this->style['altezza']; ?>" border="0" oncontextmenu="Lightview.show({ url: '<?php echo $this->abs_path; ?>istant/grafica/modale.php?t=<?php echo $this->tabella; ?>&i=<?php echo $this->id; ?>&n=<?php echo $this->campo; ?>&d=<?php echo $this->dimensione['full']; ?>&m=<?php echo $this->thumb['full']; ?>&j=<?php echo $this->javascript; ?>', options: { width: 455, height: 490 }, type: 'iframe' });return false;" onMouseOver="MM_swapImage(<?php echo implode("", $rollovero); ?>0);" onMouseOut="MM_swapImgRestore()" <?php echo $this->onclick; ?> />
</div>
<?php
				}
				else
				{
?>
<div class="bg_grafica <?php echo $this->classe; ?>" style="width:<?php echo $this->dimensione['larghezza']; ?>; height:<?php echo $this->dimensione['altezza']; ?>; overflow:hidden;">
<img id="grafica_<?php echo $this->tabella; ?>_<?php echo $this->nome; ?>_<?php echo $this->id; ?>" original="<?php echo $this->abs_path; ?><?php echo $this->path; ?>" src="<?php echo $this->abs_path; ?><?php echo $this->path; ?>" title="<?php echo $this->title; ?>" alt="<?php echo $this->title; ?>" name="<?php echo $this->tabella; ?>_<?php echo $this->nome; ?>_<?php echo $this->id; ?>" style=" <?php echo $this->style['margin']; ?> <?php echo $this->style['larghezza']; ?> <?php echo $this->style['altezza']; ?>" border="0" oncontextmenu="Lightview.show({ url: '<?php echo $this->abs_path; ?>istant/grafica/modale.php?t=<?php echo $this->tabella; ?>&i=<?php echo $this->id; ?>&n=<?php echo $this->campo; ?>&d=<?php echo $this->dimensione['full']; ?>&m=<?php echo $this->thumb['full']; ?>&j=<?php echo $this->javascript; ?>', options: { width: 455, height: 490 }, type: 'iframe' });return false;" <?php echo $this->onclick; ?> />
</div>
<?php
				}
			}
		}
		else
		{
			if($this->dimensione['altezza'] == "auto")
			{
				$altezza = $this->dimensione['larghezza'];
			}
			else
			{
				$altezza = $this->dimensione['altezza'];
			}
?>
<div class="bg_grafica <?php echo $this->classe; ?>" style="width:<?php echo $this->dimensione['larghezza']; ?>; height:<?php echo $altezza; ?>; overflow:hidden;">
<img id="grafica_<?php echo $this->tabella; ?>_<?php echo $this->nome; ?>_<?php echo $this->id; ?>" original="" src="<?php echo $this->abs_path; ?>manager/dot.gif" title="<?php echo $this->title; ?>" alt="<?php echo $this->title; ?>" name="<?php echo $this->tabella; ?>_<?php echo $this->nome; ?>_<?php echo $this->id; ?>" border="0" width="<?php echo $this->dimensione['larghezza']; ?>" height="<?php echo $altezza; ?>" style=" <?php echo $this->style['margin']; ?> <?php echo $this->style['larghezza']; ?> <?php echo $this->style['altezza']; ?>" oncontextmenu="Lightview.show({ url: '<?php echo $this->abs_path; ?>istant/grafica/modale.php?t=<?php echo $this->tabella; ?>&i=<?php echo $this->id; ?>&n=<?php echo $this->campo; ?>&d=<?php echo $this->dimensione['full']; ?>&m=<?php echo $this->thumb['full']; ?>&j=<?php echo $this->javascript; ?>', options: { width: 455, height: 490 }, type: 'iframe' });return false;" <?php echo $this->onclick; ?> />
</div>
<?php
		}
	}
	function vedi()
	{
		if($this->dimensione['larghezza'] != "auto")
		{
			$this->dimensione['larghezza'] .= "px";
		}
		if($this->dimensione['altezza'] != "auto")
		{
			$this->dimensione['altezza'] .= "px";
		}
		if ($this->esiste)
		{
			if (trim($this->linka['url']) != "")
			{
				if ($this->roll_over)
				{
					$query = "SELECT * FROM ".$this->tabella." WHERE ID = '".$this->id."'";
					$risultato = mysql_query($query);
					$riga = mysql_fetch_array($risultato);
					$rollovero = array();
					reset($this->roll_text);
					while (list($key, $val) = each($this->roll_text))
					{
						$path_roll = "file/".$this->tabella."_".$val."_".$this->id."_".$riga[$val];
						$rollovero[$key] = "'".$key."', '', '".$path_roll."',";
					}
?>
<div class="<?php echo $this->classe; ?>" style="width:<?php echo $this->dimensione['larghezza']; ?>; height:<?php echo $this->dimensione['altezza']; ?>; overflow:hidden;">
<a href="<?php echo $this->linka['url']; ?>" target="<?php echo $this->linka['target']; ?>"><img original="<?php echo $this->abs_path; ?><?php echo $this->path; ?>" src="<?php echo $this->abs_path; ?><?php echo $this->path; ?>" title="<?php echo $this->title; ?>" alt="<?php echo $this->title; ?>" name="<?php echo $this->tabella; ?>_<?php echo $this->nome; ?>_<?php echo $this->id; ?>" border="0" style=" <?php echo $this->style['margin']; ?> <?php echo $this->style['larghezza']; ?> <?php echo $this->style['altezza']; ?>" id="<?php echo $this->tabella; ?>_<?php echo $this->nome; ?>_<?php echo $this->id; ?>" onMouseOver="MM_swapImage(<?php echo implode("", $rollovero); ?>0);" onMouseOut="MM_swapImgRestore()" <?php echo $this->onclick; ?> /></a>
</div>
<?php
				}
				else
				{
?>
<div class="<?php echo $this->classe; ?>" style="width:<?php echo $this->dimensione['larghezza']; ?>; height:<?php echo $this->dimensione['altezza']; ?>; overflow:hidden;">
<a href="<?php echo $this->linka['url']; ?>" target="<?php echo $this->linka['target']; ?>"><img original="<?php echo $this->abs_path; ?><?php echo $this->path; ?>" src="<?php echo $this->abs_path; ?><?php echo $this->path; ?>" title="<?php echo $this->title; ?>" alt="<?php echo $this->title; ?>" name="<?php echo $this->tabella; ?>_<?php echo $this->nome; ?>_<?php echo $this->id; ?>" border="0" style=" <?php echo $this->style['margin']; ?> <?php echo $this->style['larghezza']; ?> <?php echo $this->style['altezza']; ?>" id="<?php echo $this->tabella; ?>_<?php echo $this->nome; ?>_<?php echo $this->id; ?>" <?php echo $this->onclick; ?> /></a>
</div>
<?php
				}
			}
			else
			{
				if ($this->roll_over)
				{
					$query = "SELECT * FROM ".$this->tabella." WHERE ID = '".$this->id."'";
					$risultato = mysql_query($query);
					$riga = mysql_fetch_array($risultato);
					$rollovero = array();
					reset($this->roll_text);
					while (list($key, $val) = each($this->roll_text))
					{
						$path_roll = "file/".$this->tabella."_".$val."_".$this->id."_".$riga[$val];
						$rollovero[$key] = "'".$key."', '', '".$path_roll."',";
					}
?>
<div class="<?php echo $this->classe; ?>" style="width:<?php echo $this->dimensione['larghezza']; ?>; height:<?php echo $this->dimensione['altezza']; ?>; overflow:hidden;">
<img original="<?php echo $this->abs_path; ?><?php echo $this->path; ?>" src="<?php echo $this->abs_path; ?><?php echo $this->path; ?>" title="<?php echo $this->title; ?>" alt="<?php echo $this->title; ?>" name="<?php echo $this->tabella; ?>_<?php echo $this->nome; ?>_<?php echo $this->id; ?>" border="0" style=" <?php echo $this->style['margin']; ?> <?php echo $this->style['larghezza']; ?> <?php echo $this->style['altezza']; ?>" id="<?php echo $this->tabella; ?>_<?php echo $this->nome; ?>_<?php echo $this->id; ?>" onMouseOver="MM_swapImage(<?php echo implode("", $rollovero); ?>0);" onMouseOut="MM_swapImgRestore()" <?php echo $this->onclick; ?> />
</div>
<?php
				}
				else
				{
?>
<div class="<?php echo $this->classe; ?>" style="width:<?php echo $this->dimensione['larghezza']; ?>; height:<?php echo $this->dimensione['altezza']; ?>; overflow:hidden;">
<img original="<?php echo $this->abs_path; ?><?php echo $this->path; ?>" src="<?php echo $this->abs_path; ?><?php echo $this->path; ?>" title="<?php echo $this->title; ?>" alt="<?php echo $this->title; ?>" name="<?php echo $this->tabella; ?>_<?php echo $this->nome; ?>_<?php echo $this->id; ?>" border="0" style=" <?php echo $this->style['margin']; ?> <?php echo $this->style['larghezza']; ?> <?php echo $this->style['altezza']; ?>" id="<?php echo $this->tabella; ?>_<?php echo $this->nome; ?>_<?php echo $this->id; ?>" <?php echo $this->onclick; ?> />
</div>
<?php
				}
			}
?>
<?php
		}
	}
	function plus($extra, $title)
	{
		$this->title = $title;
		if($this->altezza == 0)
		{
			$this->altezza = 50;
		}
?>
<div class="bg_grafica <?php echo $this->classe; ?>"><img id="grafica_<?php echo $this->tabella; ?>_<?php echo $this->nome; ?>_0" src="manager/dot.gif" title="<?php echo $this->title; ?>" alt="<?php echo $this->title; ?>" name="<?php echo $this->tabella; ?>_<?php echo $this->nome; ?>_<?php echo $this->id; ?>" width="<?php echo $this->larghezza; ?>" height="<?php echo $this->altezza; ?>" border="0" oncontextmenu="Lightview.show({ url: '<?php echo $this->abs_path; ?>istant/grafica/modale.php?t=<?php echo $this->tabella; ?>&i=<?php echo $this->id; ?>&n=<?php echo $this->campo; ?>&d=<?php echo $this->dimensione['full']; ?>&m=<?php echo $this->thumb['full']; ?>&e=<?php echo $extra; ?>&j=<?php echo $this->javascript; ?>', options: { width: 455, height: 490 }, type: 'iframe' });return false;" /></div>
<?php
	}
}
?>