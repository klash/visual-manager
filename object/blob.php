<?php
class blob
{
    var $lingua;
    var $trovato;
    var $tabella;
    var $id;
    var $nome;
    var $classe;
    var $testo;
    var $larghezza;
    var $altezza;
    var $esiste;
    var $reload;
	function lingua($lingua)
	{
		$this->lingua = $lingua;
	}
	function init($tabella, $id, $nome, $classe)
	{
		$this->tabella = $tabella;
		$this->id = $id;
		$this->nome = $nome;
		$this->classe = $classe;
		if($this->id > 0)
		{
			$this->trovato = 0;
			if($this->lingua > 0)
			{
				$query = "SELECT * FROM ".$this->tabella." WHERE primario = '".$this->id."' AND lingua = '".$this->lingua."'";
				$risultato = mysql_query($query);
				$this->trovato = mysql_num_rows($risultato);
				if($this->trovato > 0)
				{
					$riga = mysql_fetch_array($risultato);
				}
				else
				{
					$query = "SELECT * FROM ".$this->tabella." WHERE ID = '".$this->id."'";
					$risultato = mysql_query($query);
					$riga = mysql_fetch_array($risultato);
				}
			}
			else
			{
				$query = "SELECT * FROM ".$this->tabella." WHERE ID = '".$this->id."'";
				$risultato = mysql_query($query);
				$riga = mysql_fetch_array($risultato);
				$this->trovato = mysql_num_rows($risultato);
			}
			$this->testo = stripslashes(trim($riga[$this->nome]));
		}
		if($this->testo == "")
		{
			if($this->lingua > 0)
			{
				$query = "SELECT * FROM ".$this->tabella." WHERE ID = '".$this->id."'";
				$risultato = mysql_query($query);
				$riga = mysql_fetch_array($risultato);
				$this->testo = stripslashes(trim($riga[$this->nome]));
				if($this->testo == "")
				{
					$this->testo = $nome;
					$this->esiste = 0;
				}
				else
				{
					$this->esiste = 1;
				}
			}
			else
			{
				$this->testo = $nome;
				$this->esiste = 0;
			}
		}
		else
		{
			$this->esiste = 1;
		}
	}
	function useit($valore, $tabella, $id, $nome, $classe)
	{
		$this->tabella = $tabella;
		$this->id = $id;
		$this->nome = $nome;
		$this->classe = $classe;
		$this->testo = trim($valore);
		$this->testo = stripslashes($this->testo);
		if ($this->testo == "")
		{
			$this->testo = $nome;
			$this->esiste = 0;
		}
		else
		{
			$this->esiste = 1;
		}
	}
	function dimensioni($larghezza, $altezza)
	{
		if($larghezza > 0)
		{
			$this->larghezza = $larghezza."px";
		}
		else
		{
			$this->larghezza = "auto";
		}
		if($altezza > 0)
		{
			$this->altezza = $altezza."px";
		}
		else
		{
			$this->altezza = "auto";
		}
	}
	function paragraph($paragraph)
	{
		if($paragraph == "+")
		{
			$this->testo = "<p>".$this->testo."</p>";
		}
		if($paragraph == "-")
		{
			$this->testo = str_replace("<p>","",$this->testo);
			$this->testo = str_replace("</p>","",$this->testo);
		}
	}
	function reload($reload)
	{
		$this->reload = $reload;
	}
	function visualizza_breve($parole,$continua)
	{
		$testo = visualizza_rientri($this->testo);
		$riunite = "";
		$separate = explode( " ",$testo);
		for ($i = 0; $i <= $parole; $i++) {
			$riunite .= $separate[$i]." ";
		}
		$this->testo = trim($riunite);
		$this->testo = strip_tags($this->testo);
		$this->testo .= " ".$continua;
	}
	function manager()
	{
		if ($this->testo == "")
		{
			$this->testo = "&nbsp;";
		}
?>
<div id="blob_<?php echo $this->tabella; ?>_<?php echo $this->nome; ?>_<?php echo $this->id; ?>" valign="top" class="bg_blob <?php echo $this->classe; ?>" style="width:<?php echo $this->larghezza; ?>; height:<?php echo $this->altezza; ?>;" oncontextmenu="Lightview.show({ url: 'http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>istant/blob/modale.php?t=<?php echo $this->tabella; ?>&i=<?php echo $this->id; ?>&n=<?php echo $this->nome; ?>', options: { width: 640, height: 600 }, type: 'iframe' });return false;"><?php echo $this->testo; ?></div>
<?php
	}
	function vedi()
	{
		if ($this->esiste)
		{
?>
<div style="width:<?php echo $this->larghezza; ?>; height:<?php echo $this->altezza; ?>;" class="<?php echo $this->classe; ?>"><?php echo $this->testo; ?></div>
<?php
		}
	}
	function plus($extra, $testo)
	{
		if (trim($testo) == "")
		{
			$this->testo = "&nbsp;";
		}
		else
		{
			$this->testo = $testo;
		}
?>
<div id="blob_<?php echo $this->tabella; ?>_<?php echo $this->nome; ?>_0" style="width:<?php echo $this->larghezza; ?>; height:<?php echo $this->altezza; ?>;" valign="top" class="bg_blob <?php echo $this->classe; ?>" oncontextmenu="Lightview.show({ url: 'http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>popup/blob.php?t=<?php echo $this->tabella; ?>&i=<?php echo $this->id; ?>&n=<?php echo $this->nome; ?>&e=<?php echo $extra; ?>', options: { width: 640, height: 600 }, type: 'iframe' });return false;"><?php echo $this->testo; ?></div>
<?php
	}
}
?>