<?php
class traduzione
{
    var $chiave;
    var $lingua;
    var $classe;
    var $primario;
    var $id;
    var $traduzione;
    var $permalink;
    var $reload;
    var $db_link;
	function init($chiave,$lingua,$classe,$db_link)
	{
		$this->chiave = $chiave;
		$this->lingua = $lingua;
		$this->classe = $classe;
		$this->db_link = $db_link;
		$query = "SELECT * FROM traduzione WHERE LOWER(chiave) = '".strtolower(trim($this->chiave))."'";
//echo $query."<br>";
		$risultato = mysql_query($query);
		$trovato = mysql_num_rows($risultato);
		if($trovato > 0)
		{
			$riga = mysql_fetch_array($risultato);
			$this->primario = $riga['primario'];
		}
		else
		{
			$this->permalink = toAscii($this->chiave, array('*','`',"'",'"','“','”'));
//			$this->permalink = sanitize_title_with_dashes($this->chiave);
			$indice = array();
			$query_lingua = "SELECT * FROM lingua WHERE attivo = 'si' ORDER BY posizione";
			$risultato_lingua = mysql_query($query_lingua);
			while ($riga_lingua = mysql_fetch_array($risultato_lingua))
			{
				$query_aggiungi = "INSERT INTO traduzione (primario,lingua,chiave,testo,permalink) VALUES ('0', '".$riga_lingua['ID']."', '".addslashes($this->chiave)."', '".addslashes($this->chiave)."', '".$this->permalink."')";
//echo $query_aggiungi."<br>";
				$risultato_aggiungi = mysql_query($query_aggiungi);
				$ultimo = mysql_insert_id($this->db_link);
				array_push($indice, $ultimo);
				if($riga_lingua['primario'] == 'si')
				{
					$this->primario = $ultimo;
				}
			}
			reset($indice);
			while (list($key, $val) = each($indice))
			{
				$query_modifica = "UPDATE traduzione SET primario = '".$this->primario."' WHERE ID = '".$val."'";
//echo $query_modifica."<br>";
				$risultato_modifica = mysql_query($query_modifica);
			}
		}
		$query = "SELECT * FROM traduzione WHERE primario = '".$this->primario."' AND lingua = '".$this->lingua."'";
//echo $query;
		$risultato = mysql_query($query);
		$riga = mysql_fetch_array($risultato);
		$this->id = $riga['ID'];
		$this->traduzione = trim($riga['testo']);
		$this->permalink = $riga['permalink'];
	}
	function pre($pre)
	{
		$this->traduzione = $pre.$this->traduzione;
	}
	function post($post)
	{
		$this->traduzione = $this->traduzione.$post;
	}
	function reload($reload)
	{
		$this->reload = $reload;
	}
	function hyperlink($hyperlink,$target,$tipo)
	{
		$this->hyperlink = $hyperlink;
		$this->target = $target;
		$this->tipo = $tipo;
	}
	function onclick($script)
	{
		$this->onclick = "onclick=\"".$script."\"";
	}
	function stile($stile)
	{
		switch ($stile)
		{
		   case "upper":
				$this->traduzione = strtoupper($this->traduzione);
				break;
		   case "lower":
				$this->traduzione = strtolower($this->traduzione);
				break;
		   case "bold":
				$this->traduzione = "<strong>".$this->traduzione."</strong>";
				break;
		   case "italic":
				$this->traduzione = "<em>".$this->traduzione."</em>";
				break;
		}
	}
	function db_link($db_link)
	{
	}
	function vedi()
	{
		if ($this->hyperlink != "")
		{
			if ($this->tipo == "ext")
			{
				$this->testo = str_replace("http://","",$this->testo);
				$this->hyperlink = "http://".str_replace("http://","",$this->hyperlink);
			}
?>
<a href="<?php echo $this->hyperlink; ?>" class="<?php echo $this->classe; ?>" target="<?php echo $this->target; ?>" <?php echo $this->onclick; ?>><?php echo $this->traduzione; ?></a>
<?php
		}
		else
		{
?>
<span valign="top" class="<?php echo $this->classe; ?>" <?php echo $this->onclick; ?>><?php echo $this->traduzione; ?></span>
<?php
		}
	}
	function manager()
	{
		if ($this->hyperlink != "")
		{
			if ($this->tipo == "ext")
			{
				$this->testo = str_replace("http://","",$this->testo);
				$this->hyperlink = "http://".str_replace("http://","",$this->hyperlink);
			}
?>
<a id="translate_traduzione_testo_<?php echo $this->primario; ?>" href="<?php echo $this->hyperlink; ?>" class="bg_varchar <?php echo $this->classe; ?>" target="<?php echo $this->target; ?>" <?php echo $this->onclick; ?> oncontextmenu="Lightview.show({ url:'http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>istant/translate/modale.php?i=<?php echo $this->primario; ?>&l=<?php echo $this->lingua; ?>', options: { width: 455, height: 425 }, type: 'iframe' });return false;"><?php echo $this->traduzione; ?></a>
<?php
		}
		else
		{
?>
<span id="translate_traduzione_testo_<?php echo $this->primario; ?>" valign="top" class="bg_varchar <?php echo $this->classe; ?>" <?php echo $this->onclick; ?> oncontextmenu="Lightview.show({ url:'http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>istant/translate/modale.php?i=<?php echo $this->primario; ?>&l=<?php echo $this->lingua; ?>', options: { width: 455, height: 425 }, type: 'iframe' });return false;"><?php echo $this->traduzione; ?></span>
<?php
		}
	}
	function testo()
	{
		return $this->traduzione;
	}
}
?>