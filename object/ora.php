<?php
class ora
{
    var $tabella;
    var $id;
    var $nome;
    var $classe;
	var $valore;
	var $ora;
	var $minuti;
	var $vedi;
	var $esiste;
	var $testo;
	var $reload;
	function init($tabella, $id, $nome, $classe)
	{
		$this->tabella = $tabella;
		$this->id = $id;
		$this->nome = $nome;
		$this->classe = $classe;
		$query = "SELECT ".$nome." AS valore, HOUR(".$nome.") AS ora, MINUTE(".$nome.") AS minuti, SECOND(".$nome.") AS secondi FROM ".$this->tabella." WHERE ID = '".$this->id."'";
		$risultato = mysql_query($query);
		$riga = mysql_fetch_array($risultato);
		$this->valore = trim($riga['valore']);
		$this->ora = $riga['ora'];
		$this->minuti = $riga['minuti'];
		$this->secondi = $riga['secondi'];
		if (intval($this->ora) >= 0 && intval($this->ora) <= 23 && intval($this->minuti) >= 0 && intval($this->minuti) <= 59 && $this->valore != NULL)
		{
		 	$this->esiste = 1;
		}
		else
		{
		 	$this->esiste = 0;
		}
	}
	function formato($formato)
	{
		if ($this->esiste)
		{
			$formato = str_replace("hh",$this->ora,$formato);
			$formato = str_replace("mm",$this->minuti,$formato);
			$formato = str_replace("ss",$this->secondi,$formato);
			$this->vedi = $formato;
		}
		else
		{
			$this->vedi = $formato;
		}
	}
	function reload($reload)
	{
		$this->reload = $reload;
	}
	function manager()
	{
?>
<span class="bg_time"><span valign="top" class="<?php echo $this->classe; ?>" oncontextmenu="MM_openBrWindow('<?php echo $_SESSION['path']; ?>popup/ora.php?t=<?php echo $this->tabella; ?>&i=<?php echo $this->id; ?>&n=<?php echo $this->nome; ?>&r=<?php echo $this->reload; ?>','','width=455,height=320');return false;"><?php echo $this->vedi; ?></span></span>
<?php
	}
	function vedi()
	{
		if ($this->esiste)
		{
?>
<span valign="top" class="<?php echo $this->classe; ?>"><?php echo $this->vedi; ?></span>
<?php
		}
	}
	function plus($extra, $testo)
	{
		$this->testo = $testo;
?>
<span class="bg_time"><span valign="top" class="<?php echo $this->classe; ?>" oncontextmenu="MM_openBrWindow('<?php echo $_SESSION['path']; ?>popup/ora.php?t=<?php echo $this->tabella; ?>&i=<?php echo $this->id; ?>&n=<?php echo $this->nome; ?>&e=<?php echo $extra; ?>&r=<?php echo $this->reload; ?>','','width=455,height=320');return false;"><?php echo $this->testo; ?></span></span>
<?php
	}
}
?>