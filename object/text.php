<?php
class varchar
{
    var $lingua;
    var $trovato;
    var $tabella;
    var $id;
    var $nome;
    var $classe;
    var $testo;
    var $hyperlink;
    var $target;
	var $onclick;
    var $tipo;
    var $ico;
    var $posizione;
    var $su;
    var $giu;
    var $esiste;
    var $stato;
    var $secure;
    var $reload;
	function lingua($lingua)
	{
		$this->lingua = $lingua;
	}
	function init($tabella, $id, $nome, $classe)
	{
		$this->tabella = $tabella;
		$this->id = $id;
		$this->nome = $nome;
		$this->classe = $classe;
		$this->testo == "";
		if($this->id > 0)
		{
			$this->trovato = 0;
			if($this->lingua > 0)
			{
				$query = "SELECT * FROM ".$this->tabella." WHERE primario = '".$this->id."' AND lingua = '".$this->lingua."'";
				$risultato = mysql_query($query);
				$this->trovato = mysql_num_rows($risultato);
				if($this->trovato > 0)
				{
					$riga = mysql_fetch_array($risultato);
				}
				else
				{
					$query = "SELECT * FROM ".$this->tabella." WHERE ID = '".$this->id."'";
					$risultato = mysql_query($query);
					$riga = mysql_fetch_array($risultato);
				}
			}
			else
			{
				$query = "SELECT * FROM ".$this->tabella." WHERE ID = '".$this->id."'";
				$risultato = mysql_query($query);
				$riga = mysql_fetch_array($risultato);
				$this->trovato = mysql_num_rows($risultato);
			}
			$this->testo = stripslashes(trim($riga[$this->nome]));
		}
		if($this->testo == "")
		{
			if($this->lingua > 0)
			{
				$query = "SELECT * FROM ".$this->tabella." WHERE ID = '".$this->id."'";
				$risultato = mysql_query($query);
				$riga = mysql_fetch_array($risultato);
				$this->testo = stripslashes(trim($riga[$this->nome]));
				if($this->testo == "")
				{
					$this->testo = $nome;
					$this->esiste = 0;
				}
				else
				{
					$this->esiste = 1;
				}
			}
			else
			{
				$this->testo = $nome;
				$this->esiste = 0;
			}
		}
		else
		{
			$this->esiste = 1;
		}
	}
	function useit($valore, $tabella, $id, $nome, $classe)
	{
		$this->tabella = $tabella;
		$this->id = $id;
		$this->nome = $nome;
		$this->classe = $classe;
		if($this->id > 0)
		{
			$this->testo = stripslashes(trim($valore));
		}
		if($this->testo == "")
		{
			$this->testo = $nome;
			$this->esiste = 0;
		}
		else
		{
			$this->esiste = 1;
		}
	}
	function h($numero)
	{
		$this->testo = "<H".$numero." class='bg_varchar'>".$pre.$this->testo."</H".$numero.">";
	}
	function pre($pre)
	{
		$this->testo = $pre.$this->testo;
	}
	function post($post)
	{
		$this->testo = $this->testo.$post;
	}
	function reload($reload)
	{
		$this->reload = $reload;
	}
	function hyperlink($hyperlink,$target,$tipo)
	{
		$this->hyperlink = $hyperlink;
		$this->target = $target;
		$this->tipo = $tipo;
	}
	function onclick($script)
	{
		$this->onclick = "onclick=\"".$script."\"";
	}
	function stile($stile)
	{
		switch ($stile)
		{
		   case "upper":
				$this->testo = strtoupper($this->testo);
				break;
		   case "lower":
				$this->testo = strtolower($this->testo);
				break;
		   case "bold":
				$this->testo = "<strong>".$this->testo."</strong>";
				break;
		   case "italic":
				$this->testo = "<em>".$this->testo."</em>";
				break;
		}
	}
	function vedi()
	{
		if ($this->esiste)
		{
			if ($this->hyperlink != "")
			{
				if ($this->tipo == "ext")
				{
					$this->testo = str_replace("http://","",$this->testo);
					$this->hyperlink = "http://".str_replace("http://","",$this->hyperlink);
?>
<span valign="top"><a href="<?php echo $this->hyperlink; ?>" class="<?php echo $this->classe; ?>" target="<?php echo $this->target; ?>" <?php echo $this->onclick; ?>><?php echo $this->testo; ?></a></span>
<?php
				}
				else
				{
?>
<span valign="top"><a href="<?php echo $this->hyperlink; ?>" class="<?php echo $this->classe; ?>" target="<?php echo $this->target; ?>" <?php echo $this->onclick; ?>><?php echo $this->testo; ?></a></span>
<?php
				}
			}
			else
			{
?>
<span valign="top" class="<?php echo $this->classe; ?>" <?php echo $this->onclick; ?>><?php echo $this->testo; ?></span>
<?php
			}
		}
	}
	function manager()
	{
		if ($this->hyperlink != "")
		{
			if ($this->tipo == "ext")
			{
				$this->testo = str_replace("http://","",$this->testo);
				$this->hyperlink = "http://".str_replace("http://","",$this->hyperlink);
?>
<span id="varchar_<?php echo $this->tabella; ?>_<?php echo $this->nome; ?>_<?php echo $this->id; ?>" class="bg_varchar" valign="top" oncontextmenu="Lightview.show({ url: 'http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>istant/text/modale.php?t=<?php echo $this->tabella; ?>&i=<?php echo $this->id; ?>&n=<?php echo $this->nome; ?>', options: { width: 455, height: 425 }, type: 'iframe' });return false;"><a href="<?php echo $this->hyperlink; ?>" class="<?php echo $this->classe; ?>" target="<?php echo $this->target; ?>" <?php echo $this->onclick; ?>><?php echo $this->testo; ?></a></span>
<?php
			}
			else
			{
?>
<span id="varchar_<?php echo $this->tabella; ?>_<?php echo $this->nome; ?>_<?php echo $this->id; ?>" class="bg_varchar" valign="top" oncontextmenu="Lightview.show({ url: 'http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>istant/text/modale.php?t=<?php echo $this->tabella; ?>&i=<?php echo $this->id; ?>&n=<?php echo $this->nome; ?>', options: { width: 455, height: 425 }, type: 'iframe' });return false;"><a href="<?php echo $this->hyperlink; ?>" class="<?php echo $this->classe; ?>" target="<?php echo $this->target; ?>" <?php echo $this->onclick; ?>><?php echo $this->testo; ?></a></span>
<?php
			}
		}
		else
		{
?>
<span id="varchar_<?php echo $this->tabella; ?>_<?php echo $this->nome; ?>_<?php echo $this->id; ?>" class="bg_varchar <?php echo $this->classe; ?>" valign="top"  oncontextmenu="Lightview.show({ url: 'http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>istant/text/modale.php?t=<?php echo $this->tabella; ?>&i=<?php echo $this->id; ?>&n=<?php echo $this->nome; ?>', options: { width: 455, height: 425 }, type: 'iframe' });return false" <?php echo $this->onclick; ?>><?php echo $this->testo; ?></span>
<?php
		}
	}
	function plus($extra, $testo)
	{
		$this->testo = $testo;
		if ($this->hyperlink > 0)
		{
?>
<span id="varchar_<?php echo $this->tabella; ?>_<?php echo $this->nome; ?>_0" class="bg_varchar" valign="top" oncontextmenu="Lightview.show({ url: 'http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>istant/text/modale.php?t=<?php echo $this->tabella; ?>&i=<?php echo $this->id; ?>&n=<?php echo $this->nome; ?>&e=<?php echo $extra; ?>', options: { width: 455, height: 425 }, type: 'iframe' });return false;"><a href="index.php?id_page=<?php echo $this->hyperlink; ?>" class="<?php echo $this->classe; ?>" target="_top"><?php echo $this->testo; ?></a></span>
<?php
		}
		else
		{
?>
<span id="varchar_<?php echo $this->tabella; ?>_<?php echo $this->nome; ?>_0" valign="top" class="bg_varchar <?php echo $this->classe; ?>" oncontextmenu="Lightview.show({ url: 'http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>istant/text/modale.php?t=<?php echo $this->tabella; ?>&i=<?php echo $this->id; ?>&n=<?php echo $this->nome; ?>&e=<?php echo $extra; ?>', options: { width: 455, height: 425 }, type: 'iframe' });return false;"><?php echo $this->testo; ?></span>
<?php
		}
	}
	function translate($grantType, $scopeUrl, $clientID, $clientSecret, $authUrl)
	{
		if($this->trovato == 0)
		{
			$query = "SELECT * FROM lingua WHERE primario = 'si'";
			$risultato = mysql_query($query);
			$riga = mysql_fetch_array($risultato);
			$from = $riga['path'];
			$query = "SELECT * FROM ".$this->tabella." WHERE ID = '".$this->id."'";
			$risultato = mysql_query($query);
			$riga = mysql_fetch_array($risultato);
			$testo = stripslashes(trim($riga[$this->nome]));
			$query = "SELECT * FROM lingua WHERE ID = '".$this->lingua."'";
			$risultato = mysql_query($query);
			$riga = mysql_fetch_array($risultato);
			$to = $riga['path'];
			$accessToken = $this->getBingTokens($grantType, $scopeUrl, $clientID, $clientSecret, $authUrl);
			$params = array();
			$client = new SoapClient("http://api.microsofttranslator.com/V2/SOAP.svc");
			$params['appId'] = "Bearer ".$accessToken;
			$params['text'] = $this->testo;
			$params['from'] = $from;
			$params['to'] = $to;
			$result = $client->Translate($params);
			$this->testo = $result->TranslateResult;
		}
	}
    function getBingTokens($grantType, $scopeUrl, $clientID, $clientSecret, $authUrl)
	{
        try {
            //Initialize the Curl Session.
            $ch = curl_init();
            //Create the request Array.
            $paramArr = array (
             'grant_type'    => $grantType,
             'scope'         => $scopeUrl,
             'client_id'     => $clientID,
             'client_secret' => $clientSecret
            );
            //Create an Http Query.//
            $paramArr = http_build_query($paramArr);
            //Set the Curl URL.
            curl_setopt($ch, CURLOPT_URL, $authUrl);
            //Set HTTP POST Request.
            curl_setopt($ch, CURLOPT_POST, TRUE);
            //Set data to POST in HTTP "POST" Operation.
            curl_setopt($ch, CURLOPT_POSTFIELDS, $paramArr);
            //CURLOPT_RETURNTRANSFER- TRUE to return the transfer as a string of the return value of curl_exec().
            curl_setopt ($ch, CURLOPT_RETURNTRANSFER, TRUE);
            //CURLOPT_SSL_VERIFYPEER- Set FALSE to stop cURL from verifying the peer's certificate.
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            //Execute the  cURL session.
            $strResponse = curl_exec($ch);
            //Get the Error Code returned by Curl.
            $curlErrno = curl_errno($ch);
            if($curlErrno){
                $curlError = curl_error($ch);
                throw new Exception($curlError);
            }
            //Close the Curl Session.
            curl_close($ch);
            //Decode the returned JSON string.
            $objResponse = json_decode($strResponse);
                
            if ($objResponse->error){
                throw new Exception($objResponse->error_description);
            }
            return $objResponse->access_token;
        } catch (Exception $e) {
            echo "Exception-".$e->getMessage();
        }
    }
}
?>