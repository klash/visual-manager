<?php
class photogallery
{
    var $directory;
    var $abs_path;
    var $tabella;
    var $id;
    var $nome;
    var $div;
    var $foto;
    var $dida;
    var $posizione;
    var $dimensione;
    var $gutter;
    var $minimum;
    var $larghezza;
    var $altezza;
    var $colonne;
	var $javascript;
    var $esiste;
    var $reload;
	function init($tabella, $id, $nome)
	{
		$this->directory = $_SERVER['DOCUMENT_ROOT'].$_SESSION['path'];
		$this->abs_path = "http://".$_SERVER['SERVER_NAME'].$_SESSION['path'];
		$this->tabella = $tabella;
		$this->id = $id;
		$this->nome = $nome;
		$this->foto = array();
		$this->thumb = array();
		$this->dida = array();
		$this->div = array();
		$this->posizione = array();
		$this->image = array();
		$query_tabella = "SELECT * FROM tabella WHERE nome = '".$tabella."'";
//print $query_tabella."<br>";
		$risultato_tabella = mysql_query($query_tabella);
		$riga_tabella = mysql_fetch_array($risultato_tabella);
		if($riga_tabella['lingua'] == 'si')
		{
			$query = "SELECT * FROM ".$tabella."_img WHERE ".$tabella." = ".$this->id." AND ID = primario ORDER BY posizione";
//print $query."<br>";
			$risultato = mysql_query($query);
			while ($riga = mysql_fetch_array($risultato))
			{
				$ctrl_path = "file/".$this->tabella."_".$riga['primario']."_".$this->id."_".$riga[$this->nome];
//echo $ctrl_path."<br>";
				if (file_exists($this->directory.$ctrl_path))
				{
					$this->foto[$riga['primario']] = "file/".$this->tabella."_".$riga['primario']."_".$this->id."_".$riga[$this->nome];
//echo $this->foto[$riga['primario']]."<br>";
					$this->thumb[$riga['primario']] = "file/thumb_".$this->tabella."_".$riga['primario']."_".$this->id."_".$riga[$this->nome];
//echo $this->thumb[$riga['primario']]."<br>";
//print $riga['numero']." > ".$riga['didascalia']."<br>";
					$query_vedi = "SELECT * FROM ".$tabella."_img WHERE primario = '".$riga['primario']."' AND lingua = '".$_SESSION['language']['id']."'";
					$risultato_vedi = mysql_query($query_vedi);
					$riga_vedi = mysql_fetch_array($risultato_vedi);
					if($riga_vedi['didascalia'] != "didascalia")
					{
						$this->dida[$riga['primario']] = $riga_vedi['didascalia'];
					}
					else
					{
						$this->dida[$riga['primario']] = "";
					}
					$this->div[$riga['primario']] = "photogallery_".$this->tabella."_".$this->nome."_".$this->id."_".$riga['primario'];
					$this->posizione[$riga['primario']] = $riga['posizione'];
					$this->image[$riga['ID']] = $riga['primario'];
				}
			}
		}
		else
		{
			$query = "SELECT * FROM ".$tabella."_img WHERE ".$tabella." = ".$this->id." ORDER BY posizione";
print $query."<br>";
			$risultato = mysql_query($query);
			while ($riga = mysql_fetch_array($risultato))
			{
				$this->foto[$riga['ID']] = "file/".$this->tabella."_".$riga['ID']."_".$this->id."_".$riga[$this->nome];
				$this->thumb[$riga['ID']] = "file/thumb_".$this->tabella."_".$riga['ID']."_".$this->id."_".$riga[$this->nome];
				$this->dida[$riga['ID']] = $riga['didascalia'];
				$this->div[$riga['ID']] = "photogallery_".$this->tabella."_".$this->nome."_".$this->id."_".$riga['ID'];
				$this->posizione[$riga['ID']] = $riga['posizione'];
				$this->image[$riga['ID']] = $riga['ID'];
//print $riga['numero']." > ".$this->foto[$riga['ID']]."<br>";
			}
		}
		if(count($this->foto) > 0)
		{
			$this->esiste = 1;
		}
		else
		{
			$this->esiste = 0;
		}
	}
	function dimensioni($larghezza, $altezza, $gutter)
	{
		$this->dimensione = $larghezza.":".$altezza;
		$this->larghezza['thumb'] = $larghezza;
		$this->altezza['thumb'] = $altezza;
		$this->gutter = $gutter;
	}
	function minimum($larghezza, $altezza)
	{
		$this->minimum = $larghezza.":".$altezza;
		$this->larghezza['min'] = $larghezza;
		$this->altezza['min'] = $altezza;
	}
	function javascript($javascript)
	{
		$this->javascript = $javascript;
	}
	function reload($reload)
	{
		$this->reload = $reload;
	}
	function manager($style)
	{
		if(count($this->foto) > 0)
		{
?>
<script>
$(function() {
	$("#sortable_photogallery_<?php echo $this->tabella; ?>_<?php echo $this->id; ?>").sortable({
		placeholder: "widget-highlight",
		update: function(event, ui) {
			var tabella = "<?php echo $this->tabella; ?>";
			var ordine = $('#sortable_photogallery_<?php echo $this->tabella; ?>_<?php echo $this->id; ?>').sortable('toArray'); 
			path = "http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>istant/photogallery/sortable.php";
			$.ajax({
				type: "POST",
				url: path,
				data: "tabella="+tabella+"&ordine="+ordine,
				success: function(msg) {
					//alert(msg)
				}
			});
		},
	});
	$("#sortable_photogallery_<?php echo $this->tabella; ?>_<?php echo $this->id; ?>").disableSelection();
});
</script>
<ul id="sortable_photogallery_<?php echo $this->tabella; ?>_<?php echo $this->id; ?>" style="padding:0px !important; list-style-type: none">
<?php
			reset($this->foto);
			while (list($key, $val) = each($this->foto))
			{
				if (file_exists($val))
				{
					$area = $this->larghezza['thumb']*$this->altezza['thumb'];
					$ratio = floatval(@(($this->larghezza['thumb']/$this->altezza['thumb'])));
					list($real_width, $real_height) = getimagesize($this->thumb[$key]);
					$area_real = $real_width*$real_height;
					$real_ratio = floatval($real_width/$real_height);
					if($area > $area_real)
					{
						switch ($ratio)
						{
							case ($ratio < 1):
								switch ($real_ratio)
								{
									case ($real_ratio < 1):
										$real_width = $this->larghezza['thumb'];
										$real_height = intval($real_width/$real_ratio);
										$this->style['larghezza'] = "width:".$real_width."px;";
										$this->style['altezza'] = "height:".$real_height."px;";
										break;
									default:
										$real_height =$this->altezza['thumb'];
										$real_width = intval($real_height*$real_ratio);
										$this->style['larghezza'] = "width:".$real_width."px;";
										$this->style['altezza'] = "height:".$real_height."px;";
										break;
								}
								break;
							default:
								switch ($real_ratio)
								{
									case ($real_ratio <= 1):
										$real_width = $this->larghezza['thumb'];
										$real_height = intval($real_width/$real_ratio);
										$this->style['larghezza'] = "width:".$real_width."px;";
										$this->style['altezza'] = "height:".$real_height."px;";
										break;
									default:
										$real_height =$this->altezza['thumb'];
										$real_width = intval($real_height*$real_ratio);
										$this->style['larghezza'] = "width:".$real_width."px;";
										$this->style['altezza'] = "height:".$real_height."px;";
										break;
								}
						}
					}
					else
					{
						switch ($ratio)
						{
							case ($ratio < 1):
								switch ($real_ratio)
								{
									case ($real_ratio < 1):
										$real_width = $this->larghezza['thumb'];
										$real_height = intval($real_width/$real_ratio);
										$this->style['larghezza'] = "width:".$real_width."px;";
										$this->style['altezza'] = "height:".$real_height."px;";
										break;
									default:
										$real_height =$this->altezza['thumb'];
										$real_width = intval($real_height*$real_ratio);
										$this->style['larghezza'] = "width:".$real_width."px;";
										$this->style['altezza'] = "height:".$real_height."px;";
										break;
								}
								break;
							default:
								switch ($real_ratio)
								{
									case ($real_ratio < 1):
										$real_width = $this->larghezza['thumb'];
										$real_height = intval($real_width/$real_ratio);
										$this->style['larghezza'] = "width:".$real_width."px;";
										$this->style['altezza'] = "height:".$real_height."px;";
										break;
									default:
										$real_height =$this->altezza['thumb'];
										$real_width = intval($real_height*$real_ratio);
										$this->style['larghezza'] = "width:".$real_width."px;";
										$this->style['altezza'] = "height:".$real_height."px;";
										break;
								}
						}
					}
					switch ($ratio)
					{
						case ($ratio < 1):
							switch ($real_ratio)
							{
								case ($real_ratio < 1):
									$delta = ($real_height - $this->altezza['thumb']) / 2;
									$margin = "margin-top:-".$delta."px;";
									break;
								default:
									$delta = ($real_width - $this->larghezza['thumb']) / 2;
									$margin = "margin-left:-".$delta."px;";
									break;
							}
							break;
						default:
							switch ($real_ratio)
							{
								case ($real_ratio > 1):
									$delta = ($real_width - $this->larghezza['thumb']) / 2;
									$margin = "margin-left:-".$delta."px;";
									break;
								default:
									$delta = ($real_height - $this->altezza['thumb']) / 2;
									$margin = "margin-top:-".$delta."px;";
									break;
							}
							break;
					}
//					$area['foto'] = $real_width*$real_height;
//					$area['min'] = $this->larghezza['min']*$this->altezza['min'];
					switch ($style)
					{
						case "zoom":
?>
<li id="<?php echo $this->div[$key]; ?>" style="float:left; margin-right:<?php echo $this->gutter; ?>px; margin-bottom:<?php echo $this->gutter; ?>px; width:<?php echo $this->larghezza['thumb']; ?>px;">
<div style="height:<?php echo $this->altezza['thumb']; ?>px; overflow:hidden; " oncontextmenu="Lightview.show({ url: 'http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>istant/photogallery/modale.php?t=<?php echo $this->tabella; ?>&i=<?php echo $this->id; ?>&n=<?php echo $this->nome; ?>&o=<?php echo $this->image[$key]; ?>&k=<?php echo $this->posizione[$key]; ?>&d=<?php echo $this->dimensione; ?>&m=<?php echo $this->minimum; ?>&j=<?php echo $this->javascript; ?>', options: { width: 455, height: 530 }, type: 'iframe' });return false;">
<a href="#" onclick="$('#gallery_zoom').attr('src','<?php echo $_SESSION['path']; ?><?php echo $val; ?>');zoom_action('<?php echo $_SESSION['path']; ?><?php echo $val; ?>');" title="<?php echo $this->dida[$key]; ?>"><img id="<?php echo "thumb_".$this->div[$key]; ?>" src="<?php echo $_SESSION['path']; ?><?php echo $this->thumb[$key]; ?>" alt="<?php echo $this->dida[$key]; ?>" title="<?php echo $this->dida[$key]; ?>" border="0" style=" <?php echo $margin; ?> <?php echo $this->style['larghezza']; ?> <?php echo $this->style['altezza']; ?>" /></a>
</div>
</li>
<?php
							break;
						case "lightview":
?>
<li id="<?php echo $this->div[$key]; ?>" style="float:left; margin-right:<?php echo $this->gutter; ?>px; margin-bottom:<?php echo $this->gutter; ?>px; width:<?php echo $this->larghezza['thumb']; ?>px;">
<div style="height:<?php echo $this->altezza['thumb']; ?>px; overflow:hidden; " oncontextmenu="Lightview.show({ url: 'http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>istant/photogallery/modale.php?t=<?php echo $this->tabella; ?>&i=<?php echo $this->id; ?>&n=<?php echo $this->nome; ?>&o=<?php echo $this->image[$key]; ?>&k=<?php echo $this->posizione[$key]; ?>&d=<?php echo $this->dimensione; ?>&m=<?php echo $this->minimum; ?>&j=<?php echo $this->javascript; ?>', options: { width: 455, height: 530 }, type: 'iframe' });return false;">
<a href="<?php echo $_SESSION['path']; ?><?php echo $val; ?>" class="lightview" data-lightview-group="gallery[<?php echo $this->id; ?>]" data-lightview-caption="<?php echo $this->dida[$key]; ?>"><img id="<?php echo "thumb_".$this->div[$key]; ?>" src="<?php echo $_SESSION['path']; ?><?php echo $this->thumb[$key]; ?>" alt="<?php echo $this->dida[$key]; ?>" title="<?php echo $this->dida[$key]; ?>" border="0" style=" <?php echo $margin; ?> <?php echo $this->style['larghezza']; ?> <?php echo $this->style['altezza']; ?>" /></a>
</div>
</li>
<?php
							break;
						default:
?>
<li id="<?php echo $this->div[$key]; ?>" style="float:left; margin-right:<?php echo $this->gutter; ?>px; margin-bottom:<?php echo $this->gutter; ?>px; width:<?php echo $this->larghezza['thumb']; ?>px;">
<div style="height:<?php echo $this->altezza['thumb']; ?>px; overflow:hidden;" oncontextmenu="Lightview.show({ url: 'http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>istant/photogallery/modale.php?t=<?php echo $this->tabella; ?>&i=<?php echo $this->id; ?>&n=<?php echo $this->nome; ?>&o=<?php echo $this->image[$key]; ?>&k=<?php echo $this->posizione[$key]; ?>&d=<?php echo $this->dimensione; ?>&m=<?php echo $this->minimum; ?>&j=<?php echo $this->javascript; ?>', options: { width: 455, height: 530 }, type: 'iframe' });return false;">
<img id="<?php echo "thumb_".$this->div[$key]; ?>" src="<?php echo $_SESSION['path']; ?><?php echo $this->thumb[$key]; ?>" alt="<?php echo $this->dida[$key]; ?>" title="<?php echo $this->dida[$key]; ?>" border="0" style=" <?php echo $margin; ?> <?php echo $this->style['larghezza']; ?> <?php echo $this->style['altezza']; ?>" />
</div>
</li>
<?php
					}
				}
				else
				{
?>
<li id="<?php echo $this->div[$key]; ?>" style="float:left; margin-right:<?php echo $this->gutter; ?>px; margin-bottom:<?php echo $this->gutter; ?>px; width:<?php echo $this->larghezza['thumb']; ?>px;">
<div class="bg_grafica" style="width:<?php echo $this->larghezza['thumb']; ?>px; height:<?php echo $this->altezza['thumb']; ?>px;" oncontextmenu="Lightview.show({ url: 'http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>istant/photogallery/modale.php?t=<?php echo $this->tabella; ?>&i=<?php echo $this->id; ?>&n=<?php echo $this->nome; ?>&o=<?php echo $this->image[$key]; ?>&k=<?php echo $this->posizione[$key]; ?>&d=<?php echo $this->dimensione; ?>&m=<?php echo $this->minimum; ?>&j=<?php echo $this->javascript; ?>', options: { width: 455, height: 530 }, type: 'iframe' });return false;">
<?php echo $this->dida[$key]; ?>
</div>
</li>
<?php
				}
			}
?>
</ul>
<?php
		}
	}
	function vedi($style)
	{
		if(count($this->foto) > 0)
		{
			reset($this->foto);
			while (list($key, $val) = each($this->foto))
			{
				if (file_exists($val))
				{
					$area = $this->larghezza['thumb']*$this->altezza['thumb'];
					$ratio = floatval(@(($this->larghezza['thumb']/$this->altezza['thumb'])));
					list($real_width, $real_height) = getimagesize($this->directory.$this->thumb[$key]);
					$area_real = $real_width*$real_height;
					$real_ratio = floatval($real_width/$real_height);
					if($area > $area_real)
					{
						switch ($ratio)
						{
							case ($ratio < 1):
								switch ($real_ratio)
								{
									case ($real_ratio < 1):
										$real_width = $this->larghezza['thumb'];
										$real_height = intval($real_width/$real_ratio);
										$this->style['larghezza'] = "width:".$real_width."px;";
										$this->style['altezza'] = "height:".$real_height."px;";
										break;
									default:
										$real_height =$this->altezza['thumb'];
										$real_width = intval($real_height*$real_ratio);
										$this->style['larghezza'] = "width:".$real_width."px;";
										$this->style['altezza'] = "height:".$real_height."px;";
										break;
								}
								break;
							default:
								switch ($real_ratio)
								{
									case ($real_ratio <= 1):
										$real_width = $this->larghezza['thumb'];
										$real_height = intval($real_width/$real_ratio);
										$this->style['larghezza'] = "width:".$real_width."px;";
										$this->style['altezza'] = "height:".$real_height."px;";
										break;
									default:
										$real_height =$this->altezza['thumb'];
										$real_width = intval($real_height*$real_ratio);
										$this->style['larghezza'] = "width:".$real_width."px;";
										$this->style['altezza'] = "height:".$real_height."px;";
										break;
								}
						}
					}
					else
					{
						switch ($ratio)
						{
							case ($ratio < 1):
								switch ($real_ratio)
								{
									case ($real_ratio < 1):
										$real_width = $this->larghezza['thumb'];
										$real_height = intval($real_width/$real_ratio);
										$this->style['larghezza'] = "width:".$real_width."px;";
										$this->style['altezza'] = "height:".$real_height."px;";
										break;
									default:
										$real_height =$this->altezza['thumb'];
										$real_width = intval($real_height*$real_ratio);
										$this->style['larghezza'] = "width:".$real_width."px;";
										$this->style['altezza'] = "height:".$real_height."px;";
										break;
								}
								break;
							default:
								switch ($real_ratio)
								{
									case ($real_ratio < 1):
										$real_width = $this->larghezza['thumb'];
										$real_height = intval($real_width/$real_ratio);
										$this->style['larghezza'] = "width:".$real_width."px;";
										$this->style['altezza'] = "height:".$real_height."px;";
										break;
									default:
										$real_height =$this->altezza['thumb'];
										$real_width = intval($real_height*$real_ratio);
										$this->style['larghezza'] = "width:".$real_width."px;";
										$this->style['altezza'] = "height:".$real_height."px;";
										break;
								}
						}
					}
					switch ($ratio)
					{
						case ($ratio < 1):
							switch ($real_ratio)
							{
								case ($real_ratio < 1):
									$delta = ($real_height - $this->altezza['thumb']) / 2;
									$margin = "margin-top:-".$delta."px;";
									break;
								default:
									$delta = ($real_width - $this->larghezza['thumb']) / 2;
									$margin = "margin-left:-".$delta."px;";
									break;
							}
							break;
						default:
							switch ($real_ratio)
							{
								case ($real_ratio > 1):
									$delta = ($real_width - $this->larghezza['thumb']) / 2;
									$margin = "margin-left:-".$delta."px;";
									break;
								default:
									$delta = ($real_height - $this->altezza['thumb']) / 2;
									$margin = "margin-top:-".$delta."px;";
									break;
							}
							break;
					}
					switch ($style)
					{
						case "zoom":
?>
<div id="<?php echo $this->div[$key]; ?>" style="float:left; margin-right:<?php echo $this->gutter; ?>px; margin-bottom:<?php echo $this->gutter; ?>px; width:<?php echo $this->larghezza['thumb']; ?>px;">
<div style="height:<?php echo $this->altezza['thumb']; ?>px; overflow:hidden;">
<a href="#" onclick="$('#gallery_zoom').attr('src','<?php echo $_SESSION['path']; ?><?php echo $val; ?>');zoom_action('<?php echo $_SESSION['path']; ?><?php echo $val; ?>');" title="<?php echo $this->dida[$key]; ?>"><img src="<?php echo $_SESSION['path']; ?><?php echo $this->thumb[$key]; ?>" alt="<?php echo $this->dida[$key]; ?>" title="<?php echo $this->dida[$key]; ?>" border="0" style=" <?php echo $margin; ?> <?php echo $this->style['larghezza']; ?> <?php echo $this->style['altezza']; ?>" /></a>
</div>
</div>
<?php
							break;
						case "lightview":
?>
<div id="<?php echo $this->div[$key]; ?>" style="float:left; margin-right:<?php echo $this->gutter; ?>px; margin-bottom:<?php echo $this->gutter; ?>px; width:<?php echo $this->larghezza['thumb']; ?>px;">
<div style="height:<?php echo $this->altezza['thumb']; ?>px; overflow:hidden;">
<a href="<?php echo $_SESSION['path']; ?><?php echo $val; ?>" class="lightview" data-lightview-group="gallery[<?php echo $this->id; ?>]" data-lightview-caption="<?php echo $this->dida[$key]; ?>"><img src="<?php echo $_SESSION['path']; ?><?php echo $this->thumb[$key]; ?>" alt="<?php echo $this->dida[$key]; ?>" title="<?php echo $this->dida[$key]; ?>" border="0" style=" <?php echo $margin; ?> <?php echo $this->style['larghezza']; ?> <?php echo $this->style['altezza']; ?>" /></a>
</div>
</div>
<?php
							break;
					default:
?>
<div id="<?php echo $this->div[$key]; ?>" style="float:left; margin-right:<?php echo $this->gutter; ?>px; margin-bottom:<?php echo $this->gutter; ?>px; width:<?php echo $this->larghezza['thumb']; ?>px;">
<div style="height:<?php echo $this->altezza['thumb']; ?>px; overflow:hidden;">
<img src="<?php echo $_SESSION['path']; ?><?php echo $this->thumb[$key]; ?>" alt="<?php echo $this->dida[$key]; ?>" title="<?php echo $this->dida[$key]; ?>" border="0" style=" <?php echo $margin; ?> <?php echo $this->style['larghezza']; ?> <?php echo $this->style['altezza']; ?>" />
</div>
</div>
<?php
					}
				}
			}
		}
	}
	function plus($tabella, $id, $nome, $extra, $testo, $classe)
	{
		$this->tabella = $tabella;
		$this->id = $id;
		$this->nome = $nome;
?>
<div id="<?php echo $this->div; ?>">
<div class="bg_grafica" style="width:<?php echo $this->larghezza['thumb']; ?>px; height:<?php echo $this->altezza['thumb']; ?>px; margin-bottom:10px; float:left;" oncontextmenu="Lightview.show({ url: 'http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>istant/photogallery/modale.php?t=<?php echo $this->tabella; ?>&i=<?php echo $this->id; ?>&n=<?php echo $this->nome; ?>&o=0&k=0&d=<?php echo $this->dimensione; ?>&m=<?php echo $this->minimum; ?>&j=<?php echo $this->javascript; ?>&e=<?php echo $extra; ?>', options: { width: 455, height: 490 }, type: 'iframe' });return false;">
<span class="<?php echo $classe; ?>"><?php echo $testo; ?></span>
</div>
</div>
<?php
	}
	function rotate($larghezza, $altezza)
	{
		$area = $larghezza*$altezza;
		$ratio = floatval(@(($larghezza/$altezza)));
		reset($this->foto);
		while (list($key, $val) = each($this->foto))
		{
			$this->margin = "";
			$this->larghezza = "";
			$this->altezza = "";
			if (file_exists($val))
			{
				list($real_width, $real_height) = getimagesize($val);
				$area_real = $real_width*$real_height;
				$real_ratio = floatval($real_width/$real_height);
				if($area > $area_real)
				{
					switch ($ratio)
					{
						case ($ratio < 1):
							switch ($real_ratio)
							{
								case ($real_ratio < 1):
									$real_width = $larghezza;
									$real_height = $real_width/$real_ratio;
									$this->larghezza = "width:".$real_width."px;";
									$this->altezza = "height:".$real_height."px;";
									break;
								default:
									$real_height = $altezza;
									$real_width = $real_height*$real_ratio;
									$this->larghezza = "width:".$real_width."px;";
									$this->altezza = "height:".$real_height."px;";
									break;
							}
							break;
						default:
							switch ($real_ratio)
							{
								case ($real_ratio <= 1):
									$real_width = $larghezza;
									$real_height = $real_width/$real_ratio;
									$this->larghezza = "width:".$real_width."px;";
									$this->altezza = "height:".$real_height."px;";
									break;
								default:
									$real_height = $altezza;
									$real_width = $real_height*$real_ratio;
									$this->larghezza = "width:".$real_width."px;";
									$this->altezza = "height:".$real_height."px;";
									break;
							}
					}
				}
				else
				{
					switch ($ratio)
					{
						case ($ratio < 1):
							switch ($real_ratio)
							{
								case ($real_ratio < 1):
									$real_width = $larghezza;
									$real_height = $real_width/$real_ratio;
									$this->larghezza = "width:".$real_width."px;";
									$this->altezza = "height:".$real_height."px;";
									break;
								default:
									$real_height = $altezza;
									$real_width = $real_height*$real_ratio;
									$this->larghezza = "width:".$real_width."px;";
									$this->altezza = "height:".$real_height."px;";
									break;
							}
							break;
						default:
							switch ($real_ratio)
							{
								case ($real_ratio < 1):
									$real_width = $larghezza;
									$real_height = $real_width/$real_ratio;
									$this->larghezza = "width:".$real_width."px;";
									$this->altezza = "height:".$real_height."px;";
									break;
								default:
									$real_height = $altezza;
									$real_width = $real_height*$real_ratio;
									$this->larghezza = "width:".$real_width."px;";
									$this->altezza = "height:".$real_height."px;";
									break;
							}
					}
				}
				switch ($ratio)
				{
					case ($ratio < 1):
						switch ($real_ratio)
						{
							case ($real_ratio < 1):
								$delta = ($real_height - $altezza) / 2;
								$this->margin = "margin-top:-".$delta."px;";
								break;
							default:
								$delta = ($real_width - $larghezza) / 2;
								$this->margin = "margin-left:-".$delta."px;";
								break;
						}
						break;
					default:
						switch ($real_ratio)
						{
							case ($real_ratio > 1):
								$delta = ($real_width - $larghezza) / 2;
								$this->margin = "margin-left:-".$delta."px;";
								break;
							default:
								$delta = ($real_height - $altezza) / 2;
								$this->margin = "margin-top:-".$delta."px;";
								break;
						}
						break;
				}
				if (trim($this->linka) != "")
				{
?>
<div style="width:<?php echo $larghezza; ?>px; height:<?php echo $altezza; ?>px; overflow:hidden;">
<a href="<?php echo $this->linka; ?>"><img id="<?php echo $this->div[$key]; ?>" original="<?php echo $this->abs_path; ?><?php echo $val; ?>" src="<?php echo $this->abs_path; ?><?php echo $val; ?>" name="<?php echo $this->div[$key]; ?>" style=" <?php echo $this->margin; ?> <?php echo $this->larghezza; ?> <?php echo $this->altezza; ?>" border="0" <?php echo $this->onclick; ?> /></a>
</div>
<?php
				}
				else
				{
?>
<div style="width:<?php echo $larghezza; ?>px; height:<?php echo $altezza; ?>px; overflow:hidden;">
<img id="<?php echo $this->div[$key]; ?>" original="<?php echo $this->abs_path; ?><?php echo $val; ?>" src="<?php echo $this->abs_path; ?><?php echo $val; ?>" name="<?php echo $this->div[$key]; ?>" style=" <?php echo $this->margin; ?> <?php echo $this->larghezza; ?> <?php echo $this->altezza; ?>" border="0" <?php echo $this->onclick; ?> />
</div>
<?php
				}
			}
		}
	}
}
?>
