<?php
class data
{
    var $lingua;
    var $tabella;
    var $id;
    var $nome;
    var $classe;
    var $data;
    var $giorno;
    var $mese;
    var $anno;
	var $lettera;
	var $parola;
	var $vedi;
	var $esiste;
	var $testo;
	var $reload;
	function lingua($lingua)
	{
		$this->lingua = $lingua;
	}
	function init($tabella, $id, $nome, $classe)
	{
		$this->tabella = $tabella;
		$this->id = $id;
		$this->nome = $nome;
		$this->classe = $classe;
		$query = "SELECT ".$nome.", DAY(".$nome.") AS giorno, MONTH(".$nome.") AS mese, YEAR(".$nome.") AS anno FROM ".$this->tabella." WHERE ID = '".$this->id."'";
		$risultato = mysql_query($query);
		$riga = mysql_fetch_array($risultato);
		$this->data = trim($riga[$nome]);
		$this->giorno = str_repeat("0",(2-strlen($riga['giorno']))).$riga['giorno'];
		$this->mese = str_repeat("0",(2-strlen($riga['mese']))).$riga['mese'];
		$this->anno = $riga['anno'];
		if (checkdate($this->mese,$this->giorno,$this->anno))
		{
		 	$this->esiste = 1;
			// parola giorno
			$this->parola['giorno'] = date("w", mktime(0,0,0,$this->mese,$this->giorno,$this->anno));
			$query_giorno = "SELECT * FROM data_giorno WHERE posizione = '".$this->parola['giorno']."'";
			$risultato_giorno = mysql_query($query_giorno);
			$riga_giorno = mysql_fetch_array($risultato_giorno);
			if($this->lingua > 0)
			{
				$query_giorno = "SELECT * FROM data_giorno WHERE primario = '".$riga_giorno['primario']."' AND lingua = '".$this->lingua."'";
				$risultato_giorno = mysql_query($query_giorno);
				$riga_giorno = mysql_fetch_array($risultato_giorno);
				$this->parola['giorno'] = $riga_giorno['giorno'];
			}
			else
			{
				$this->parola['giorno'] = $riga_giorno['giorno'];
			}
			$this->lettera['giorno'] = strtoupper($this->parola['giorno'][0]);
			// parola mese
			$query_mese = "SELECT * FROM data_mese WHERE posizione = '".$this->mese."'";
			$risultato_mese = mysql_query($query_mese);
			$riga_mese = mysql_fetch_array($risultato_mese);
			if($this->lingua > 0)
			{
				$query_mese = "SELECT * FROM data_mese WHERE primario = '".$riga_mese['primario']."' AND lingua = '".$this->lingua."'";
				$risultato_mese = mysql_query($query_mese);
				$riga_mese = mysql_fetch_array($risultato_mese);
				$this->parola['mese'] = $riga_mese['mese'];
			}
			else
			{
				$this->parola['mese'] = $riga_mese['mese'];
			}
			$this->lettera['mese'] = strtoupper($this->parola['mese'][0]);
		}
		else
		{
		 	$this->esiste = 0;
		}
	}
	function formato($formato)
	{
		if ($this->esiste)
		{
			$formato = str_replace("gg",$this->giorno,$formato);
			$formato = str_replace("mm",$this->mese,$formato);
			$formato = str_replace("aaaa",$this->anno,$formato);
			$formato = str_replace("aa",substr($this->anno, -2),$formato);
			$formato = str_replace("LG",$this->lettera['giorno'],$formato);
			$formato = str_replace("giorno",$this->parola['giorno'],$formato);
			$formato = str_replace("LM",$this->lettera['mese'],$formato);
			$formato = str_replace("mese",$this->parola['mese'],$formato);
			$this->vedi = $formato;
		}
		else
		{
			$this->vedi = $formato;
		}
	}
	function reload($reload)
	{
		$this->reload = $reload;
	}
	function manager()
	{
?>
<span id="data_<?php echo $this->tabella; ?>_<?php echo $this->nome; ?>_<?php echo $this->id; ?>" valign="top" class="<?php echo $this->classe; ?> bg_time" oncontextmenu="Lightview.show({ url: 'http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>istant/data/modale.php?t=<?php echo $this->tabella; ?>&i=<?php echo $this->id; ?>&n=<?php echo $this->nome; ?>', options: { width: 455, height: 320 }, type: 'iframe' });return false;"><?php echo $this->vedi; ?></span>
<?php
	}
	function vedi()
	{
		if ($this->esiste)
		{
?>
<span valign="top" class="<?php echo $this->classe; ?>"><?php echo $this->vedi; ?></span>
<?php
		}
	}
	function plus($extra, $testo)
	{
		$this->testo = $testo;
?>
<span id="data_<?php echo $this->tabella; ?>_<?php echo $this->nome; ?>_0" valign="top" class="<?php echo $this->classe; ?> bg_time" oncontextmenu="Lightview.show({ url: 'http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>istant/data/modale.php?t=<?php echo $this->tabella; ?>&i=<?php echo $this->id; ?>&n=<?php echo $this->nome; ?>&e=<?php echo $extra; ?>', options: { width: 455, height: 320 }, type: 'iframe' });return false;"><?php echo $this->testo; ?></span>
<?php
	}
}
?>