<?php
class enum
{
    var $directory;
    var $abs_path;
    var $tabella;
    var $id;
    var $nome;
    var $ordine;
    var $valore;
    var $ico;
    var $vedi;
    var $hyperlink;
    var $reload;
	function init($tabella, $id, $nome, $ordine)
	{
		$this->directory = $_SERVER['DOCUMENT_ROOT'].$_SESSION['path'];
		$this->abs_path = "http://".$_SERVER['SERVER_NAME'].$_SESSION['path'];
		$this->tabella = $tabella;
		$this->id = $id;
		$this->nome = $nome;
		$this->ordine = $ordine;
		$query_tabella = "SELECT * FROM ".$this->tabella." WHERE `ID` = '".$this->id."'";
		$risultato_tabella = mysql_query($query_tabella);
		$riga_tabella = mysql_fetch_array($risultato_tabella);
		$this->valore = $riga_tabella[$this->nome];
		$this->ico = "enum/".$this->nome."_".$this->valore.".gif";
		if (file_exists($this->directory.$this->ico))
		{
			$this->vedi = "<img id='enum_".$this->tabella."_".$this->nome."_".$this->id."' src='".$this->abs_path.$this->ico."' alt='".$this->nome."' title='".$this->nome."' style='vertical-align:text-top; cursor:pointer;' />";
		}
		else
		{
			$this->vedi = $riga_tabella[$this->nome];
		}
	}
	function useit($valore, $tabella, $id, $nome, $ordine)
	{
		$this->directory = $_SERVER['DOCUMENT_ROOT'].$_SESSION['path'];
		$this->abs_path = "http://".$_SERVER['SERVER_NAME'].$_SESSION['path'];
		$this->tabella = $tabella;
		$this->id = $id;
		$this->nome = $nome;
		$this->ordine = $ordine;
		$this->valore = $valore;
		$this->ico = "enum/".$this->nome."_".$this->valore.".gif";
		if (file_exists($this->directory.$this->ico))
		{
			$this->vedi = "<img id='enum_".$this->tabella."_".$this->nome."_".$this->id."' src='".$this->abs_path.$this->ico."' alt='".$this->nome."' title='".$this->nome."' style='vertical-align:text-top; cursor:pointer;' />";
		}
		else
		{
			$this->vedi = $valore;
		}
	}
	function stile($stile)
	{
		switch ($stile)
		{
		   case "upper":
				$this->vedi = strtoupper($this->vedi);
				break;
		   case "lower":
				$this->vedi = strtolower($this->vedi);
				break;
		}
	}
	function reload($reload)
	{
		$this->reload = $reload;
	}
	function hyperlink($hyperlink)
	{
		$this->hyperlink = $hyperlink;
	}
	function vedi()
	{
		if ($this->hyperlink != "")
		{
?>
<span valign="top"><a href="index.php?<?php echo $this->hyperlink; ?>"><?php echo $this->vedi; ?></a></span>
<?php
		}
		else
		{
?>
<span valign="top"><?php echo $this->vedi; ?></span>
<?php
		}
	}
	function manager()
	{
		if ($this->hyperlink != "")
		{
?>
<span valign="top" oncontextmenu="Lightview.show({ url: 'http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>istant/enum/modale.php?t=<?php echo $this->tabella; ?>&i=<?php echo $this->id; ?>&n=<?php echo $this->nome; ?>', options: { width: 455, height: 425 }, type: 'iframe' });return false;"><a href="index.php?<?php echo $this->hyperlink; ?>"><?php echo $this->vedi; ?></a></span>
<?php
		}
		else
		{
?>
<span valign="top" oncontextmenu="Lightview.show({ url: 'http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>istant/enum/modale.php?t=<?php echo $this->tabella; ?>&i=<?php echo $this->id; ?>&n=<?php echo $this->nome; ?>', options: { width: 455, height: 425 }, type: 'iframe' });return false;"><?php echo $this->vedi; ?></span>
<?php
		}
	}
}
?>