<?php
class video
{
    var $tabella;
    var $id;
    var $nome;
    var $classe;
    var $code;
    var $video;
    var $esiste;
    var $larghezza;
    var $altezza;
    var $reload;
    var $ancor;
	function init($tabella, $id, $nome, $classe)
	{
		$this->tabella = $tabella;
		$this->id = $id;
		$this->nome = $nome;
		$this->classe = $classe;
		$query = "SELECT * FROM ".$this->tabella." WHERE ID = '".$this->id."'";
		$risultato = mysql_query($query);
		$riga = mysql_fetch_array($risultato);
		if (trim($riga[$this->nome]) == "")
		{
			$this->esiste = 0;
		}
		else
		{
			$this->video = stripslashes($riga[$this->nome]);
			$this->esiste = 1;
		}
	}
	function resize($larghezza, $altezza)
	{
		$this->larghezza = $larghezza;
		$this->altezza = $altezza;
		$patterns = array();
		$replacements = array();
		if(!empty($this->larghezza))
		{
			$patterns[] = '/width="([0-9]+)"/';
			$patterns[] = "/width='([0-9]+)'/";
			$patterns[] = "/width:([0-9]+)/";
			$replacements[] = 'width="'.$larghezza.'"';
			$replacements[] = "width='".$larghezza."'";
			$replacements[] = "width:".$larghezza;
		}
		if(!empty($this->altezza))
		{
			$patterns[] = '/height="([0-9]+)"/';
			$patterns[] = "/height='([0-9]+)'/";
			$patterns[] = "/height:([0-9]+)/";
			$replacements[] = 'height="'.$altezza.'"';
			$replacements[] = "height='".$altezza."'";
			$replacements[] = "height:".$altezza;
		}
		$this->video = preg_replace($patterns, $replacements, $this->video);
	}
	function reload($reload)
	{
		$this->reload = $reload;
	}
	function ancor($ancor)
	{
		$this->ancor=$ancor;
	}
	function manager()
	{
		if ($this->esiste)
		{
?>
<div id="video_<?php echo $this->tabella; ?>_<?php echo $this->nome; ?>_<?php echo $this->id; ?>" class="bg_allegato" oncontextmenu="Lightview.show({ url: 'http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>istant/video/modale.php?t=<?php echo $this->tabella; ?>&i=<?php echo $this->id; ?>&n=<?php echo $this->nome; ?>&l=<?php echo $this->larghezza; ?>&a=<?php echo $this->altezza; ?>', options: { width: 455, height: 425 }, type: 'iframe' });return false;" style="width:<?php echo $this->larghezza; ?>px; height:<?php echo $this->altezza; ?>px;"><?php echo $this->video; ?></div>
<span oncontextmenu="Lightview.show({ url: 'http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>istant/video/modale.php?t=<?php echo $this->tabella; ?>&i=<?php echo $this->id; ?>&n=<?php echo $this->nome; ?>&l=<?php echo $this->larghezza; ?>&a=<?php echo $this->altezza; ?>', options: { width: 455, height: 425 }, type: 'iframe' });return false;" class="<?php echo $this->classe; ?> bg_allegato">modifica video</span>
<?php
		}
		else
		{
?>
<div id="video_<?php echo $this->tabella; ?>_<?php echo $this->nome; ?>_<?php echo $this->id; ?>" class="bg_allegato" oncontextmenu="Lightview.show({ url: 'http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>istant/video/modale.php?t=<?php echo $this->tabella; ?>&i=<?php echo $this->id; ?>&n=<?php echo $this->nome; ?>&l=<?php echo $this->larghezza; ?>&a=<?php echo $this->altezza; ?>', options: { width: 455, height: 425 }, type: 'iframe' });return false;" style="width:<?php echo $this->larghezza; ?>px; height:<?php echo $this->altezza; ?>px;"><img src="http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>manager/dot.gif" title="<?php echo $this->title; ?>" alt="<?php echo $this->title; ?>" width="<?php echo $this->larghezza; ?>" height="<?php echo $this->altezza; ?>" border="0" /></div>
<span oncontextmenu="Lightview.show({ url: 'http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>istant/video/modale.php?t=<?php echo $this->tabella; ?>&i=<?php echo $this->id; ?>&n=<?php echo $this->nome; ?>&l=<?php echo $this->larghezza; ?>&a=<?php echo $this->altezza; ?>', options: { width: 455, height: 425 }, type: 'iframe' });return false;" class="<?php echo $this->classe; ?> bg_allegato">modifica video</span>
<?php
		}
	}
	function vedi()
	{
		if ($this->esiste)
		{
?>
<div><?php echo $this->video; ?></div>
<?php
		}
	}
}
?>