<?php
class vimeo
{
    var $tabella;
    var $id;
    var $nome;
    var $classe;
    var $code;
    var $video;
    var $esiste;
    var $larghezza;
    var $altezza;
    var $reload;
    var $ancor;
	function init($tabella, $id, $nome, $classe)
	{
		$this->tabella = $tabella;
		$this->id = $id;
		$this->nome = $nome;
		$this->classe = $classe;
		$query = "SELECT * FROM ".$this->tabella." WHERE ID = '".$this->id."'";
		$risultato = mysql_query($query);
		$riga = mysql_fetch_array($risultato);
		if (trim($riga[$this->nome]) == "")
		{
			$this->esiste = 0;
		}
		else
		{
			$this->code = explode("/",$riga[$this->nome]);
			$this->code = $this->code[count($this->code)-1];
			$this->video = "<iframe src='http://player.vimeo.com/video/".$this->code."' width='400' height='225' frameborder='0' webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>";
			$this->esiste = 1;
		}
	}
	function resize($larghezza, $altezza)
	{
		$this->larghezza = $larghezza;
		$this->altezza = $altezza;
		$patterns = array();
		$replacements = array();
		if(!empty($this->larghezza))
		{
			$patterns[] = "/width='([0-9]+)'/";
			$patterns[] = "/width:([0-9]+)/";
			$replacements[] = "width='".$this->larghezza."'";
			$replacements[] = "width:".$this->larghezza;
		}
		if(!empty($this->altezza))
		{
			$patterns[] = "/height='([0-9]+)'/";
			$patterns[] = "/height:([0-9]+)/";
			$replacements[] = "height='".$this->altezza."'";
			$replacements[] = "height:".$this->altezza;
		}
		$this->video = preg_replace($patterns, $replacements, $this->video);
	}
	function reload($reload)
	{
		$this->reload = $reload;
	}
	function ancor($ancor)
	{
		$this->ancor=$ancor;
	}
	function manager()
	{
		if ($this->esiste)
		{
?>
<div class="bg_allegato" oncontextmenu="MM_openBrWindow('popup/vimeo.php?t=<?php echo $this->tabella; ?>&i=<?php echo $this->id; ?>&n=<?php echo $this->nome; ?>&r=<?php echo $this->reload; ?>&a=<?php echo $this->ancor; ?>','','width=455,height=340');return false;"><div><?php echo $this->video; ?></div><span class="<?php echo $this->classe; ?>">modifica</span></div>
<?php
		}
		else
		{
?>
<div class="bg_allegato" oncontextmenu="MM_openBrWindow('popup/vimeo.php?t=<?php echo $this->tabella; ?>&i=<?php echo $this->id; ?>&n=<?php echo $this->nome; ?>&r=<?php echo $this->reload; ?>&a=<?php echo $this->ancor; ?>','','width=455,height=340');return false;"><img src="image/video.gif" width="320" height="240" alt="video" /></span></div>
<?php
		}
	}
	function vedi()
	{
		if ($this->esiste)
		{
?>
<div><?php echo $this->video; ?></div>
<?php
		}
	}
}
?>