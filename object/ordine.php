<?php
class ordine
{
// init 
    var $cliente;
    var $esiste;
    var $numero;
    var $data;
    var $nome;
    var $cognome;
    var $email;
    var $indirizzo;
    var $nazione;
    var $comune;
    var $provincia;
    var $cap;
    var $telefono;
    var $metodo_spedizione;
    var $metodo_pagamento;
    var $acquisto;
    var $pagato;
    var $totale;
    var $prezzo;
// carrello
    var $ordine;
	var $carrello;
	var $prodotto;
	var $colore;
	var $taglia;
	var $qta;
	var $settore;
	function init($cliente)
	{
		$this->cliente = $cliente;
		$this->data = array();
		$this->nome = array();
		$this->cognome = array();
		$this->email = array();
		$this->indirizzo = array();
		$this->nazione = array();
		$this->comune = array();
		$this->provincia = array();
		$this->cap = array();
		$this->telefono = array();
		$this->metodo_spedizione = array();
		$this->metodo_pagamento = array();
		$this->acquisto = array();
		$this->pagato = array();
		$this->totale = array();
		$this->prezzo = array();
		$query_ordine = "SELECT * FROM ordine WHERE cliente=".$this->cliente." ORDER BY timestamp";
		$risultato_ordine = mysql_query($query_ordine);
		$trovato_ordine = mysql_num_rows($risultato_ordine);
		if ($trovato_ordine > 0)
		{
			$this->esiste = 1;
			while($riga_ordine = mysql_fetch_array($risultato_ordine))
			{
				$this->numero[$riga_ordine['ID']] = str_repeat("0",(8-strlen($riga_ordine['ID']))).$riga_ordine['ID'];
				$this->data[$riga_ordine['ID']] = $riga_ordine['timestamp'];
				$this->nome[$riga_ordine['ID']] = $riga_ordine['nome'];
				$this->cognome[$riga_ordine['ID']] = $riga_ordine['cognome'];
				$this->email[$riga_ordine['ID']] = $riga_ordine['email'];
				$this->indirizzo[$riga_ordine['ID']] = $riga_ordine['indirizzo'];
				$this->nazione[$riga_ordine['ID']] = $riga_ordine['nazione'];
				$this->comune[$riga_ordine['ID']] = $riga_ordine['comune'];
				$this->cap[$riga_ordine['ID']] = $riga_ordine['cap'];
				$this->telefono[$riga_ordine['ID']] = $riga_ordine['telefono'];
				$this->acquisto[$riga_ordine['ID']] = $riga_ordine['acquisto'];
				$this->pagato[$riga_ordine['ID']] = $riga_ordine['pagato'];
				$prezzo = 0;
				$totale = 0;
				$query_carrello = "SELECT * FROM carrello WHERE ordine=".$riga_ordine['ID'];
				$risultato_carrello = mysql_query($query_carrello);
				$trovato_carrello = mysql_num_rows($risultato_carrello);
				while($riga_carrello = mysql_fetch_array($risultato_carrello))
				{
					$qry_prodotto = "SELECT prodotto.* FROM magazzino, colore, prodotto WHERE magazzino.ID = ".$riga_carrello['magazzino']." AND colore.ID = magazzino.colore AND prodotto.ID = colore.prodotto";
					$risultato_prodotto = mysql_query($qry_prodotto);
					$trovato_prodotto = mysql_num_rows($risultato_prodotto);
					if($trovato_prodotto > 0)
					{
						$riga_prodotto = mysql_fetch_array($risultato_prodotto);
						$prezzo += $riga_carrello['qta']*$riga_prodotto['prezzo'];
						$totale += $riga_carrello['qta'];
					}
				}
				$this->totale[$riga_ordine['ID']] = $totale;
				$this->prezzo[$riga_ordine['ID']]['carrello'] = $prezzo;
				$query_metodo_spedizione = "SELECT * FROM metodo_spedizione WHERE ID = ".$riga_ordine['metodo_spedizione'];
				$risultato_metodo_spedizione = mysql_query($query_metodo_spedizione);
				$riga_metodo_spedizione = mysql_fetch_array($risultato_metodo_spedizione);
				$this->metodo_spedizione[$riga_ordine['ID']]['ID'] = $riga_ordine['metodo_spedizione'];
				$this->metodo_spedizione[$riga_ordine['ID']]['nome'] = $riga_metodo_spedizione['nome'];
				$this->prezzo[$riga_ordine['ID']]['metodo_spedizione'] = $riga_metodo_spedizione['prezzo'];
				$query_metodo_pagamento = "SELECT * FROM metodo_pagamento WHERE ID = ".$riga_ordine['metodo_pagamento'];
				$risultato_metodo_pagamento = mysql_query($query_metodo_pagamento);
				$riga_metodo_pagamento = mysql_fetch_array($risultato_metodo_pagamento);
				$this->metodo_pagamento[$riga_ordine['ID']]['ID'] = $riga_ordine['metodo_pagamento'];
				$this->metodo_pagamento[$riga_ordine['ID']]['nome'] = $riga_metodo_pagamento['nome'];
				$this->prezzo[$riga_ordine['ID']]['metodo_pagamento'] = $riga_metodo_pagamento['prezzo'];
				$this->prezzo[$riga_ordine['ID']]['totale'] = $this->prezzo[$riga_ordine['ID']]['carrello'] + $this->prezzo[$riga_ordine['ID']]['metodo_spedizione'] + $this->prezzo[$riga_ordine['ID']]['metodo_pagamento'];
			}
		}
	}
	function carrello($ordine)
	{
		$this->ordine = $ordine;
		$this->totale = 0;
		$this->prodotto = array();
		$this->colore = array();
		$this->taglia = array();
		$this->qta = array();
		$this->settore = array();
		$this->carrello = 0;
		$query_carrello = "SELECT * FROM carrello WHERE ordine=".$this->ordine." ORDER BY timestamp";
		$risultato_carrello = mysql_query($query_carrello);
		$trovato_carrello = mysql_num_rows($risultato_carrello);
		if ($trovato_carrello > 0)
		{
			$this->carrello = 1;
			while($riga_carrello = mysql_fetch_array($risultato_carrello))
			{
				$qry_prodotto = "SELECT colore.colore, prodotto.ID, prodotto.prezzo, magazzino.qta FROM magazzino, colore, prodotto WHERE magazzino.ID = ".$riga_carrello['magazzino']." AND colore.ID = magazzino.colore AND prodotto.ID = colore.prodotto";
				$res_prodotto = mysql_query($qry_prodotto);
				$trovato_prodotto = mysql_num_rows($res_prodotto);
				if($trovato_prodotto > 0)
				{
					$riga_prodotto = mysql_fetch_array($res_prodotto);
					$this->qta_disponibile[$riga_carrello['ID']] = $riga_prodotto['qta'];
					$this->qta_richiesta[$riga_carrello['ID']] = $riga_carrello['qta'];
					$this->prodotto[$riga_carrello['ID']] = $riga_prodotto['ID'];
					$this->colore[$riga_carrello['ID']] = $riga_prodotto['colore'];
					$qry_settore = "SELECT settore FROM settore_prodotto WHERE prodotto = ".$riga_prodotto['ID'];
					$res_settore = mysql_query($qry_settore);
					$riga_settore = mysql_fetch_array($res_settore);
					$this->settore[$riga_carrello['ID']] = $riga_settore['settore'];
					$qry_taglia = "SELECT taglia.taglia FROM magazzino, taglia WHERE magazzino.ID = ".$riga_carrello['magazzino']." AND taglia.ID = magazzino.taglia";
					$res_taglia = mysql_query($qry_taglia);
					$trovato_taglia = mysql_num_rows($res_taglia);
					if($trovato_taglia > 0)
					{
						$riga_taglia = mysql_fetch_array($res_taglia);
						$this->taglia[$riga_carrello['ID']] = $riga_taglia['taglia'];
					}
				}
			}
		}
	}
}
