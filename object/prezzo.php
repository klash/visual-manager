<?php
class prezzo
{
    var $tabella;
    var $id;
    var $nome;
    var $classe;
    var $testo;
    var $hyperlink;
    var $target;
	var $onclick;
    var $tipo;
    var $ico;
    var $posizione;
    var $su;
    var $giu;
    var $esiste;
    var $stato;
    var $secure;
    var $reload;
	function init($tabella, $id, $nome, $classe)
	{
		$this->tabella = $tabella;
		$this->id = $id;
		$this->nome = $nome;
		$this->classe = $classe;
		if($this->id > 0)
		{
			$query = "SELECT * FROM ".$this->tabella." WHERE ID = '".$this->id."'";
			$risultato = mysql_query($query);
			$riga = mysql_fetch_array($risultato);
			$this->prezzo = number_format($riga[$this->nome], 2, ",", ".");
		}
		if($this->prezzo > 0)
		{
			$this->esiste = 1;
		}
		else
		{
			$this->prezzo = $nome;
			$this->esiste = 0;
		}
	}
	function useit($valore, $tabella, $id, $nome, $classe)
	{
		$this->tabella = $tabella;
		$this->id = $id;
		$this->nome = $nome;
		$this->classe = $classe;
		if($this->id > 0)
		{
			$this->prezzo = number_format($valore, 2, ",", ".");
		}
		if($this->prezzo > 0)
		{
			$this->esiste = 1;
		}
		else
		{
			$this->prezzo = $nome;
			$this->esiste = 0;
		}
	}
	function h($numero)
	{
		$this->prezzo = "<H".$numero.">".$pre.$this->prezzo."</H".$numero.">";
	}
	function pre($pre)
	{
		$this->prezzo = $pre.$this->prezzo;
	}
	function post($post)
	{
		$this->prezzo = $this->prezzo.$post;
	}
	function reload($reload)
	{
		$this->reload = $reload;
	}
	function hyperlink($hyperlink,$target,$tipo)
	{
		$this->hyperlink = $hyperlink;
		$this->target = $target;
		$this->tipo = $tipo;
	}
	function onclick($script)
	{
		$this->onclick = "onclick=\"".$script."\"";
	}
	function stile($stile)
	{
		switch ($stile)
		{
		   case "upper":
				$this->prezzo = strtoupper($this->prezzo);
				break;
		   case "lower":
				$this->prezzo = strtolower($this->prezzo);
				break;
		}
	}
	function vedi()
	{
		if ($this->esiste)
		{
			if ($this->hyperlink != "")
			{
				if ($this->tipo == "ext")
				{
					$this->prezzo = str_replace("http://","",$this->prezzo);
					$this->hyperlink = "http://".str_replace("http://","",$this->hyperlink);
?>
<span valign="top"><a href="<?php echo $this->hyperlink; ?>" class="<?php echo $this->classe; ?>" target="<?php echo $this->target; ?>" <?php echo $this->onclick; ?>><?php echo $this->prezzo; ?></a></span>
<?php
				}
				else
				{
?>
<span valign="top"><a href="<?php echo $this->hyperlink; ?>" class="<?php echo $this->classe; ?>" target="<?php echo $this->target; ?>" <?php echo $this->onclick; ?>><?php echo $this->prezzo; ?></a></span>
<?php
				}
			}
			else
			{
?>
<span valign="top" class="<?php echo $this->classe; ?>" <?php echo $this->onclick; ?>><?php echo $this->prezzo; ?></span>
<?php
			}
		}
	}
	function manager()
	{
		if ($this->hyperlink != "")
		{
			if ($this->tipo == "ext")
			{
				$this->prezzo = str_replace("http://","",$this->prezzo);
				$this->hyperlink = "http://".str_replace("http://","",$this->hyperlink);
?>
<span id="prezzo_<?php echo $this->tabella; ?>_<?php echo $this->nome; ?>_<?php echo $this->id; ?>" class="bg_varchar" valign="top" oncontextmenu="Lightview.show({ url: 'http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>istant/prezzo/modale.php?t=<?php echo $this->tabella; ?>&i=<?php echo $this->id; ?>&n=<?php echo $this->nome; ?>', options: { width: 455, height: 425 }, type: 'iframe' });return false;"><a href="<?php echo $this->hyperlink; ?>" class="<?php echo $this->classe; ?>" target="<?php echo $this->target; ?>" <?php echo $this->onclick; ?>><?php echo $this->prezzo; ?></a></span>
<?php
			}
			else
			{
?>
<span id="prezzo_<?php echo $this->tabella; ?>_<?php echo $this->nome; ?>_<?php echo $this->id; ?>" class="bg_varchar" valign="top" oncontextmenu="Lightview.show({ url: 'http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>istant/prezzo/modale.php?t=<?php echo $this->tabella; ?>&i=<?php echo $this->id; ?>&n=<?php echo $this->nome; ?>', options: { width: 455, height: 425 }, type: 'iframe' });return false;"><a href="<?php echo $this->hyperlink; ?>" class="<?php echo $this->classe; ?>" target="<?php echo $this->target; ?>" <?php echo $this->onclick; ?>><?php echo $this->prezzo; ?></a></span>
<?php
			}
		}
		else
		{
?>
<span id="prezzo_<?php echo $this->tabella; ?>_<?php echo $this->nome; ?>_<?php echo $this->id; ?>" class="bg_varchar <?php echo $this->classe; ?>" valign="top"  oncontextmenu="Lightview.show({ url: 'http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>istant/prezzo/modale.php?t=<?php echo $this->tabella; ?>&i=<?php echo $this->id; ?>&n=<?php echo $this->nome; ?>', options: { width: 455, height: 425 }, type: 'iframe' });return false" <?php echo $this->onclick; ?>><?php echo $this->prezzo; ?></span>
<?php
		}
	}
	function plus($extra, $testo)
	{
		$this->prezzo = $testo;
		if ($this->hyperlink > 0)
		{
?>
<span id="prezzo_<?php echo $this->tabella; ?>_<?php echo $this->nome; ?>_0" class="bg_varchar" valign="top" oncontextmenu="Lightview.show({ url: 'http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>istant/prezzo/modale.php?t=<?php echo $this->tabella; ?>&i=<?php echo $this->id; ?>&n=<?php echo $this->nome; ?>&e=<?php echo $extra; ?>', options: { width: 455, height: 425 }, type: 'iframe' });return false;"><a href="index.php?id_page=<?php echo $this->hyperlink; ?>" class="<?php echo $this->classe; ?>" target="_top"><?php echo $this->prezzo; ?></a></span>
<?php
		}
		else
		{
?>
<span id="prezzo_<?php echo $this->tabella; ?>_<?php echo $this->nome; ?>_0" valign="top" class="bg_varchar <?php echo $this->classe; ?>" oncontextmenu="Lightview.show({ url: 'http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>istant/prezzo/modale.php?t=<?php echo $this->tabella; ?>&i=<?php echo $this->id; ?>&n=<?php echo $this->nome; ?>&e=<?php echo $extra; ?>', options: { width: 455, height: 425 }, type: 'iframe' });return false;"><?php echo $this->prezzo; ?></span>
<?php
		}
	}
}
?>