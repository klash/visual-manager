<?php
class permalink
{
    var $lingua;
    var $trovato;
    var $tabella;
    var $id;
    var $nome;
    var $classe;
    var $testo;
    var $permalink;
    var $hyperlink;
    var $target;
    var $tipo;
    var $ancor;
    var $esiste;
    var $posizione;
    var $rule;
    var $attivo;
    var $pre;
    var $post;
    var $reload;
	var $tokens;
	var $delete;
	var $rss;
	function lingua($lingua)
	{
		$this->lingua = $lingua;
	}
	function init($tabella, $id, $nome, $classe)
	{
		$this->tabella = $tabella;
		$this->id = $id;
		$this->nome = $nome;
		$this->classe = $classe;
		$this->trovato = 0;
		if($this->id > 0)
		{
			if($this->lingua > 0)
			{
				$query = "SELECT * FROM ".$this->tabella." WHERE primario = '".$this->id."' AND lingua = '".$this->lingua."'";
				$risultato = mysql_query($query);
				$this->trovato = mysql_num_rows($risultato);
				if($this->trovato > 0)
				{
					$riga = mysql_fetch_array($risultato);
				}
				else
				{
					$query = "SELECT * FROM ".$this->tabella." WHERE ID = '".$this->id."'";
					$risultato = mysql_query($query);
					$riga = mysql_fetch_array($risultato);
				}
			}
			else
			{
				$query = "SELECT * FROM ".$this->tabella." WHERE ID = '".$this->id."'";
				$risultato = mysql_query($query);
				$riga = mysql_fetch_array($risultato);
				$this->trovato = mysql_num_rows($risultato);
			}
			$this->testo = stripslashes(trim($riga[$this->nome]));
			$this->permalink = stripslashes(trim($riga['permalink']));
		}
		if($this->testo == "")
		{
			$this->testo = $nome;
			$this->esiste = 0;
		}
		else
		{
			$this->esiste = 1;
		}
		$query = "SELECT * FROM tabella WHERE nome = '".$this->tabella."'";
		$risultato = mysql_query($query);
		$riga = mysql_fetch_array($risultato);
		if($riga['attivo'] == "si")
		{
			$query = "SELECT * FROM ".$this->tabella." WHERE ID = '".$this->id."'";
			$risultato = mysql_query($query);
			$riga = mysql_fetch_array($risultato);
			$this->attivo = $riga['attivo'];
		}
	}
	function rule($rule)
	{
		$this->rule = $rule;
	}
	function delete($delete)
	{
		$this->delete = $delete;
	}
	function rss($title, $description, $url, $image)
	{
		if($this->esiste)
		{
			$this->rss['title'] = $title;
			$this->rss['description'] = $description;
			$this->rss['url'] = $url;
			$this->rss['image'] = $image;
			if($this->lingua > 0)
			{
				$query = "SELECT * FROM ".$this->tabella." WHERE primario = '".$this->id."' AND lingua = '".$this->lingua."'";
				$risultato = mysql_query($query);
				$riga = mysql_fetch_array($risultato);
				$this->rss['id'] = $riga['ID'];
			}
			else
			{
				$this->rss['id'] = $this->id;
			}
			$query = "SELECT * FROM rss WHERE tabella = '".$this->tabella."' AND tabella_id = '".$this->rss['id']."'";
			$risultato = mysql_query($query);
			$trovato = mysql_num_rows($risultato);
			if($trovato > 0)
			{
				$this->rss['stato'] = "linked";
				$riga = mysql_fetch_array($risultato);
				if($riga['url'] != $this->rss['url'])
				{
					$query = "UPDATE rss SET url = '".$this->rss['url']."' WHERE tabella = '".$this->tabella."' AND tabella_id = '".$this->rss['id']."'";
					mysql_query($query);
				}
			}
			else
			{
				$this->rss['stato'] = "unlink";
			}
		}
	}
	function posizione()
	{
		$query = "SELECT * FROM ".$this->tabella." WHERE ID = '".$this->id."'";
		$risultato = mysql_query($query);
		$riga = mysql_fetch_array($risultato);
		$this->posizione['id'] = $riga['posizione'];
		if($this->lingua > 0)
		{
			$this->rule['lingua'] = "ID = primario";
		}
		if(count($this->rule) > 0)
		{
			$query = "SELECT * FROM ".$this->tabella." WHERE ".implode(" AND ",$this->rule);
		}
		else
		{
			$query = "SELECT * FROM ".$this->tabella;
		}
		$risultato = mysql_query($query);
		$trovato = mysql_num_rows($risultato);
		$this->posizione['trovato'] = $trovato;
	}
	function h($numero)
	{
		$this->testo = "<H".$numero.">".$this->testo."</H".$numero.">";
	}
	function pre($pre)
	{
		$this->testo = $pre.$this->testo;
	}
	function post($post)
	{
		$this->testo = $this->testo.$post;
	}
	function reload($reload)
	{
		$this->reload = $reload;
	}
	function hyperlink($hyperlink,$target,$tipo)
	{
		$this->hyperlink = $hyperlink;
		$this->target = $target;
		$this->tipo = $tipo;
	}
	function ancor($ancor)
	{
		$this->ancor = "#".$ancor;
	}
	function stile($stile)
	{
		switch ($stile)
		{
		   case "upper":
				$this->testo = strtoupper($this->testo);
				break;
		   case "lower":
				$this->testo = strtolower($this->testo);
				break;
		   case "bold":
				$this->testo = "<strong>".$this->testo."</strong>";
				break;
		   case "italic":
				$this->testo = "<em>".$this->testo."</em>";
				break;
		}
	}
	function vedi()
	{
		if ($this->esiste)
		{
			if ($this->hyperlink != "")
			{
?>
<a href="<?php echo $this->hyperlink; ?><?php echo $this->permalink; ?>.html<?php echo $this->ancor; ?>" class="<?php echo $this->classe; ?>" target="<?php echo $this->target; ?>"><?php echo $this->testo; ?></a>
<?php
			}
			else
			{
?>
<span valign="top" class="<?php echo $this->classe; ?>"><?php echo $this->testo; ?></span>
<?php
			}
		}
	}
	function manager()
	{
		if ($this->delete)
		{
?>
<img src="http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>manager/delete.png" alt="cancella" title="cancella" style="vertical-align:baseline; cursor:pointer;" oncontextmenu="cancella('<?php echo htmlspecialchars($this->testo); ?>','<?php echo $this->tabella; ?>',<?php echo $this->id; ?>,'<?php echo $this->delete; ?>','<?php echo implode("','",$this->rule); ?>'); return false;" border="0" style="vertical-align:bottom; cursor:pointer;" />
<?php
		}
		if ($this->attivo == 'si')
		{
?>
<img src="http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>manager/on.png" alt="disattiva" title="disattiva" style="vertical-align:baseline; cursor:pointer;" oncontextmenu="attivo('<?php echo $this->tabella; ?>',<?php echo $this->id; ?>);return false;" />
<?php
		}
		if ($this->attivo == 'no')
		{
?>
<img src="http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>manager/off.png" alt="attiva" title="attiva" style="vertical-align:baseline; cursor:pointer;" oncontextmenu="attivo('<?php echo $this->tabella; ?>',<?php echo $this->id; ?>);return false;" />
<?php
		}
		if ($this->posizione)
		{
			if($this->posizione['id'] > 1)
			{
?>
<img src="http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>manager/su.png" alt="sposta sopra" title="sposta sopra" style="vertical-align:baseline; cursor:pointer;" oncontextmenu="sposta('<?php echo $this->tabella; ?>',<?php echo $this->id; ?>,'up','<?php echo implode("','",$this->rule); ?>');return false;" />
<?php
			}
			if($this->posizione['id'] < $this->posizione['trovato'])
			{
?>
<img src="http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>manager/giu.png" alt="sposta sotto" title="sposta sotto" style="vertical-align:baseline; cursor:pointer;" oncontextmenu="sposta('<?php echo $this->tabella; ?>',<?php echo $this->id; ?>,'down','<?php echo implode("','",$this->rule); ?>');return false;" />
<?php
			}
		}
		if ($this->rss['stato'] == 'linked')
		{
?>
<img src="http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>manager/linked.png" alt="unlink" title="unlink" style="vertical-align:baseline; cursor:pointer;" oncontextmenu="rss('<?php echo $this->tabella; ?>',<?php echo $this->rss['id']; ?>, '<?php echo $this->rss['title']; ?>', '<?php echo $this->rss['description']; ?>', '<?php echo $this->rss['url']; ?>', '<?php echo $this->rss['image']; ?>'); return false;" />
<?php
		}
		if ($this->rss['stato'] == 'unlink')
		{
?>
<img src="http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>manager/unlink.png" alt="link" title="link" style="vertical-align:baseline; cursor:pointer;" oncontextmenu="rss('<?php echo $this->tabella; ?>',<?php echo $this->rss['id']; ?>, '<?php echo $this->rss['title']; ?>', '<?php echo $this->rss['description']; ?>', '<?php echo $this->rss['url']; ?>', '<?php echo $this->rss['image']; ?>'); return false;" />
<?php
		}
		if ($this->hyperlink != "")
		{
?>
<a id="permalink_<?php echo $this->tabella; ?>_<?php echo $this->nome; ?>_<?php echo $this->id; ?>" href="<?php echo $this->hyperlink; ?><?php echo $this->permalink; ?>.html<?php echo $this->ancor; ?>" class="bg_varchar <?php echo $this->classe; ?>" target="<?php echo $this->target; ?>" oncontextmenu="Lightview.show({ url: 'http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>istant/permalink/modale.php?t=<?php echo $this->tabella; ?>&i=<?php echo $this->id; ?>&n=<?php echo $this->nome; ?>&h=<?php echo $this->hyperlink; ?>', options: { width: 455, height: 425 }, type: 'iframe' });return false;"><?php echo $this->testo; ?></a>
<?php
		}
		else
		{
?>
<span id="permalink_<?php echo $this->tabella; ?>_<?php echo $this->nome; ?>_<?php echo $this->id; ?>" valign="top" class="bg_varchar <?php echo $this->classe; ?>" oncontextmenu="Lightview.show({ url: 'http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>istant/permalink/modale.php?t=<?php echo $this->tabella; ?>&i=<?php echo $this->id; ?>&n=<?php echo $this->nome; ?>&h=<?php echo $this->hyperlink; ?>', options: { width: 455, height: 425 }, type: 'iframe' });return false;"><?php echo $this->testo; ?></span>
<?php
		}
	}
	function plus($extra, $testo)
	{
		$this->testo = $testo;
?>
<span valign="top" class="bg_varchar <?php echo $this->classe; ?>" oncontextmenu="Lightview.show({ url: 'http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>istant/permalink/modale.php?t=<?php echo $this->tabella; ?>&i=<?php echo $this->id; ?>&n=<?php echo $this->nome; ?>&h=<?php echo $this->hyperlink; ?>&e=<?php echo $extra; ?>', options: { width: 455, height: 425 }, type: 'iframe' });return false;"><?php echo $this->testo; ?></span>
<?php
	}
	function menu($function, $tabella, $nome, $crtl, $hyperlink, $classe)
	{
		$where = "";
		if(count($this->rule) > 0)
		{
			$where = " AND ".stripslashes(implode(" AND ",$this->rule));
		}
		if($function == "manager")
		{
?>
<script>
$(function() {
	$("#sortable_<?php echo $tabella; ?>_<?php echo $nome; ?>").sortable({
		placeholder: "widget-highlight",
		update: function(event, ui) {
			var tabella = "<?php echo $tabella; ?>";
			var ordine = $('#sortable_<?php echo $tabella; ?>_<?php echo $nome; ?>').sortable('toArray'); 
			path = "http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>istant/permalink/sortable.php";
			$.ajax({
				type: "POST",
				url: path,
				data: "tabella="+tabella+"&ordine="+ordine,
				success: function(msg) {
//					alert(msg)
				}
			});
		},
	});
	$("#sortable_<?php echo $tabella; ?>_<?php echo $nome; ?>").disableSelection();
});
</script>
<?php
		}
?>
  <ul id="sortable_<?php echo $tabella; ?>_<?php echo $nome; ?>" class="<?php echo $classe; ?>">
<?php
		$nome_obj = "menu_".$tabella;
		if($function == "manager")
		{
			$query = "SELECT * FROM ".$tabella." WHERE ID = primario ".$where." ORDER BY posizione";
		}
		else
		{
			$query = "SELECT * FROM ".$tabella." WHERE attivo = 'si' AND ID = primario ".$where." ORDER BY posizione";
		}
		$risultato = mysql_query($query);
		while ($riga = mysql_fetch_array($risultato))
		{
			if($crtl == $riga['primario'])
			{
				$$nome_obj[$riga['primario']] = new permalink;
				$$nome_obj[$riga['primario']]->init($tabella,$riga['primario'],"menu",$classe);
				$$nome_obj[$riga['primario']]->rule($this->rule);
				$$nome_obj[$riga['primario']]->delete($hyperlink.".html");
				$$nome_obj[$riga['primario']]->reload($_SERVER['REQUEST_URI']);
			}
			else
			{
				$$nome_obj[$riga['primario']] = new permalink;
				$$nome_obj[$riga['primario']]->init($tabella,$riga['primario'],"menu",$classe);
				$$nome_obj[$riga['primario']]->rule($this->rule);
				$$nome_obj[$riga['primario']]->delete($hyperlink.".html");
				$$nome_obj[$riga['primario']]->hyperlink($hyperlink."/","_self","");
				$$nome_obj[$riga['primario']]->reload($_SERVER['REQUEST_URI']);
			}
?>
    <li id="<?php echo $riga['primario']; ?>"><?php echo $$nome_obj[$riga['primario']]->$function(); ?></li>
<?php
		}
?>
  </ul>
<?php
	}
	function translate($grantType, $scopeUrl, $clientID, $clientSecret, $authUrl)
	{
		if($this->trovato == 0)
		{
			$query = "SELECT * FROM lingua WHERE primario = 'si'";
			$risultato = mysql_query($query);
			$riga = mysql_fetch_array($risultato);
			$from = $riga['path'];
			$query = "SELECT * FROM ".$this->tabella." WHERE ID = '".$this->id."'";
			$risultato = mysql_query($query);
			$riga = mysql_fetch_array($risultato);
			$testo = stripslashes(trim($riga[$this->nome]));
			$query = "SELECT * FROM lingua WHERE ID = '".$this->lingua."'";
			$risultato = mysql_query($query);
			$riga = mysql_fetch_array($risultato);
			$to = $riga['path'];
			$accessToken = $this->getBingTokens($grantType, $scopeUrl, $clientID, $clientSecret, $authUrl);
//			ExpiresOn
			echo urldecode($accessToken);
			$tokens = parse_str(urldecode($accessToken));
			var_dump($tokens);
			$params = array();
			$client = new SoapClient("http://api.microsofttranslator.com/V2/SOAP.svc");
			$params['appId'] = "Bearer ".$accessToken;
			$params['text'] = $this->testo;
			$params['from'] = $from;
			$params['to'] = $to;
			$result = $client->Translate($params);
			$this->testo = $result->TranslateResult;
		}
	}
    function getBingTokens($grantType, $scopeUrl, $clientID, $clientSecret, $authUrl)
	{
        try {
            //Initialize the Curl Session.
            $ch = curl_init();
            //Create the request Array.
            $paramArr = array (
             'grant_type'    => $grantType,
             'scope'         => $scopeUrl,
             'client_id'     => $clientID,
             'client_secret' => $clientSecret
            );
            //Create an Http Query.//
            $paramArr = http_build_query($paramArr);
            //Set the Curl URL.
            curl_setopt($ch, CURLOPT_URL, $authUrl);
            //Set HTTP POST Request.
            curl_setopt($ch, CURLOPT_POST, TRUE);
            //Set data to POST in HTTP "POST" Operation.
            curl_setopt($ch, CURLOPT_POSTFIELDS, $paramArr);
            //CURLOPT_RETURNTRANSFER- TRUE to return the transfer as a string of the return value of curl_exec().
            curl_setopt ($ch, CURLOPT_RETURNTRANSFER, TRUE);
            //CURLOPT_SSL_VERIFYPEER- Set FALSE to stop cURL from verifying the peer's certificate.
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            //Execute the  cURL session.
            $strResponse = curl_exec($ch);
            //Get the Error Code returned by Curl.
            $curlErrno = curl_errno($ch);
            if($curlErrno){
                $curlError = curl_error($ch);
                throw new Exception($curlError);
            }
            //Close the Curl Session.
            curl_close($ch);
            //Decode the returned JSON string.
            $objResponse = json_decode($strResponse);
                
            if ($objResponse->error){
                throw new Exception($objResponse->error_description);
            }
            return $objResponse->access_token;
        } catch (Exception $e) {
            echo "Exception-".$e->getMessage();
        }
    }
}
?>