<?php
class allegato
{
    var $tabella;
    var $contenuto;
    var $nome;
    var $id;
    var $testo;
    var $titolo;
    var $info;
    var $dida;
    var $ico;
    var $classe;
    var $ext;
    var $path;
    var $dimensione;
    var $esiste;
    var $reload;
	function init($tabella, $id, $nome, $classe)
	{
		$this->tabella = $tabella;
		$this->id = $id;
		$this->nome = $nome;
		$this->classe = $classe;
		$query = "SELECT * FROM ".$this->tabella." WHERE ID = '".$this->id."'";
		$risultato = mysql_query($query);
		$riga = mysql_fetch_array($risultato);
		$ext = explode(".",$riga[$this->nome]);
		$this->ext = $ext[count($ext)-1];
		$this->titolo = str_replace(".".$this->ext,"",$riga[$this->nome]);
		$this->path = "allegato/".$this->tabella."_".$this->nome."_".$this->id."_".$riga[$this->nome];
		if (file_exists($this->path))
		{
			$this->esiste = 1;
			$dimensione = filesize($this->path);
			if(($dimensione/1024) < 1)
			{
				$this->dimensione = $dimensione." byte";
			}
			else
			{
				$this->dimensione = intval($dimensione/1024)." Kb";
			}
			$this->path = $_SESSION['path'].$this->path;
		}
		else
		{
			$this->esiste = 0;
			$this->titolo = "nessun allegato";
		}
	}
	function view($ico, $testo)
	{
		$this->ico = "<img src='http://".$_SERVER['SERVER_NAME'].$_SESSION['path'].$ico."' border='0' align='absbottom'>";
		$this->testo = $testo;
	}
	function info()
	{
		$this->info = $this->titolo." [".$this->ext."] [".$this->dimensione."]";
	}
	function reload($reload)
	{
		$this->reload = $reload;
	}
	function manager()
	{
		if ($this->esiste)
		{
?>
<a id="allegato_<?php echo $this->tabella; ?>_<?php echo $this->nome; ?>_<?php echo $this->id; ?>" valign="top" href="<?php echo $this->path; ?>" class="<?php echo $this->classe; ?> bg_varchar" target="_blank" oncontextmenu="Lightview.show({ url: 'http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>istant/allegato/modale.php?t=<?php echo $this->tabella; ?>&i=<?php echo $this->id; ?>&n=<?php echo $this->nome; ?>', options: { width: 455, height: 425 }, type: 'iframe' });return false;"><?php echo $this->ico; ?> <?php echo $this->testo; ?><br /><?php echo $this->info; ?></a>
<?php
		}
		else
		{
?>
<span id="allegato_<?php echo $this->tabella; ?>_<?php echo $this->nome; ?>_<?php echo $this->id; ?>" class="<?php echo $this->classe; ?> bg_varchar" valign="top" oncontextmenu="Lightview.show({ url: 'http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>istant/allegato/modale.php?t=<?php echo $this->tabella; ?>&i=<?php echo $this->id; ?>&n=<?php echo $this->nome; ?>', options: { width: 455, height: 425 }, type: 'iframe' });return false;"><?php echo $this->ico; ?> <?php echo $this->testo; ?></span>
<?php
		}
	}
	function vedi()
	{
		if ($this->esiste)
		{
?>
<a id="allegato_<?php echo $this->tabella; ?>_<?php echo $this->nome; ?>_<?php echo $this->id; ?>" valign="top" href="<?php echo $this->path; ?>" class="<?php echo $this->classe; ?>" target="_blank"><?php echo $this->ico; ?> <?php echo $this->testo; ?><br /><?php echo $this->info; ?></a>
<?php
		}
	}
}
?>