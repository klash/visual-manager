<?php
class videogallery
{
    var $tabella;
    var $id;
    var $nome;
    var $classe;
    var $color;
    var $div;
    var $video;
    var $dida;
    var $obj;
    var $posizione;
    var $dimensione;
    var $gutter;
    var $margin;
    var $larghezza;
    var $altezza;
    var $esiste;
    var $reload;
	function init($tabella, $id, $nome, $classe, $color)
	{
		$this->tabella = $tabella;
		$this->id = $id;
		$this->nome = $nome;
		$this->classe = $classe;
		$this->color = $color;
		$this->video = array();
		$this->dida = array();
		$this->div = array();
		$this->obj = array();
		$this->posizione = array();
		$query_tabella = "SELECT * FROM tabella WHERE nome = '".$tabella."'";
//print $query_tabella."<br>";
		$risultato_tabella = mysql_query($query_tabella);
		$riga_tabella = mysql_fetch_array($risultato_tabella);
		if($riga_tabella['lingua'] == 'si')
		{
			$query = "SELECT * FROM ".$tabella."_video WHERE ".$tabella." = ".$this->id." AND ID = primario ORDER BY posizione";
//print $query."<br>";
			$risultato = mysql_query($query);
			while ($riga = mysql_fetch_array($risultato))
			{
				$this->video[$riga['primario']] = stripslashes(html_entity_decode($riga[$this->nome]));
//print $this->video[$riga['primario']]."<br>";
				$query_vedi = "SELECT * FROM ".$tabella."_video WHERE primario = '".$riga['primario']."' AND lingua = '".$_SESSION['language']['id']."'";
				$risultato_vedi = mysql_query($query_vedi);
				$riga_vedi = mysql_fetch_array($risultato_vedi);
				if($riga_vedi['descrizione'] != "descrizione")
				{
					$this->dida[$riga['primario']] = $riga_vedi['descrizione'];
				}
				else
				{
					$this->dida[$riga['primario']] = "";
				}
				$this->div[$riga['primario']] = $this->tabella."_".$this->nome."_".$this->id."_".$riga['primario'];
				$this->posizione[$riga['primario']] = $riga['posizione'];
				$this->obj[$riga['ID']] = $riga['primario'];
			}
		}
		else
		{
			$query = "SELECT * FROM ".$tabella."_video WHERE ".$tabella." = ".$this->id." ORDER BY posizione";
//print $query."<br>";
			$risultato = mysql_query($query);
			while ($riga = mysql_fetch_array($risultato))
			{
				$this->video[$riga['ID']] = html_entity_decode($riga[$this->nome]);
				if($riga['descrizione'] != "descrizione")
				{
					$this->dida[$riga['ID']] = $riga['descrizione'];
				}
				else
				{
					$this->dida[$riga['ID']] = "";
				}
				$this->div[$riga['ID']] = $this->tabella."_".$this->nome."_".$this->id."_".$riga['ID'];
				$this->posizione[$riga['ID']] = $riga['posizione'];
				$this->obj[$riga['ID']] = $riga['ID'];
//print $riga['numero']." > ".$this->foto[$riga['ID']]."<br>";
			}
		}
		if(count($this->video) > 0)
		{
			$this->esiste = 1;
		}
		else
		{
			$this->esiste = 0;
		}
	}
	function dimensioni($larghezza, $altezza, $gutter, $margin)
	{
		$this->dimensione = $larghezza.":".$altezza.":".$margin;
		$this->larghezza = $larghezza;
		$this->altezza = $altezza;
		$this->gutter = $gutter;
		$larghezza = $larghezza - $margin;
		if(count($this->video) > 0)
		{
			reset($this->video);
			while (list($key, $val) = each($this->video))
			{
				$patterns = array();
				$replacements = array();
				if($larghezza > 0)
				{
					$patterns[] = '/width="([0-9]+)"/';
					$patterns[] = "/width='([0-9]+)'/";
					$patterns[] = "/width:([0-9]+)/";
					$replacements[] = 'width="'.$larghezza.'"';
					$replacements[] = "width='".$larghezza."'";
					$replacements[] = "width:".$larghezza;
				}
				if($altezza > 0)
				{
					$patterns[] = '/height="([0-9]+)"/';
					$patterns[] = "/height='([0-9]+)'/";
					$patterns[] = "/height:([0-9]+)/";
					$replacements[] = 'height="'.$altezza.'"';
					$replacements[] = "height='".$altezza."'";
					$replacements[] = "height:".$altezza;
				}
				$this->video[$key] = preg_replace($patterns, $replacements, $val);
			}
		}
	}
	function reload($reload)
	{
		$this->reload = $reload;
	}
	function manager()
	{
		if(count($this->video) > 0)
		{
?>
<script>
$(function() {
	$("#sortable_videogallery_<?php echo $this->tabella; ?>_<?php echo $this->id; ?>").sortable({
		placeholder: "widget-highlight",
		update: function(event, ui) {
			var tabella = "<?php echo $this->tabella; ?>";
			var ordine = $('#sortable_videogallery_<?php echo $this->tabella; ?>_<?php echo $this->id; ?>').sortable('toArray'); 
			path = "http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>istant/videogallery/sortable.php";
			$.ajax({
				type: "POST",
				url: path,
				data: "tabella="+tabella+"&ordine="+ordine,
				success: function(msg) {
					//alert(msg)
				}
			});
		},
	});
	$("#sortable_videogallery_<?php echo $this->tabella; ?>_<?php echo $this->id; ?>").disableSelection();
});
</script>
<ul id="sortable_videogallery_<?php echo $this->tabella; ?>_<?php echo $this->id; ?>" style="padding:0px !important; list-style-type: none">
<?php
			reset($this->video);
			while (list($key, $val) = each($this->video))
			{
				if (trim($val) != "")
				{
?>
<li id="videogallery_<?php echo $this->div[$key]; ?>" style="float:left; margin-right:<?php echo $this->gutter; ?>px; margin-bottom:<?php echo $this->gutter; ?>px; width:<?php echo $this->larghezza; ?>px; height:<?php echo $this->altezza; ?>px; background-color:<?php echo $this->color; ?>;">
<table border="0" cellspacing="0" cellpadding="0" class="bg_allegato" oncontextmenu="Lightview.show({ url: 'http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>istant/videogallery/modale.php?t=<?php echo $this->tabella; ?>&i=<?php echo $this->id; ?>&n=<?php echo $this->nome; ?>&o=<?php echo $this->obj[$key]; ?>&k=<?php echo $this->posizione[$key]; ?>&d=<?php echo $this->dimensione; ?>', options: { width: 455, height: 625 }, type: 'iframe' });return false;">
  <tr>
    <td id="video_<?php echo $this->div[$key]; ?>" valign="top"><?php echo $val; ?></td>
    <td id="dida_<?php echo $this->div[$key]; ?>" valign="top" width="<?php echo $this->margin; ?>" style="padding:10px" class="<?php echo $this->classe; ?>"><?php echo $this->dida[$key]; ?></td>
  </tr>
</table>
</li>
<?php
				}
				else
				{
?>
<li id="videogallery_<?php echo $this->div[$key]; ?>" style="float:left; margin-right:<?php echo $this->gutter; ?>px; margin-bottom:<?php echo $this->gutter; ?>px; width:<?php echo $this->larghezza; ?>px; height:<?php echo $this->altezza; ?>px; background-color:<?php echo $this->color; ?>;">
<table border="0" cellspacing="0" cellpadding="0" class="bg_allegato" oncontextmenu="Lightview.show({ url: 'http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>istant/videogallery/modale.php?t=<?php echo $this->tabella; ?>&i=<?php echo $this->id; ?>&n=<?php echo $this->nome; ?>&o=<?php echo $this->obj[$key]; ?>&k=<?php echo $this->posizione[$key]; ?>&d=<?php echo $this->dimensione; ?>', options: { width: 455, height: 625 }, type: 'iframe' });return false;">
  <tr>
    <td id="video_<?php echo $this->div[$key]; ?>" valign="top">&nbsp;</td>
    <td id="dida_<?php echo $this->div[$key]; ?>" valign="top" width="<?php echo $this->margin; ?>" style="padding:10px" class="<?php echo $this->classe; ?>">inserisci video</td>
  </tr>
</table>
</li>
<?php
				}
			}
?>
</ul>
<?php
		}
	}
	function vedi()
	{
		if(count($this->video) > 0)
		{
			reset($this->video);
			while (list($key, $val) = each($this->video))
			{
				if (trim($val) != "")
				{
?>
<div id="videogallery_<?php echo $this->div[$key]; ?>" style="float:left; margin-right:<?php echo $this->gutter; ?>px; margin-bottom:<?php echo $this->gutter; ?>px; width:<?php echo $this->larghezza; ?>px; height:<?php echo $this->altezza; ?>px; background-color:<?php echo $this->color; ?>;">
<table border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td valign="top"><?php echo $val; ?></td>
    <td valign="top" width="<?php echo $this->margin; ?>" style="padding:10px" class="<?php echo $this->classe; ?>"><?php echo $this->dida[$key]; ?></td>
  </tr>
</table>
</div>
<?php
				}
			}
		}
	}
	function plus($tabella, $id, $nome, $testo, $classe)
	{
		$this->tabella = $tabella;
		$this->id = $id;
		$this->nome = $nome;
		$this->classe = $classe;
?>
<div id="videogallery_0" class="bg_allegato" style="float:left; margin-right:<?php echo $this->gutter; ?>px; margin-bottom:<?php echo $this->gutter; ?>px; width:<?php echo $this->larghezza; ?>px; height:<?php echo $this->altezza; ?>px;">
<table width="<?php echo $this->larghezza; ?>" height="<?php echo $this->altezza; ?>" border="0" cellspacing="0" cellpadding="0" class="bg_allegato" oncontextmenu="Lightview.show({ url: 'http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>istant/videogallery/modale.php?t=<?php echo $this->tabella; ?>&i=<?php echo $this->id; ?>&n=<?php echo $this->nome; ?>&o=0&k=0&d=<?php echo $this->dimensione; ?>', options: { width: 455, height: 625 }, type: 'iframe' });return false;">
  <tr>
    <td valign="top">&nbsp;</td>
    <td valign="top" width="<?php echo $this->margin; ?>" style="padding:10px" class="<?php echo $this->classe; ?>"><?php echo $testo; ?></td>
  </tr>
</table>
</div>
<?php
	}
}
?>
