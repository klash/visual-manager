<?php
session_start();
reset($_SESSION);
require("../../config.inc.php");
?>
<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
<script type="text/javascript">
function data(azione){
	path = "http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>istant/data/"+azione+".php";
	obj_extra=$("#obj_extra").val();
	obj_tabella=$("#obj_tabella").val();
	obj_id=$("#obj_id").val();
	obj_nome=$("#obj_nome").val();
	obj_giorno=$("#obj_giorno").val();
	obj_mese=$("#obj_mese").val();
	obj_anno=$("#obj_anno").val();
	$.ajax({
		type: "POST",
		url: path,
		data: "obj_extra="+obj_extra+"&obj_tabella="+obj_tabella+"&obj_id="+obj_id+"&obj_nome="+obj_nome+"&obj_giorno="+obj_giorno+"&obj_mese="+obj_mese+"&obj_anno="+obj_anno,
		success: function(msg) {
			if(msg == "errore")
			{
				alert("data non corretta");
			}
			else
			{
				if(azione == 'modifica')
				{
					parent.$("#data_"+obj_tabella+"_"+obj_nome+"_"+obj_id).html(msg);
				}
				else
				{
					parent.document.location.reload();
				}
				parent.Lightview.hide();
			}
		}
	});
}
</script>
<html>
<head>
<title>data</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<?php
$query = "SELECT DAY(".$_GET['n'].") AS giorno, MONTH(".$_GET['n'].") AS mese, YEAR(".$_GET['n'].") AS anno FROM ".$_GET['t']." WHERE ID = '".$_GET['i']."'";
$risultato = mysql_query($query);
$riga = mysql_fetch_array($risultato);
if(checkdate($riga['mese'],$riga['giorno'],$riga['anno']))
{
	$giorno = $riga['giorno'];
	$mese = $riga['mese'];
	$anno = $riga['anno'];
}
else
{
	$giorno = 'gg';
	$mese = 'mm';
	$anno = 'aaaa';
}
?>
<link href="../popup.css" rel="stylesheet" type="text/css">
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" bgcolor="#FFFFFF">
<table width="100%" height="70" border="0" cellpadding="0" cellspacing="0" bgcolor="#004185">
  <tr>
    <td width="30">&nbsp;</td>
<?php
if($riga_tabella['lingua'] == 'si')
{
?>
    <td valign="bottom" class="pop_title"><?php echo $_SESSION['language']['path']; ?> / inserisci o modifica data</td>
<?php
}
else
{
?>
    <td valign="bottom" class="pop_title">inserisci o modifica data</td>
<?php
}
?>
  </tr>
</table>
<div style="height:10px; background-color:#004185;"></div>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="30">&nbsp;</td>
    <td valign="top">
    <div style="height:10px;"></div>
<?php
if($_GET['i'] > 0)
{
?>
    <span class="pop_operazione">stai modificando:</span>
<?php
}
else
{
?>
    <span class="pop_operazione">stai aggiungendo:</span>
<?php
}
?>
    <span class="pop_breadcrumbs"><?php include("../breadcrumbs_popup.php"); ?></span>
    <div style="height:10px;"></div>
    <div style="border-bottom:1px dotted #000000;"></div>
    <span class="pop_info">per aggiungere o modificare la data inserire i valori. il sistema verificher&ograve; l'esattezza della data inserita.<br /><span class="pop_errore"><?php echo $errore; ?></span></span>
    <div style="height:10px;"></div>
    <div style="border-bottom:1px dotted #000000;"></div>
    <div style="height:10px;"></div>
	<input id="obj_extra" type="hidden" value="<?php echo $_GET['e']; ?>">
	<input id="obj_tabella" type="hidden" value="<?php echo $_GET['t']; ?>">
	<input id="obj_id" type="hidden" value="<?php echo $_GET['i']; ?>">
	<input id="obj_nome" type="hidden" value="<?php echo $_GET['n']; ?>">
    <div style="height:10px;"></div>
	<table border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td><input id="obj_giorno" name="giorno" type="text" value="<?php echo $giorno; ?>" class="pop_data" size="2" maxlength="2" onFocus="if(this.value=='<?php echo $giorno; ?>')this.value='';" onBlur="if(this.value=='')this.value='<?php echo $giorno; ?>';"></td>
        <td width="10">&nbsp;</td>
        <td><input id="obj_mese" name="mese" type="text" value="<?php echo $mese; ?>" class="pop_data" size="2" maxlength="2" onFocus="if(this.value=='<?php echo $mese; ?>')this.value='';" onBlur="if(this.value=='')this.value='<?php echo $mese; ?>';"></td>
        <td width="10">&nbsp;</td>
        <td><input id="obj_anno" name="anno" type="text" value="<?php echo $anno; ?>" class="pop_data" size="4" maxlength="4" onFocus="if(this.value=='<?php echo $anno; ?>')this.value='';" onBlur="if(this.value=='')this.value='<?php echo $anno; ?>';"></td>
      </tr>
    </table>
    <div style="height:10px;"></div>
	<table border="0" cellspacing="0" cellpadding="0">
      <tr> 
<?php
if ($_GET['i'] > 0)
{
?>
        <td><img src="../bottoni/pop_modifica.gif" alt="modifica" width="160" height="40" border="0" style="cursor:pointer" onClick="data('modifica');"></td>
<?php
}
else
{
?>
        <td><img src="../bottoni/pop_aggiungi.gif" alt="aggiungi" width="160" height="40" border="0" style="cursor:pointer" onClick="data('aggiungi');"></td>
<?php
}
?>
		<td width="5">&nbsp;</td>
        <td><img src="../bottoni/pop_annulla.gif" alt="annulla" width="120" height="40" border="0" style="cursor:pointer" onClick="parent.Lightview.hide();"></td>
      </tr>
    </table>
    </form>
	</td>
    <td width="30">&nbsp;</td>
  </tr>
</table>
</body>
</html>