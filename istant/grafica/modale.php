<?php
session_start();
reset($_SESSION);
require("../../config.inc.php");
?>
<link rel="stylesheet" type="text/css" href="http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>js/uploadify/uploadify.css">
<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
<script type="text/javascript" src="http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>js/uploadify/jquery.uploadify-3.1.min.js"></script>
<script type="text/javascript">

$(function() {
    $('#file_upload').uploadify({
		'formData' : {'obj_tabella' : '<?php echo $_GET['t']; ?>', 'obj_nome' : '<?php echo $_GET['n']; ?>', 'obj_id' : '<?php echo $_GET['i']; ?>', 'obj_extra' : '<?php echo $_GET['e']; ?>', 'obj_dimensione' : '<?php echo $_GET['d']; ?>', 'obj_thumb' : '<?php echo $_GET['m']; ?>'}, 
		'swf'      : 'http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>js/uploadify/uploadify.swf',
		'uploader' : 'http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>istant/grafica/grafica.php',
		'multi'          : false,
		'auto'           : true,
		'fileTypeExt'    : '*.jpg;*.jpeg;*.gif;*.png',
		'fileTypeDesc'   : 'Image Files (.JPG,.JPEG, .GIF, .PNG)',
		'removeCompleted': false,
		'fileSizeLimit'  : '8MB',
		'onUploadSuccess' : function(file, data, response) {
//			alert(file)
//			alert(data)
//			alert(response)
			id = <?php echo $_GET['i']; ?>;
			script = '<?php echo $_GET['j']; ?>';
			obj = "grafica_<?php echo $_GET['t']; ?>_<?php echo $_GET['n']; ?>_<?php echo $_GET['i']; ?>";
			if(id > 0)
			{
				info = data.split(":");
				name = info[0];
				path = "http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>file/"+info[0];
				parent.$("#"+obj).attr('src',path);
				parent.$("#"+obj).attr('original',path);
				parent.$("#"+obj).attr('width',info[1]);
				parent.$("#"+obj).attr('height',info[2]);
				if(script != "")
				{
					eval("parent."+script+"('"+obj+"','"+name+"','"+dimensioni[0]+"','"+dimensioni[1]+"')")
				}
			}
			else
			{
				parent.document.location.reload();
			}
			parent.Lightview.hide();
		}
	});
});

function grafica(azione) {
	path = "http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>istant/grafica/"+azione+".php";
	obj_tabella=$("#obj_tabella").val();
	obj_id=$("#obj_id").val();
	obj_nome=$("#obj_nome").val();
	obj = "grafica_"+obj_tabella+"_"+obj_nome+"_"+obj_id;
	$.ajax({
		type: "POST",
		url: path,
		data: "obj_tabella="+obj_tabella+"&obj_id="+obj_id+"&obj_nome="+obj_nome,
		success: function(msg) {
			parent.$("#"+obj).attr('src','');
			parent.Lightview.hide();
		}
	});
}

</script>
<html>
<head>
<title>grafica</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<?php
		if($_GET['i'] > 0)
		{
			$query = "SELECT * FROM ".$_GET['t']." WHERE ID = '".$_GET['i']."'";
//echo $query."<br>";
			$risultato = mysql_query($query);
			$riga = mysql_fetch_array($risultato);
			$image_path = $_GET['t']."_".$_GET['n']."_".$_GET['i']."_".$riga[$_GET['n']];
			$abs_path = $_SERVER['DOCUMENT_ROOT'].$_SESSION['path']."file/".$image_path;
			$image_name = $riga[$_GET['n']];
		}
		$larghezza = "";
		list($larghezza, $altezza) = explode(":",$_GET['d']);
		$larghezza = " larghezza ".$larghezza." pixel";
		$altezza = " altezza ".$altezza." pixel";
		$dimensione = $larghezza.$altezza;
?>
<link href="../popup.css" rel="stylesheet" type="text/css">
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" bgcolor="#FFFFFF">
<table width="100%" height="70" border="0" cellpadding="0" cellspacing="0" bgcolor="#004185">
  <tr>
    <td width="30">&nbsp;</td>
    <td valign="bottom" class="pop_title">inserisci o modifica elemento grafico</td>
  </tr>
</table>
<div style="height:10px; background-color:#004185;"></div>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="30">&nbsp;</td>
    <td valign="top">
    <div style="height:10px;"></div>
<?php
if($_GET['i'] > 0)
{
?>
    <span class="pop_operazione">stai modificando:</span>
<?php
}
else
{
?>
    <span class="pop_operazione">stai aggiungendo:</span>
<?php
}
?>
    <span class="pop_breadcrumbs"><?php include("../breadcrumbs_popup.php"); ?></span>
    <div style="height:10px;"></div>
    <div style="border-bottom:1px dotted #000000;"></div>
    <span class="pop_info">aggiungere o modificare un elemento grafico. le immagini saranno tutte ridotte alla dimensione di<?php echo $dimensione; ?>.</span>
    <div style="height:10px;"></div>
    <div style="border-bottom:1px dotted #000000;"></div>
    <div style="height:10px;"></div>
	<input id="obj_extra" type="hidden" value="<?php echo $_GET['e']; ?>">
    <input id="obj_tabella" type="hidden" value="<?php echo $_GET['t']; ?>">
    <input id="obj_id" type="hidden" value="<?php echo $_GET['i']; ?>">
    <input id="obj_nome" type="hidden" value="<?php echo $_GET['n']; ?>">
    <input id="obj_dimensione" type="hidden" value="<?php echo $_GET['d']; ?>">
	<input id="obj_thumb" type="hidden" value="<?php echo $_GET['m']; ?>">
    <div style="height:10px;"></div>
<?php
if (!file_exists($abs_path))
{
?>
    <table width="100%" height="150" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="200">&nbsp;</td>
          <td width="50">&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
    </table>
<?php
}
else
{
	$dimensione = GetImageSize($abs_path);
	if($dimensione[0] > $dimensione[1])
	{
		if($dimensione[0] > 150)
		{
			$larghezza = 150;
			$altezza = intval($larghezza*$dimensione[1]/$dimensione[0]);
		}
		else
		{
			$larghezza = $dimensione[0];
			$altezza = $dimensione[1];
		}
	}
	else
	{
		if($dimensione[1] > 150)
		{
			$altezza = 150;
			$larghezza = intval($altezza*$dimensione[0]/$dimensione[1]);
		}
		else
		{
			$altezza = $dimensione[1];
			$larghezza = $dimensione[0];
		}
	}
	$peso = round(filesize($abs_path)/1024);
?>
    <table width="100%" height="150" border="0" cellspacing="0" cellpadding="0">
        <tr>
		  <td width="150" valign="top"><img src="http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>file/<?php echo $image_path; ?>" width="<?php echo $larghezza; ?>" height="<?php echo $altezza; ?>"></td>
          <td width="50">&nbsp;</td>
          <td valign="top" class="pop_info"><span class="pop_operazione">info</span><br /><?php echo $image_name; ?><br /><?php echo $dimensione[0]; ?>X<?php echo $dimensione[1]; ?>px<br /><?php echo $peso; ?> Kb</td>
        </tr>
    </table>
<?php
}
?>
    <div style="height:10px;"></div>
    <div style="border-bottom:1px dotted #000000;"></div>
    <div style="height:10px;"></div>
	<input type="file" name="file_upload" id="file_upload" />
    <div style="height:50px;"></div>
    <table border="0" cellspacing="0" cellpadding="0">
       <tr> 
<?php
if (file_exists($abs_path))
{
?>
          <td><img src="../bottoni/pop_cancella.gif" alt="cancella" width="120" height="40" border="0" style="cursor:pointer" onClick="grafica('cancella');"></td>
<?php
}
?>
          <td width="5">&nbsp;</td>
          <td><img src="../bottoni/pop_annulla.gif" alt="annulla" width="120" height="40" border="0" style="cursor:pointer" onClick="parent.Lightview.hide();"></td>
        </tr>
    </table>
	</td>
    <td width="30">&nbsp;</td>
  </tr>
</table>
</body>
</html>

