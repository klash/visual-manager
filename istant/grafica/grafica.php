<?php
include("../../config.inc.php");
require("../../front.inc.php");
$directory = $_SERVER['DOCUMENT_ROOT'].$_SESSION['path']."file/";
$path = $_SERVER['SERVER_NAME'].$_SESSION['path']."file/";
?>
<?php
if ($_FILES["Filedata"]["name"] != "")
{
	if($_POST['obj_id'] > 0)
	{
		list($obj['larghezza'], $obj['altezza']) = explode(":",$_POST['obj_dimensione']);
		$nome_file = explode(".",$_FILES["Filedata"]["name"]);
		$ext = ".".array_pop($nome_file);
		$nome_file = implode(".",$nome_file);
		$obj_grafica_name = toAscii($nome_file, array('*','`',"'",'"','“','”')).$ext;
//		$obj_grafica_name = filter_var($nome_file, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
//		$obj_grafica_name = str_replace(" ","_",$obj_grafica_name).$ext;
//		$obj_grafica_name = sanitize_title_with_dashes($nome_file);
		$query_path = "SELECT * FROM ".$_POST['obj_tabella']." WHERE ID = '".$_POST['obj_id']."'";
//echo $obj_grafica_name."\n";
		$risultato_path = mysql_query($query_path);
		$riga_path = mysql_fetch_array($risultato_path);
		if(is_dir($directory))
		{
			$old_path = $_POST['obj_tabella']."_".$_POST['obj_nome']."_".$_POST['obj_id']."_".$riga_path[$_POST['obj_nome']];
			if (file_exists($directory.$old_path))
			{
				unlink($directory.$old_path);
			}
			$old_thumb = "thumb_".$_POST['obj_tabella']."_".$_POST['obj_nome']."_".$_POST['obj_id']."_".$riga_path[$_POST['obj_nome']];
			if (file_exists($directory.$old_thumb))
			{
				unlink($directory.$old_thumb);
			}
			$new_path = $_POST['obj_tabella']."_".$_POST['obj_nome']."_".$_POST['obj_id']."_".$obj_grafica_name;
			@copy($_FILES["Filedata"]["tmp_name"] ,$directory.$new_path);
			@chmod($directory.$new_path, 0755);
			$query_modifica = "UPDATE ".$_POST['obj_tabella']." SET ".$_POST['obj_nome']." = '".$obj_grafica_name."' WHERE ID = '".$_POST['obj_id']."'";
			$risultato = mysql_query($query_modifica);
//echo $directory.$new_path."\n";
			while (!file_exists($directory.$new_path))
			{
				echo "";
			}
			resize($directory, $new_path, $obj['larghezza'], $obj['altezza']);
			if($_POST['obj_thumb'] != "")
			{
				$thumb_path = "thumb_".$_POST['obj_tabella']."_".$_POST['obj_nome']."_".$_POST['obj_id']."_".$obj_grafica_name;
				@copy($directory.$new_path ,$directory.$thumb_path);
				@chmod($directory.$thumb_path, 0755);
				list($thumb['larghezza'], $thumb['altezza']) = explode(":",$_POST['obj_thumb']);
				resize($directory, $thumb_path, $thumb['larghezza'], $thumb['altezza']);
			}
			//echo $query_modifica."<br>";
			if($_POST['obj_thumb'] != "")
			{
				list($width, $height) = getimagesize($directory.$thumb_path);
				echo $thumb_path.":".$width.":".$height;
			}
			else
			{
				list($width, $height) = getimagesize($directory.$new_path);
				echo $new_path.":".$width.":".$height;
			}
			$query_tabella = "SELECT * FROM tabella WHERE nome = '".$_POST['obj_tabella']."'";
			$risultato_tabella = mysql_query($query_tabella);
			$riga_tabella = mysql_fetch_array($risultato_tabella);
			$query_log = "INSERT INTO log (`redattore` ,`azione` ,`tabella` ,`key` ,`campo` ,`data`) VALUES ('".$_SESSION['id_utente']."', 'modifica', '".$riga_tabella['ID']."', '".$_POST['obj_id']."', '".$_POST['obj_nome']."', CURRENT_TIMESTAMP)";
			$risultato_log = mysql_query($query_log);
		}
	}
}
?>

<?php
function resize($folder,$nome_file,$w,$h)
{
	$fil=$sorge;
	$ext=strrchr($nome_file,".");
	list($width, $height) = getimagesize($folder.$nome_file);
	$ratio = floatval(@(($w/$h)));
	switch ($ratio)
	{
		case 0:
			if($w == 0)
			{
				$newheight = $h;
				$newwidth = $h*$width/$height;
			}
			if($h == 0)
			{
				$newwidth = $w;
				$newheight = $w*$height/$width;
			}
			break;
		case ($ratio < 1):
			$newheight = $h;
			$newwidth = $h*$width/$height;
			if ($newwidth < $w)
			{
				$newwidth = $w;
				$newheight = $w*$height/$width;
			}
			break;
		default:
			$newwidth = $w;
			$newheight = $w*$height/$width;
			if ($newheight < $h)
			{
				$newheight = $h;
				$newwidth = $h*$width/$height;
			}
			break;
	}
	$thumb = imagecreatetruecolor($newwidth,$newheight);
	if($ext==".jpg" || $ext==".jpeg" || $ext==".JPG" || $ext==".JPEG"){
		$source = imagecreatefromjpeg($folder.$nome_file);
		imagecopyresampled($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
		imagejpeg($thumb,$folder.$nome_file,100);
	}
	if($ext==".png" || $ext==".PNG"){
		$source = imagecreatefrompng($folder.$nome_file);
		imagecopyresampled($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
		imagepng($thumb,$folder.$nome_file,0);
	}
	if($ext==".gif" || $ext==".GIF"){
		$source = imagecreatefromgif($folder.$nome_file);
		imagecopyresampled($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
		imagegif($thumb,$folder.$nome_file);
	}
}
?>