<?php
session_start();
reset($_SESSION);
include("../../config.inc.php");
require("../../front.inc.php");
$directory = $_SERVER['DOCUMENT_ROOT'].$_SESSION['path']."file/";
$path = "http://".$_SERVER['SERVER_NAME'].$_SESSION['path']."file/";
list($obj_width, $obj_height) = explode(":",$_POST['obj_dimensione']);
?>
<?php
if ($_FILES["Filedata"]["name"] != "")
{
	list($width, $height) = getimagesize($_FILES["Filedata"]["tmp_name"]);
	$image_dimensione = $width*$height;
	list($obj_min_width, $obj_min_height) = explode(":",$_POST['obj_minimum']);
	$obj_minimum = $obj_min_width*$obj_min_height;
	if($width >= $obj_min_width || $height >=$obj_min_height)
	{
		if($_POST['obj_posizione'] > 0)
		{
			$query_obj = "SELECT * FROM ".$_POST['obj_tabella']."_img WHERE ".$_POST['obj_tabella']." = '".$_POST['obj_id']."' AND posizione = '".$_POST['obj_posizione']."'";
//echo $query_path."<br>";
			$risultato_obj = mysql_query($query_obj);
			$riga_obj = mysql_fetch_array($risultato_obj);
			$nome_file = explode(".",$_FILES["Filedata"]["name"]);
			$ext = ".".array_pop($nome_file);
			$nome_file = implode(".",$nome_file);
			$file_immagine_name = toAscii($nome_file, array('*','`',"'",'"','“','”')).$ext;
//			$file_immagine_name = filter_var($nome_file, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
//			$file_immagine_name = str_replace(" ","_",$file_immagine_name).$ext;
//			$file_immagine_name = sanitize_title_with_dashes($nome_file).$ext;
			if($riga_tabella['lingua'] == 'si')
			{
				$obj = $_POST['obj_tabella']."_".$riga_obj['primario']."_".$_POST['obj_id'];
				$nome_old = $_POST['obj_tabella']."_".$riga_obj['primario']."_".$_POST['obj_id']."_".$riga_obj[$_POST['obj_nome']];
				$nome_new = $_POST['obj_tabella']."_".$riga_obj['primario']."_".$_POST['obj_id']."_".$file_immagine_name;
			}
			else
			{
				$obj = $_POST['obj_tabella']."_".$riga_obj['ID']."_".$_POST['obj_id'];
				$nome_old = $_POST['obj_tabella']."_".$riga_obj['ID']."_".$_POST['obj_id']."_".$riga_obj[$_POST['obj_nome']];
				$nome_new = $_POST['obj_tabella']."_".$riga_obj['ID']."_".$_POST['obj_id']."_".$file_immagine_name;
			}
			if (file_exists($directory.$nome_old))
			{
				unlink($directory.$nome_old);
			}
			if (file_exists($directory."thumb_".$nome_old))
			{
				unlink($directory."thumb_".$nome_old);
			}
			move_uploaded_file($_FILES["Filedata"]["tmp_name"],$directory.$nome_new);
			while (!file_exists($directory.$nome_new))
			{
				echo "";
			}
			@chmod($directory.$nome_new, 0777);
			resize($directory,$nome_new,$obj_width,$obj_height);
			$query_modifica = "UPDATE ".$_POST['obj_tabella']."_img SET ".$_POST['obj_nome']." = '".$file_immagine_name."' WHERE ".$_POST['obj_tabella']." = '".$_POST['obj_id']."' AND posizione = '".$_POST['obj_posizione']."'";
//echo $query_modifica."<br>";
			$risultato = mysql_query($query_modifica);
			$query_tabella = "SELECT * FROM tabella WHERE nome = '".$_POST['obj_tabella']."_img'";
//print $query_tabella."<br>";
			$risultato_tabella = mysql_query($query_tabella);
			$riga_tabella = mysql_fetch_array($risultato_tabella);
			if($riga_tabella['lingua'] == 'si')
			{
				$query_modifica = "UPDATE ".$_POST['obj_tabella']."_img SET didascalia = '".$_POST['testo']."' WHERE ".$_POST['obj_tabella']." = '".$_POST['obj_id']."' AND primario = '".$riga_obj['primario']."' AND lingua = '".$_POST['obj_flag']."'";
			}
			else
			{
				$query_modifica = "UPDATE ".$_POST['obj_tabella']."_img SET didascalia = '".$_POST['testo']."' WHERE ".$_POST['obj_tabella']." = '".$_POST['obj_id']."' AND posizione = '".$_POST['obj_posizione']."'";
			}
//echo $query_modifica."<br>";
			$risultato = mysql_query($query_modifica);
			$query_log = "INSERT INTO log (`redattore` ,`azione` ,`tabella` ,`key` ,`campo` ,`data`) VALUES ('".$_SESSION['id_utente']."', 'modifica', '".$riga_tabella['ID']."', '".$_POST['obj_id']."', '".$_POST['obj_nome']."', CURRENT_TIMESTAMP)";
			$risultato_log = mysql_query($query_log);
			echo $obj;
		}
		else
		{
			$nome_file = explode(".",$_FILES["Filedata"]["name"]);
			$ext = ".".array_pop($nome_file);
			$nome_file = implode(".",$nome_file);
			$file_immagine_name = toAscii($nome_file, array('*','`',"'",'"','“','”')).$ext;
//			$file_immagine_name = filter_var($nome_file, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
//			$file_immagine_name = str_replace(" ","_",$file_immagine_name).$ext;
//			$file_immagine_name = sanitize_title_with_dashes($nome_file).$ext;
			$chiave = array();
			$valore = array();
			$indice = array();
	//print $_POST['obj_extra']."<br>";
			if (trim($_POST['obj_extra']) != "")
			{
				$campi_fissi = explode("@",$_POST['obj_extra']);
				reset($campi_fissi);
				while (list($extra_chiave, $extra_valore) = each($campi_fissi))
				{
					$campi_singoli = explode("|",$extra_valore);
					$elenco_campi[$campi_singoli[0]] = $campi_singoli[1];
					array_push($chiave, $campi_singoli[0]);
					array_push($valore, $campi_singoli[1]);
				}
			}
			array_push($chiave, $_POST['obj_nome']);
			array_push($valore, $file_immagine_name);
			$query_tabella = "SELECT * FROM tabella WHERE nome = '".$_POST['obj_tabella']."'";
			$risultato_tabella = mysql_query($query_tabella);
			$riga_tabella = mysql_fetch_array($risultato_tabella);
			if($riga_tabella['lingua'] == 'si')
			{
				$query_posizione = "SELECT * FROM ".$_POST['obj_tabella']."_img WHERE ".$_POST['obj_tabella']." = '".$_POST['obj_id']."' AND primario = ID";
				$risultato_posizione = mysql_query($query_posizione);
				$trovato_posizione = mysql_num_rows($risultato_posizione);
				$posizione = $trovato_posizione+1;
				$query_lingua = "SELECT * FROM lingua WHERE attivo = 'si' ORDER BY posizione";
				$risultato_lingua = mysql_query($query_lingua);
				while ($riga_lingua = mysql_fetch_array($risultato_lingua))
				{
					$chiave['lingua'] = "lingua";
					$valore['lingua'] = $riga_lingua['ID'];
					$chiave['primario'] = "primario";
					$valore['primario'] = 0;
					$chiave['posizione'] = "posizione";
					$valore['posizione'] = 0;
					$query_aggiungi = "INSERT INTO ".$_POST['obj_tabella']."_img ( ".implode(",", $chiave)." ) VALUES ( '".implode("','", $valore)."' )";
//print $query_aggiungi."<br>";
					$risultato_aggiungi = mysql_query($query_aggiungi);
					$ultimo = mysql_insert_id($db_link);
					array_push($indice, $ultimo);
					if($riga_lingua['primario'] == 'si')
					{
						$default = $ultimo;
					}
				}
				reset($indice);
				while (list($key, $val) = each($indice))
				{
					$query_modifica = "UPDATE ".$_POST['obj_tabella']."_img SET primario = '".$default."' WHERE ID = '".$val."'";
	//print $query_modifica."<br>";
					$risultato_modifica = mysql_query($query_modifica);
					if($val == $default)
					{
						$query_modifica = "UPDATE ".$_POST['obj_tabella']."_img SET posizione = '".$posizione."' WHERE ID = '".$val."'";
	//print $query_modifica."<br>";
						$risultato_modifica = mysql_query($query_modifica);
					}
					else
					{
						$query_modifica = "UPDATE ".$_POST['obj_tabella']."_img SET posizione = '0' WHERE ID = '".$val."'";
	//print $query_modifica."<br>";
						$risultato_modifica = mysql_query($query_modifica);
						$query_modifica = "UPDATE ".$_POST['obj_tabella']."_img SET image = NULL WHERE ID = '".$val."'";
	//print $query_modifica."<br>";
						$risultato_modifica = mysql_query($query_modifica);
					}
				}
			}
			else
			{
				$query_posizione = "SELECT * FROM ".$_POST['obj_tabella']."_img WHERE ".$_POST['obj_tabella']." = '".$_POST['obj_id']."'";
				$risultato_posizione = mysql_query($query_posizione);
				$trovato_posizione = mysql_num_rows($risultato_posizione);
				array_push($chiave, "posizione");
				array_push($valore, ($trovato_posizione+1));
				$query_aggiungi = "INSERT INTO ".$_POST['obj_tabella']."_img ( ".implode(",", $chiave)." ) VALUES ( '".implode("','", $valore)."' )";
	//print $query_aggiungi."<br>";
				$risultato_aggiungi = mysql_query($query_aggiungi);
				$default = mysql_insert_id($db_link);
			}
			$nome_file = $_POST['obj_tabella']."_".$default."_".$_POST['obj_id']."_".$file_immagine_name;
			move_uploaded_file($_FILES["Filedata"]["tmp_name"] ,$directory.$nome_file);
			while (!file_exists($directory.$nome_file))
			{
				echo "";
			}
			@chmod($directory.$nome_file, 0777);
			resize($directory,$nome_file,$obj_width,$obj_height);
		}
		$query_log = "INSERT INTO log (`redattore` ,`azione` ,`tabella` ,`key` ,`campo` ,`data`) VALUES ('".$_SESSION['id_utente']."', 'aggiungi', '".$riga_tabella['ID']."', '".$ultimo."', '".$_POST['obj_nome']."', CURRENT_TIMESTAMP)";
		$risultato_log = mysql_query($query_log);
	}
	else
	{
		echo "errore";
	}
}

function resize($folder,$nome_file,$w,$h)
{
	$fil=$sorge;
	$ext=strrchr($nome_file,".");
	list($width, $height) = getimagesize($folder.$nome_file);
	$ratio = floatval(@(($w/$h)));
	switch ($ratio)
	{
		case 0:
			if($w == 0)
			{
				$newheight = $h;
				$newwidth = $h*$width/$height;
			}
			if($h == 0)
			{
				$newwidth = $w;
				$newheight = $w*$height/$width;
			}
			break;
		case ($ratio < 1):
			$newheight = $h;
			$newwidth = $h*$width/$height;
			if ($newwidth < $w)
			{
				$newwidth = $w;
				$newheight = $w*$height/$width;
			}
			break;
		default:
			$newwidth = $w;
			$newheight = $w*$height/$width;
			if ($newheight < $h)
			{
				$newheight = $h;
				$newwidth = $h*$width/$height;
			}
			break;
	}
	$thumb = imagecreatetruecolor($newwidth,$newheight);
	if($ext==".jpg" || $ext==".jpeg" || $ext==".JPG" || $ext==".JPEG"){
		$source = imagecreatefromjpeg($folder.$nome_file);
		imagecopyresampled($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
		imagejpeg($thumb,$folder."thumb_".$nome_file,100);
	}
	if($ext==".png" || $ext==".PNG"){
		$source = imagecreatefrompng($folder.$nome_file);
		imagecopyresampled($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
		imagepng($thumb,$folder."thumb_".$nome_file,0);
	}
	if($ext==".gif" || $ext==".GIF"){
		$source = imagecreatefromgif($folder.$nome_file);
		imagecopyresampled($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
		imagegif($thumb,$folder."thumb_".$nome_file);
	}
}
?>