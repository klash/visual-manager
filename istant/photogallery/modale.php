<?php
session_start();
reset($_SESSION);
require("../../config.inc.php");
$location = "http://".$_SERVER['SERVER_NAME'].$_SESSION['path'];
?>
<html>
<head>
<title>gallery</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>js/uploadify/uploadify.css">
<link rel="stylesheet" type="text/css" href="http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>istant/popup.css">
<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
<script type="text/javascript" src="http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>js/uploadify/jquery.uploadify-3.1.min.js"></script>
<script type="text/javascript">
<?php
if($_GET['k'] > 0)
{
	$multi = "false";
}
else
{
	$multi = "true";
}
?>
$(function() {
    $('#file_upload').uploadify({
		'formData' : {'obj_tabella' : '<?php echo $_GET['t']; ?>', 'obj_nome' : '<?php echo $_GET['n']; ?>', 'obj_id' : '<?php echo $_GET['i']; ?>', 'obj_image' : '<?php echo $_GET['o']; ?>', 'obj_posizione' : '<?php echo $_GET['k']; ?>', 'obj_dimensione' : '<?php echo $_GET['d']; ?>', 'obj_minimum' : '<?php echo $_GET['m']; ?>', 'obj_extra' : '<?php echo $_GET['e']; ?>'}, 
		'swf'      : 'http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>js/uploadify/uploadify.swf',
		'uploader' : 'http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>/istant/photogallery/gallery.php',
		'multi'          : <?php echo $multi; ?>,
		'auto'           : true,
		'fileTypeExt'    : '*.jpg;*.jpeg;*.gif;*.png',
		'fileTypeDesc'   : 'Image Files (.JPG,.JPEG, .GIF, .PNG)',
		'removeCompleted': false,
		'fileSizeLimit'  : '8MB',
		'onUploadSuccess' : function(file, data, response) {
			id = parseInt('<?php echo $_GET['k']; ?>');
			script = '<?php echo $_GET['j']; ?>';
			if(data == 'errore')
			{
				alert("immagine troppo piccola");
			}
			else
			{
				if(id > 0)
				{
					obj = "gallery_"+data;
					obj_image = "thumb_"+obj;
					name = data+"_"+file.name;
					path_thumb = "http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>file/thumb_"+name;
					parent.$('#'+obj_image).attr('src',path_thumb);
					if(script != "")
					{
						path_zoom = "http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>file/"+name;
						eval("parent."+script+"('"+obj+"','"+path_zoom+"')")
					}
				}
			}
		},
        'onQueueComplete' : function(queueData) {
			id = parseInt('<?php echo $_GET['k']; ?>');
//			if(isNaN(id))
//			{
//				parent.document.location.reload();
//			}
			parent.Lightview.hide();
			parent.document.location.reload();
        } 
	});
});

function gallery(azione) {
	path = "http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>istant/photogallery/"+azione+".php";
	obj_tabella=$("#obj_tabella").val();
	obj_id=$("#obj_id").val();
	obj_nome=$("#obj_nome").val();
	obj_posizione=$("#obj_posizione").val();
	obj_image=$("#obj_image").val();
	obj_varchar=$("#obj_varchar").val();
	obj = "photogallery_"+obj_tabella+"_"+obj_nome+"_"+obj_id+"_"+obj_image;
	$.ajax({
		type: "POST",
		url: path,
		data: "obj_tabella="+obj_tabella+"&obj_id="+obj_id+"&obj_nome="+obj_nome+"&obj_posizione="+obj_posizione+"&obj_image="+obj_image+"&obj_varchar="+obj_varchar,
		success: function(msg) {
			if(azione == "modifica")
			{
				parent.$('#thumb_'+obj).attr("alt",msg);
				parent.$('#thumb_'+obj).attr("title",msg);
			}
			else
			{
				parent.$('#'+obj).remove();
			}
			parent.Lightview.hide();
		}
	});
}

</script>
</head>
<?php
$query_tabella = "SELECT * FROM tabella WHERE nome = '".$_GET['t']."'";
$risultato_tabella = mysql_query($query_tabella);
$riga_tabella = mysql_fetch_array($risultato_tabella);
if($_GET['i'] > 0)
{
	$directory = $_SERVER['DOCUMENT_ROOT'].$_SESSION['path']."file/";
	$path = "http://".$_SERVER['SERVER_NAME'].$_SESSION['path']."file/";
	$query_obj = "SELECT * FROM ".$_GET['t']." WHERE ID = '".$_GET['i']."'";
	$risultato_obj = mysql_query($query_obj);
	$riga_obj = mysql_fetch_array($risultato_obj);
	$breadcrumbs = $riga_tabella['titolo']." / ".$riga_obj[$riga_tabella['vif']]." / photo gallery";
	$query = "SELECT * FROM ".$_GET['t']."_img WHERE ID = '".$_GET['o']."'";
//echo $query."<br>";
	$risultato = mysql_query($query);
	$riga = mysql_fetch_array($risultato);
	$image_path = $_GET['t']."_".$riga['ID']."_".$_GET['i']."_".$riga[$_GET['n']];
	if($riga_tabella['lingua'] == 'si')
	{
		$query = "SELECT * FROM ".$_GET['t']."_img WHERE primario = '".$riga['primario']."' AND lingua = '".$_SESSION['language']['id']."'";
//echo $query;
		$risultato = mysql_query($query);
		$riga = mysql_fetch_array($risultato);
		$testo = $riga['didascalia'];
	}
	else
	{
		$testo = $riga['didascalia'];
	}
//echo $query."<br>";
	$image_name = $testo;
	if(trim($testo) == "")
	{
		$testo = "didascalia";
	}
}
?>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" bgcolor="#FFFFFF">
<table width="100%" height="70" border="0" cellpadding="0" cellspacing="0" bgcolor="#004185">
  <tr>
    <td width="30">&nbsp;</td>
<?php
if($riga_tabella['lingua'] == 'si')
{
?>
    <td valign="bottom" class="pop_title"><?php echo $_SESSION['language']['path']; ?> / inserisci o modifica elemento grafico</td>
<?php
}
else
{
?>
    <td valign="bottom" class="pop_title">inserisci o modifica elemento grafico</td>
<?php
}
?>
  </tr>
</table>
<div style="height:10px; background-color:#004185;"></div>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="30">&nbsp;</td>
    <td valign="top">
    <div style="height:10px;"></div>
<?php
if($_GET['i'] > 0)
{
?>
    <span class="pop_operazione">stai modificando:</span>
<?php
}
else
{
?>
    <span class="pop_operazione">stai aggiungendo:</span>
<?php
}
?>
    <span class="pop_breadcrumbs"><?php echo $breadcrumbs; ?></span>
    <div style="height:10px;"></div>
    <div style="border-bottom:1px dotted #000000;"></div>
    <span class="pop_info">aggiungere o modificare un elemento grafico. le miniature saranno tutte ridotte alla dimensione di <?php echo $_GET['d']; ?> pixel. non verranno accettate immagini più piccole di <?php echo $_GET['m']; ?> pixel</span>
    <div style="height:10px;"></div>
    <div style="border-bottom:1px dotted #000000;"></div>
    <div style="height:10px;"></div>
    <input id="obj_tabella" type="hidden" value="<?php echo $_GET['t']; ?>">
    <input id="obj_id" type="hidden" value="<?php echo $_GET['i']; ?>">
    <input id="obj_nome" type="hidden" value="<?php echo $_GET['n']; ?>">
	<input id="obj_posizione" type="hidden" value="<?php echo $_GET['k']; ?>">
	<input id="obj_image" type="hidden" value="<?php echo $_GET['o']; ?>">
    <div style="height:10px;"></div>
<?php
if (!file_exists($directory.$image_path))
{
	$larghezza = 150;
	$altezza = 150;
	echo $background."<br>";
?>
  <table width="100%" height="140" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="200">&nbsp;</td>
      <td width="50">&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
  </table>
<?php
}
else
{
	$dimensione = GetImageSize($directory.$image_path);
	if($dimensione[0] > $dimensione[1])
	{
		if($dimensione[0] > 150)
		{
			$larghezza = 150;
			$altezza = intval($larghezza*$dimensione[1]/$dimensione[0]);
		}
		else
		{
			$larghezza = $dimensione[0];
			$altezza = $dimensione[1];
		}
	}
	else
	{
		if($dimensione[1] > 150)
		{
			$altezza = 150;
			$larghezza = intval($altezza*$dimensione[0]/$dimensione[1]);
		}
		else
		{
			$altezza = $dimensione[1];
			$larghezza = $dimensione[0];
		}
	}
	$peso = round(filesize($directory.$image_path)/1024);
?>
  <table width="100%" height="140" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="150" valign="top"><img src="<?php echo $path.$image_path; ?>" width="<?php echo $larghezza; ?>" height="<?php echo $altezza; ?>"></td>
      <td width="50">&nbsp;</td>
      <td valign="top" class="pop_info"><span class="pop_operazione">info</span><br /><?php echo $image_name; ?><br /><?php echo $dimensione[0]; ?>X<?php echo $dimensione[1]; ?>px<br /><?php echo $peso; ?> Kb</td>
    </tr>
  </table>
<?php
}
?>
<?php
if($_GET['k'] > 0)
{
?>
    <div style="height:10px;"></div>
    <input id="obj_varchar" type="text" value="<?php echo $testo; ?>" class="pop_varchar">
<?php
}
?>
    <div style="height:10px;"></div>
    <div style="border-bottom:1px dotted #000000;"></div>
    <div style="height:10px;"></div>
	<input type="file" name="file_upload" id="file_upload" />
    <div style="height:10px;"></div>
    <table border="0" cellspacing="0" cellpadding="0">
      <tr> 
<?php
if($_GET['k'] > 0)
{
?>
      <td><img src="<?php echo $location; ?>istant/bottoni/pop_modifica.gif" alt="cancella" width="160" height="40" border="0" style="cursor:pointer" onClick="gallery('modifica');"></td>
      <td width="5">&nbsp;</td>
      <td><img src="<?php echo $location; ?>istant/bottoni/pop_cancella.gif" alt="cancella" width="120" height="40" border="0" style="cursor:pointer" onClick="gallery('cancella');"></td>
      <td width="5">&nbsp;</td>
<?php
}
?>
      <td><img src="<?php echo $location; ?>istant/bottoni/pop_annulla.gif" alt="annulla" width="120" height="40" border="0" style="cursor:pointer" onClick="parent.Lightview.hide();"></td>
      </tr>
    </table>
	</td>
    <td width="30">&nbsp;</td>
  </tr>
</table>
</body>
</html>