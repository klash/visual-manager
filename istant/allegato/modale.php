<?php
session_start();
reset($_SESSION);
require("../../config.inc.php");
?>
<link rel="stylesheet" type="text/css" href="http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>js/uploadify/uploadify.css">
<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
<script type="text/javascript" src="http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>js/uploadify/jquery.uploadify-3.1.min.js"></script>
<script type="text/javascript">

$(function() {
    $('#file_upload').uploadify({
		'formData' : {'obj_tabella' : '<?php echo $_GET['t']; ?>', 'obj_nome' : '<?php echo $_GET['n']; ?>', 'obj_id' : '<?php echo $_GET['i']; ?>'}, 
		'swf'      : 'http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>js/uploadify/uploadify.swf',
		'uploader' : 'http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>istant/allegato/allegato.php',
		'multi'          : false,
		'auto'           : true,
		'fileTypeExt'    : '*.zip;*.pdf',
		'fileTypeDesc'   : 'Image Files (.ZIP,.PDF)',
		'removeCompleted': false,
		'fileSizeLimit'  : '8MB',
		'onUploadSuccess' : function(file, data, response) {
//			alert(file)
//			alert(data)
//			alert(response)
			parent.document.location.reload();
			parent.Lightview.hide();
		}
	});
});

function allegato(azione) {
	path = "http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>istant/allegato/"+azione+".php";
	obj_tabella=$("#obj_tabella").val();
	obj_id=$("#obj_id").val();
	obj_nome=$("#obj_nome").val();
	obj = "grafica_"+obj_tabella+"_"+obj_nome+"_"+obj_id;
	$.ajax({
		type: "POST",
		url: path,
		data: "obj_tabella="+obj_tabella+"&obj_id="+obj_id+"&obj_nome="+obj_nome,
		success: function(msg) {
			parent.Lightview.hide();
		}
	});
}

</script>
<html>
<head>
<title>allegato</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<?php
		if($_GET['i'] > 0)
		{
			$query = "SELECT * FROM ".$_GET['t']." WHERE ID = '".$_GET['i']."'";
//echo $query."<br>";
			$risultato = mysql_query($query);
			$riga = mysql_fetch_array($risultato);
			$allegato_path = $_GET['t']."_".$_GET['n']."_".$_GET['i']."_".$riga[$_GET['n']];
			$abs_path = $_SERVER['DOCUMENT_ROOT'].$_SESSION['path']."allegato/".$allegato_path;
			$image_name = $riga[$_GET['n']];
		}
?>
<link href="../popup.css" rel="stylesheet" type="text/css">
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" bgcolor="#FFFFFF">
<table width="100%" height="70" border="0" cellpadding="0" cellspacing="0" bgcolor="#004185">
  <tr>
    <td width="30">&nbsp;</td>
    <td valign="bottom" class="pop_title">inserisci o modifica allegato</td>
  </tr>
</table>
<div style="height:10px; background-color:#004185;"></div>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="30">&nbsp;</td>
    <td valign="top">
    <div style="height:10px;"></div>
<?php
if($_GET['i'] > 0)
{
?>
    <span class="pop_operazione">stai modificando:</span>
<?php
}
else
{
?>
    <span class="pop_operazione">stai aggiungendo:</span>
<?php
}
?>
    <span class="pop_breadcrumbs"><?php include("../breadcrumbs_popup.php"); ?></span>
    <div style="height:10px;"></div>
    <div style="border-bottom:1px dotted #000000;"></div>
    <span class="pop_info">aggiungere o modificare un allegato.</span>
    <div style="height:10px;"></div>
    <div style="border-bottom:1px dotted #000000;"></div>
    <div style="height:10px;"></div>
    <input id="obj_tabella" type="hidden" value="<?php echo $_GET['t']; ?>">
    <input id="obj_id" type="hidden" value="<?php echo $_GET['i']; ?>">
    <input id="obj_nome" type="hidden" value="<?php echo $_GET['n']; ?>">
    <div style="height:10px;"></div>
<?php
if (!file_exists($abs_path))
{
?>
    <table width="100%" height="150" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td>&nbsp;</td>
        </tr>
    </table>
<?php
}
else
{
	$peso = round(filesize($abs_path)/1024);
?>
    <table width="100%" height="150" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td valign="top" class="pop_info"><span class="pop_operazione">info</span><br /><?php echo $image_name; ?><br /><?php echo $peso; ?> Kb</td>
        </tr>
    </table>
<?php
}
?>
    <div style="height:10px;"></div>
    <div style="border-bottom:1px dotted #000000;"></div>
    <div style="height:10px;"></div>
	<input type="file" name="file_upload" id="file_upload" />
    <div style="height:50px;"></div>
    <table border="0" cellspacing="0" cellpadding="0">
       <tr> 
<?php
if (file_exists($abs_path))
{
?>
          <td><img src="../bottoni/pop_cancella.gif" alt="cancella" width="120" height="40" border="0" style="cursor:pointer" onClick="allegato('cancella');"></td>
<?php
}
?>
          <td width="5">&nbsp;</td>
          <td><img src="../bottoni/pop_annulla.gif" alt="annulla" width="120" height="40" border="0" style="cursor:pointer" onClick="parent.Lightview.hide();"></td>
        </tr>
    </table>
	</td>
    <td width="30">&nbsp;</td>
  </tr>
</table>
</body>
</html>

