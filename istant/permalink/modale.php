<?php
require('../../object.inc.php');
session_start();
reset($_SESSION);
require("../../config.inc.php");
?>
<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
<script type="text/javascript">
function permalink(azione){
	var data = $("#frm_permalink").serialize()
	path = "http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>istant/permalink/"+azione+".php";
	obj_extra=$("#obj_extra").val();
	obj_tabella=$("#obj_tabella").val();
	obj_id=$("#obj_id").val();
	obj_nome=$("#obj_nome").val();
	obj_hyperlink=$("#obj_hyperlink").val();
	obj_varchar=$("#obj_varchar").val();
	obj_permalink=$("#obj_permalink").val();
	obj = "permalink_"+obj_tabella+"_"+obj_nome+"_"+obj_id;
	$.ajax({
		type: "POST",
		url: path,
		data: data,
		success: function(msg) {
//			alert(msg);
			var listArray = msg.split("|#|");
			txt = listArray[0];
			perma = listArray[1];
			href = obj_hyperlink+perma+".html";
			if(azione == 'modifica')
			{
				parent.$("#"+obj).html(txt);
				parent.$("#"+obj).attr("href",href);
				if($(parent.$("#div_"+obj)).length > 0)
				{
					parent.$("#div_"+obj).attr("href",href);
				}
			}
			else
			{
				parent.document.location.reload();
			}
			parent.Lightview.hide();
		}
	});
}
</script>
<html>
<head>
<title>permalink</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<?php
$trovato = 0;
$query_tabella = "SELECT * FROM tabella WHERE nome = '".$_GET['t']."'";
$risultato_tabella = mysql_query($query_tabella);
$riga_tabella = mysql_fetch_array($risultato_tabella);
if($_GET['i'] > 0)
{
	if($riga_tabella['lingua'] == 'si')
	{
		$query = "SELECT * FROM ".$_GET['t']." WHERE primario = '".$_GET['i']."' AND lingua = '".$_SESSION['language']['id']."'";
		$risultato = mysql_query($query);
		$trovato = mysql_num_rows($risultato);
		if($trovato > 0)
		{
			$riga = mysql_fetch_array($risultato);
		}
		else
		{
			$query = "SELECT * FROM ".$_GET['t']." WHERE ID = '".$_GET['i']."'";
			$risultato = mysql_query($query);
			$riga = mysql_fetch_array($risultato);
		}
	}
	else
	{
		$query = "SELECT * FROM ".$_GET['t']." WHERE ID = '".$_GET['i']."'";
		$risultato = mysql_query($query);
		$trovato = mysql_num_rows($risultato);
		$riga = mysql_fetch_array($risultato);
	}
	$testo = stripslashes($riga[$_GET['n']]);
	$permalink = stripslashes($riga['permalink']);
}
else
{
	$testo = $_GET['n'];
}
?>
<link href="../popup.css" rel="stylesheet" type="text/css">
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" bgcolor="#FFFFFF">
<table width="100%" height="70" border="0" cellpadding="0" cellspacing="0" bgcolor="#004185">
  <tr>
    <td width="30">&nbsp;</td>
<?php
if($riga_tabella['lingua'] == 'si')
{
?>
    <td valign="bottom" class="pop_title"><?php echo $_SESSION['language']['path']; ?> / inserisci o modifica permalink</td>
<?php
}
else
{
?>
    <td valign="bottom" class="pop_title">inserisci o modifica permalink</td>
<?php
}
?>
  </tr>
</table>
<div style="height:10px; background-color:#004185;"></div>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="30">&nbsp;</td>
    <td valign="top">
    <div style="height:10px;"></div>
<?php
if($trovato > 0)
{
?>
    <span class="pop_operazione">stai modificando:</span>
<?php
}
else
{
?>
    <span class="pop_operazione">stai aggiungendo:</span>
<?php
}
?>
    <span class="pop_breadcrumbs"><?php include("../breadcrumbs_popup.php"); ?></span>
    <div style="height:10px;"></div>
    <div style="border-bottom:1px dotted #000000;"></div>
    <span class="pop_info">inserisci o modifica il testo nel campo sottostante.<br>poi fai click su modifica/aggiungi per aggiornate.</span>
    <div style="height:10px;"></div>
    <div style="border-bottom:1px dotted #000000;"></div>
    <div style="height:10px;"></div>
    <form method="post" id="frm_permalink" name="frm_permalink">
	<input id="obj_extra" name="obj_extra" type="hidden" value="<?php echo $_GET['e']; ?>">
	<input id="obj_tabella" name="obj_tabella" type="hidden" value="<?php echo $_GET['t']; ?>">
	<input id="obj_id" name="obj_id" type="hidden" value="<?php echo $_GET['i']; ?>">
	<input id="obj_nome" name="obj_nome" type="hidden" value="<?php echo $_GET['n']; ?>">
	<input id="obj_hyperlink" name="obj_hyperlink" type="hidden" value="<?php echo $_GET['h']; ?>">
	<input id="obj_varchar" name="obj_varchar" type="text" value="<?php echo htmlspecialchars($testo, ENT_QUOTES); ?>" class="pop_permalink">
    </form>
<?php
if ($trovato > 0)
{
?>
<!--
    <div style="height:15px;"></div>
    <span class="pop_breadcrumbs">permalink:</span><br>
    <div style="height:5px;"></div>
	<input id="obj_permalink" type="text" value="<?php echo $permalink; ?>" class="pop_permalink">
-->
<?php
}
?>
    <div style="height:100px;"></div>
	<table border="0" cellspacing="0" cellpadding="0">
      <tr>
<?php
if ($trovato > 0)
{
?>
        <td><img src="../bottoni/pop_modifica.gif" alt="modifica" width="160" height="40" border="0" style="cursor:pointer" onClick="permalink('modifica');"></td>
<?php
}
else
{
?>
        <td><img src="../bottoni/pop_aggiungi.gif" alt="aggiungi" width="160" height="40" border="0" style="cursor:pointer" onClick="permalink('aggiungi');"></td>
<?php
}
?>
		<td width="5">&nbsp;</td>
        <td><img src="../bottoni/pop_annulla.gif" alt="annulla" width="120" height="40" border="0" style="cursor:pointer" onClick="parent.Lightview.hide();"></td>
	  </tr>
    </table>
	</td>
    <td width="30">&nbsp;</td>
  </tr>
</table>
</body>
</html>
