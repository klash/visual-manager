# README #

OOP PHP content manager system full wysiwyg

I created this cms to reset the learning curve for managing the contents of a website by a content manager. with this innovative cms management of the site after authentication access will have the same vision of a normal user, but you can edit any element on the page with the use of the right mouse button. This project is designed for PHP developer with experience who want to devote much attention to the design of the UI and then simply replace elements on the page with the corresponding classes.

good work / alessandro verna

### What is this repository for? ###

* basic istaller
* 5.24

### How do I get set up? ###

* create folder test in your root folder
* copy all files in folder test
* create mySQL db and update with file start.sql in database folder
* create your CSS files and link to index.php
* create your HTML pages and replace the HTML object whit class object
* create your .htacces rules
* control all pager and logic state by index.php pages
* admin access at /yoursite/login
* admin user: klash
* admin password: B1tbucket